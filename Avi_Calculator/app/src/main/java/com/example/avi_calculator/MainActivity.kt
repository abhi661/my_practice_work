package com.example.avi_calculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.ArithmeticException
import java.lang.Exception
import kotlin.math.exp

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        one.setOnClickListener{appendOnExpresstion("1",canClear = true)}
        two.setOnClickListener{appendOnExpresstion("2",canClear = true)}
        three.setOnClickListener{appendOnExpresstion("3",canClear = true)}
        four.setOnClickListener{appendOnExpresstion("4",canClear = true)}
        five.setOnClickListener{appendOnExpresstion("5",canClear = true)}
        six.setOnClickListener{appendOnExpresstion("6",canClear = true)}
        seven.setOnClickListener{appendOnExpresstion("7",canClear = true)}
        eight.setOnClickListener{appendOnExpresstion("8",canClear = true)}
        nine.setOnClickListener{appendOnExpresstion("9",canClear = true)}
        zero.setOnClickListener{appendOnExpresstion("0",canClear = true)}
        dot.setOnClickListener{appendOnExpresstion(".",canClear = true)}

        //Operators
        plus.setOnClickListener{appendOnExpresstion("+",canClear = false)}
        minus.setOnClickListener{appendOnExpresstion("-",canClear = false)}
        multy.setOnClickListener{appendOnExpresstion("*",canClear = false)}
        division.setOnClickListener{appendOnExpresstion("/",canClear = false)}
        op.setOnClickListener{appendOnExpresstion("(",canClear = false)}
        cp.setOnClickListener{appendOnExpresstion(")",canClear = false)}

        //Clear button
        clear.setOnClickListener{
            expression.text=""
            result.text=""
        }

        //Backspace button
        back.setOnClickListener {
            val string=expression.text.toString()
            if(string.isNotEmpty()){
                expression.text=string.substring(0,string.length-1)
            }
            result.text=""
        }

        //Equal button code
        equal.setOnClickListener {
            try {
                val exp= ExpressionBuilder(expression.text.toString()).build()
                val res= exp.evaluate()
                val longResult=res.toLong()
                if(res==longResult.toDouble())
                    result.text=longResult.toString()
                else
                    result.text=res.toString()
            }catch (e:Exception){
                Log.d("Exception","message : "+e.message)
                if(e.message.toString()=="Division by zero!")
                    result.text="It will get enfinity!"
                else
                    result.text="Wrong input!"

            }
        }
    }

    fun appendOnExpresstion(string: String,canClear:Boolean){
        if(canClear){
            result.text=""
            expression.append(string)
        }else{
            expression.append(result.text)
            expression.append(string)
            result.text=""
        }
    }
}