package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.ChatBackupDao
import com.android.centrl.persistence.database.CentrlBackupDatabase
import com.android.centrl.persistence.entity.ChatBackupEntity
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for get backup details.
 */
class BackupRepository(context: Context) {
    private val chatBackupDao: ChatBackupDao

    init {
        val centrlDatabase = CentrlBackupDatabase.getInstance(context)
        chatBackupDao = centrlDatabase.chatBackupDao()
    }

    /**
     * @return the group title from the table
     */
    fun getBackupDetails(): ChatBackupEntity {
        return getGroupTitleAsyncTask()
    }

    /**
     * AsyncTask for get backup details from database
     */
    private fun getGroupTitleAsyncTask(): ChatBackupEntity {
        val chatBackupEntity = doAsyncResult(null,{
            chatBackupDao.getBackupsDetails()
        })
        if(chatBackupEntity.get() == null){
            return ChatBackupEntity("","","","","","")
        }
        return chatBackupEntity.get()
    }
}