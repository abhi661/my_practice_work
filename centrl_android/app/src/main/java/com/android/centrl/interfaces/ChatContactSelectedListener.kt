package com.android.centrl.interfaces

/**
 * Chat and mail contacts select listener
 */
interface ChatContactSelectedListener {
    fun chatContactSelected()
}