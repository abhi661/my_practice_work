package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.persistence.dao.BlockedContactsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.BlockedContactsEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for managing blocked contacts.
 */
class BlockedContactRepo(private val context: Context) {

    private var blockedContactsDao: BlockedContactsDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        blockedContactsDao = centrlDatabase.bockedContactsDao()
    }

    /**
     * Return all blocked contacts list
     */
    fun getAllBlockedList(): ArrayList<BlockedContactsEntity> {
        return getBlockedListAsyncTask()
    }

    /**
     * Return blocked details
     */
    fun getBlockedDetails(groupId: String): BlockedContactsEntity {
        return getBlockedDetailsAsyncTask(groupId)
    }

    /**
     * Delete blocked contacts list
     */
    fun deleteBlockedContact(groupId: String) {
        try {
            Completable.fromAction {
                blockedContactsDao.deleteBlockedContact(groupId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {
                        PendingIntentManager.getInstance(context).getBlockedContact()
                    }

                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * AsyncTask for get blocked list from database
     */
    private fun getBlockedListAsyncTask(): ArrayList<BlockedContactsEntity> {
        val result = doAsyncResult(null,{
            blockedContactsDao.getBlockedContactsDetails() as ArrayList<BlockedContactsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for blocked contacts details
     */
    private fun getBlockedDetailsAsyncTask(groupId: String): BlockedContactsEntity {
        val blockedContactsEntity = doAsyncResult(null,{
            blockedContactsDao.getBlockedContactDetails(groupId)
        })
        if(blockedContactsEntity.get() == null){
            return BlockedContactsEntity("","","")
        }
        return blockedContactsEntity.get()
    }
}