package com.android.centrl.models

/**
 * Chat and mail object class for separate receiver and sender.
 */
class DateObject : ListObject() {
    lateinit var date: String

    override fun getType(userId: Int): Int {
        return TYPE_DATE
    }
}