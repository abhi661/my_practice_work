package com.android.centrl.notificationservice

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.os.Build
import android.telephony.SmsManager
import android.telephony.SubscriptionInfo
import android.telephony.SubscriptionManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ListView
import com.android.centrl.R

/**
 * Send SMS class
 */
class SendSMS(private val context: Context) {

    private var simId: Int = 0
    private var isSimSelect: Boolean = false

    /**
     * Check whether dual sim or single sim
     */
    @SuppressLint("MissingPermission")
    fun checkDualSim(): Boolean {
        var isDualSim = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val localSubscriptionManager = context.getSystemService(SubscriptionManager::class.java)
            isDualSim = localSubscriptionManager!!.activeSubscriptionInfoCount > 1
        }
        return isDualSim
    }

    /**
     * Select sim to send message
     */
    fun selectSim() {
        if (checkDualSim()) {
            val simList = ListView(context)
            val accessNumberAdapter = ArrayAdapter(context, R.layout.default_list_layout, arrayOf("SIM 1", "SIM 2"))
            val simListDialogBuilder = AlertDialog.Builder(context)
            simListDialogBuilder
                .setTitle(R.string.select_sim)
                .setNegativeButton(R.string.cancel) { dialog, _ ->
                    isSimSelect = false
                    dialog.dismiss()
                }
                .setView(simList)
            val simListDialog = simListDialogBuilder.create()
            simList.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                isSimSelect = true
                simId = position + 1
                simListDialog.dismiss()
            }
            simList.adapter = accessNumberAdapter
            simListDialog.show()
        }
    }

    /**
     * Send message to number from selected sim
     */
    @SuppressLint("MissingPermission")
    fun send(message: String?, number: String?): Int {
        var sentResult = 0
        try {
            sentResult = if (!isSimSelect) {
                SmsManager.getDefault().sendTextMessage(number, null, message, null, null)
                1
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    val localSubscriptionManager = context.getSystemService(SubscriptionManager::class.java)
                    if (localSubscriptionManager!!.activeSubscriptionInfoCount > 1) {
                        val localList: List<*> = localSubscriptionManager.activeSubscriptionInfoList
                        val simInfo1: SubscriptionInfo = localList[0] as SubscriptionInfo
                        val simInfo2: SubscriptionInfo = localList[1] as SubscriptionInfo
                        if(simId == 1){
                            //SendSMS From SIM One
                            SmsManager.getSmsManagerForSubscriptionId(simInfo1.subscriptionId).sendTextMessage(number, null, message, null, null)
                        } else {
                            //SendSMS From SIM Two
                            SmsManager.getSmsManagerForSubscriptionId(simInfo2.subscriptionId).sendTextMessage(number, null, message, null, null)
                        }
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                        SmsManager.getSmsManagerForSubscriptionId(simId).sendTextMessage(number, null, message, null, null)
                    }
                }
                1
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return sentResult
    }
}