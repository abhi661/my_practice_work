package com.android.centrl.notificationservice

import android.service.notification.StatusBarNotification
import com.android.centrl.services.NotificationCatchService

/**
 * Centrl notification runnable class
 */
class CentrlNotificationRunnable(private val notiCatchService: NotificationCatchService, private val statusBarNotification: StatusBarNotification) : Runnable {
    override fun run() {
        notiCatchService.saveNotificationDetails(statusBarNotification)
    }
}