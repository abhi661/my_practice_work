package com.android.centrl.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.android.centrl.notificationservice.CentrlNotificationRunnable;
import com.android.centrl.notificationservice.NotificationCatchServicePresenter;
import com.android.centrl.notificationservice.NotificationData;
import com.android.centrl.notificationservice.NotificationCatchServicePresenterImpl;
import com.android.centrl.notificationservice.CentrlNotificationManager;
import com.android.centrl.notificationservice.PendingIntentManager;
import com.android.centrl.notificationservice.PrefManager;
import com.android.centrl.receiver.AppReceiver;
import com.android.centrl.utils.AppConstant;
import com.android.centrl.utils.CentrlUtil;
import com.android.centrl.utils.LogUtil;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static androidx.core.app.NotificationCompat.EXTRA_TITLE;

/**
 * Notification catch listener service class
 */
public class NotificationCatchService extends NotificationListenerService {

    public static final String ACCESS_NOTI_LISTENER = "access_noti_listener";
    public static final String TAG = LogUtil.makeLogTag(NotificationCatchService.class);

    private static int availableProcessors = Runtime.getRuntime().availableProcessors();

    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;

    private BlockingQueue<Runnable> blockingQueue;
    private ThreadPoolExecutor threadPoolExecutor;
    private PendingIntentManager pendingIntentManager;
    private PrefManager prefManager;
    private Context context;
    private NotificationCatchServicePresenter notificationCatchServicePresenter;
    private AppReceiver appReceiver;
    private Handler handler = new LooperHandler(this, Looper.getMainLooper());
    private BroadcastReceiver cancelReceiver = new CancelReceiver();
    private BroadcastReceiver sendIntent = new ScreenStatusReceiver();

    /**
     * Looper handler class
     */
    class LooperHandler extends Handler {
        LooperHandler(NotificationCatchService notiCatchService, Looper looper) {
            super(looper);
        }

        public void handleMessage(@NotNull Message message) {
            super.handleMessage(message);
        }
    }

    /**
     * Broadcast receiver for catch notification listener canceled.
     */
    public class CancelReceiver extends BroadcastReceiver {
        CancelReceiver() {
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void onReceive(Context context, Intent intent) {
            try {
                LogUtil.LOGI("CancelReceiver", "" + intent.getAction());
                if (Objects.requireNonNull(intent.getAction()).equalsIgnoreCase("com.Broadcast.cancel.notifications")) {
                    Notification notification = CentrlNotificationManager.Companion.getInstance(getApplicationContext()).getCentrlForegroundNotification();
                    if (notification != null) {
                        startForeground(1, notification);
                    }
                }
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Broadcast receiver for catch screen status.
     */
    class ScreenStatusReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                PrefManager.getInstance(context).saveBoolean(AppConstant.IS_CHAT_SCREEN, false);
            } else {
                if ("android.intent.action.SCREEN_ON".equals(intent.getAction())) {
                    PrefManager.getInstance(context).saveBoolean(AppConstant.IS_CHAT_SCREEN, false);
                } else {
                    if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                        PrefManager.getInstance(context).saveBoolean(AppConstant.IS_CHAT_SCREEN, false);
                    }
                }
            }
        }
    }

    /**
     * Setup thread pool
     */
    private void setThreadPool() {
        this.blockingQueue = new LinkedBlockingQueue();
        int i = availableProcessors;
        this.threadPoolExecutor = new ThreadPoolExecutor(i, i, 5, TIME_UNIT, this.blockingQueue);
    }

    /**
     * Register app broadcast receiver
     */
    private void registerPackageAction() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme("package");
        this.appReceiver = new AppReceiver();
        Context context = this.context;
        if (context != null) {
            context.getApplicationContext().registerReceiver(this.appReceiver, intentFilter);
        }
    }

    /**
     * Register canceled broadcast receiver
     */
    private void registerCancelNotification() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.Broadcast.cancel.notifications");
        Context context = this.context;
        if (context != null) {
            context.getApplicationContext().registerReceiver(this.cancelReceiver, intentFilter);
        }
    }

    /**
     * Register screen broadcast receiver
     */
    private void registerScreenActions() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        Context context = this.context;
        if (context != null) {
            context.getApplicationContext().registerReceiver(this.sendIntent, intentFilter);
        }
    }

    /**
     * Setup broadcast receiver
     */
    private void setRegisterReceiver() {
        registerPackageAction();
        registerCancelNotification();
        if (VERSION.SDK_INT >= 26) {
            registerScreenActions();
        }
    }

    /**
     * Remove call back messages
     */
    private void removeCallAndMessages() {
        for (Runnable runnable : this.blockingQueue) {
            Handler handler = this.handler;
            if (handler != null) {
                handler.removeCallbacksAndMessages(runnable);
            }
        }
    }

    /**
     * UnRegister broadcast receivers
     */
    private void UnRegisterReceivers() {
        Context context = this.context;
        if (context != null) {
            try {
                if (this.appReceiver != null) {
                    context.getApplicationContext().unregisterReceiver(this.appReceiver);
                    this.appReceiver = null;
                }
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            try {
                if (VERSION.SDK_INT >= 26 && this.sendIntent != null) {
                    this.context.getApplicationContext().unregisterReceiver(this.sendIntent);
                    this.sendIntent = null;
                }
            } catch (IllegalArgumentException e2) {
                e2.printStackTrace();
            }
            try {
                if (this.cancelReceiver != null) {
                    this.cancelReceiver = null;
                }
            } catch (IllegalArgumentException e3) {
                e3.printStackTrace();
            }
        }
    }

    /**
     * Listener created
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        super.onCreate();
        this.context = this;
        this.pendingIntentManager = PendingIntentManager.getInstance(this);
        this.notificationCatchServicePresenter = new NotificationCatchServicePresenterImpl(this);
        this.prefManager = PrefManager.getInstance(this);
        setThreadPool();
        try {
            Notification notification = CentrlNotificationManager.Companion.getInstance(getApplicationContext()).getCentrlForegroundNotification();
            startForeground(50, notification);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Listener destroyed
     */
    @Override
    public void onDestroy() {
        try {
            removeCallAndMessages();
            UnRegisterReceivers();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * Listener connected
     */
    @Override
    public void onListenerConnected() {
        this.prefManager.saveBoolean(ACCESS_NOTI_LISTENER, true);
        setRegisterReceiver();
        super.onListenerConnected();
    }

    /**
     * Listener disconnected
     */
    @Override
    public void onListenerDisconnected() {
        UnRegisterReceivers();
        this.prefManager.saveBoolean(ACCESS_NOTI_LISTENER, false);
        super.onListenerDisconnected();
    }

    /**
     * Notification received
     */
    @Override
    public void onNotificationPosted(StatusBarNotification statusBarNotification) {
        super.onNotificationPosted(statusBarNotification);
        if (!statusBarNotification.getPackageName().equalsIgnoreCase(context.getPackageName())) {
            LogUtil.LOGI(TAG, "onNotificationPosted..." + statusBarNotification);
            LogUtil.LOGI(TAG, "onNotificationPosted..." + statusBarNotification.getNotification().extras);
            String title = String.valueOf(statusBarNotification.getNotification().extras.getCharSequence(EXTRA_TITLE));
            if (title != null) {
                if (title.equals("Centrl is using battery")) {
                    if (VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cancelNotification(statusBarNotification.getKey());
                    }
                }
            }
            if (PrefManager.getInstance(this).isEnabled(AppConstant.IS_SEND, false)) {
                PrefManager.getInstance(this).saveBoolean(AppConstant.IS_SEND, false);
                cancelNotification(statusBarNotification.getKey());
                return;
            }
            if (!statusBarNotification.isOngoing()) {
                if (pendingIntentManager.isContainPackage(statusBarNotification.getPackageName())) {
                    if (!"android".equals(statusBarNotification.getPackageName()) && !this.context.getPackageName().equals(statusBarNotification.getPackageName()) && !snoozeNotification(statusBarNotification)) {
                        if (this.notificationCatchServicePresenter == null) {
                            this.notificationCatchServicePresenter = new NotificationCatchServicePresenterImpl(this.context);
                        }
                        executeThreadPool(statusBarNotification);
                    }
                }
            }
        }
    }

    /**
     * Notification removed
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        super.onNotificationRemoved(sbn);
        LogUtil.LOGI(TAG, "onNotificationRemoved" + sbn.getId());
        if (sbn.getId() == 1) {
            try {
                Notification notification = CentrlNotificationManager.Companion.getInstance(getApplicationContext()).getCentrlForegroundNotification();
                startForeground(50, notification);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Start command for service
     */
    @Override
    public int onStartCommand(Intent intent, int i, int i2) {
        return START_STICKY;
    }

    /**
     * Execute the ThreadPool for getting notifications
     */
    private void executeThreadPool(StatusBarNotification statusBarNotification) {
        if (this.prefManager.isEnabled(PrefManager.IS_INIT, false) && this.notificationCatchServicePresenter.shouldInsert(statusBarNotification)) {
            this.threadPoolExecutor.execute(new CentrlNotificationRunnable(this, statusBarNotification));
        }
    }

    /**
     * Get and Save Notifications Details from statusBarNotification
     */
    @SuppressLint("NewApi")
    public void saveNotificationDetails(StatusBarNotification statusBarNotification) {
        try {
            //boolean isGroupChat = false;
            if (Thread.interrupted()) {
                Thread.interrupted();
                return;
            }
            if (CentrlUtil.getPackageCodeList(context).get(statusBarNotification.getPackageName()) != AppConstant.SMS_CODE) {
                NotificationData parseNotification = this.notificationCatchServicePresenter.parseNotification(statusBarNotification, CentrlUtil.getPackageCodeList(context).get(statusBarNotification.getPackageName()));
                if (parseNotification != null) {
                    if (parseNotification.packCode == AppConstant.SKYPE_CODE) {
                        if (!pendingIntentManager.isBlockedContact(parseNotification.groupKey)) {
                            String chatKey = "";
                            if (parseNotification.packCode == AppConstant.GMAIL_CODE || parseNotification.packCode == AppConstant.OUTLOOK_CODE) {
                                chatKey = parseNotification.getMailKey();
                            } else {
                                chatKey = parseNotification.getUniqueKey();
                            }
                            if (!TextUtils.isEmpty(chatKey)) {
                                this.pendingIntentManager.putPendingIntent(chatKey, statusBarNotification.getNotification().contentIntent);
                            }
                            PendingIntentManager.PendingIntentAction notificationReplyInfo = this.pendingIntentManager.getNotificationReplyInfo(statusBarNotification, CentrlUtil.getPackageCodeList(context).get(statusBarNotification.getPackageName()));
                            if (notificationReplyInfo != null) {
                                if (!TextUtils.isEmpty(chatKey)) {
                                    this.pendingIntentManager.putReplyInfo(chatKey, notificationReplyInfo);
                                }
                                StringBuilder sb = new StringBuilder();
                                sb.append("chatKey:");
                                sb.append(chatKey);
                                sb.append("\nnotificationReplyInfo:");
                                sb.append(notificationReplyInfo.actionIntent);
                                LogUtil.LOGI("getChatKey", sb.toString());
                            }
                            Intent intent = new Intent(this.context, NotificationHandleService.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(NotificationHandleService.INTENT_KEY, parseNotification);
                            intent.putExtras(bundle);
                            NotificationHandleService.Companion.enqueueWork(this, intent);
                        }
                    } else {
                        if (!pendingIntentManager.isBlockedContact(parseNotification.groupKey)) {
                            String chatKey = "";
                            if (parseNotification.packCode == AppConstant.GMAIL_CODE || parseNotification.packCode == AppConstant.OUTLOOK_CODE) {
                                chatKey = parseNotification.getMailKey();
                            } else {
                                chatKey = parseNotification.getUniqueKey();
                            }
                            if (!TextUtils.isEmpty(chatKey)) {
                                this.pendingIntentManager.putPendingIntent(chatKey, statusBarNotification.getNotification().contentIntent);
                            }
                            PendingIntentManager.PendingIntentAction notificationReplyInfo = this.pendingIntentManager.getNotificationReplyInfo(statusBarNotification, CentrlUtil.getPackageCodeList(context).get(statusBarNotification.getPackageName()));
                            if (notificationReplyInfo != null) {
                                if (!TextUtils.isEmpty(chatKey)) {
                                    this.pendingIntentManager.putReplyInfo(chatKey, notificationReplyInfo);
                                }
                                StringBuilder sb = new StringBuilder();
                                sb.append("chatKey:");
                                sb.append(chatKey);
                                sb.append("\nnotificationReplyInfo:");
                                sb.append(notificationReplyInfo.actionIntent);
                                LogUtil.LOGI("getChatKey", sb.toString());
                            }
                            Intent intent = new Intent(this.context, NotificationHandleService.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable(NotificationHandleService.INTENT_KEY, parseNotification);
                            intent.putExtras(bundle);
                            NotificationHandleService.Companion.enqueueWork(this, intent);
                        }
                    }
                }
            }

            if (pendingIntentManager.isContainPackage(statusBarNotification.getPackageName())) {
                if (CentrlUtil.getPackageCodeList(context).get(statusBarNotification.getPackageName()) == AppConstant.SMS_CODE) {
                    if (PrefManager.getInstance(context).isEnabled(AppConstant.IS_VALID_NUMBER, false)) {
                        PrefManager.getInstance(context).saveBoolean(AppConstant.IS_VALID_NUMBER, false);
                        cancelNotification(statusBarNotification.getKey());
                        return;
                    }
                } else {
                    cancelNotification(statusBarNotification.getKey());
                }
            }

            Thread.interrupted();
        } catch (Exception e) {
            e.printStackTrace();
        } catch (Throwable th) {
            Thread.interrupted();
            throw th;
        }
    }

    /**
     * Snooze the notification
     */
    private boolean snoozeNotification(StatusBarNotification statusBarNotification) {
        if (VERSION.SDK_INT >= 26) {
            if ("android".equals(statusBarNotification.getPackageName())) {
                if ("0|android|40|null|1000".equals(statusBarNotification.getKey())) {
                    LogUtil.LOGI(TAG, "snoozeAndroidSystem");
                    snoozeNotification(statusBarNotification.getKey(), 2147483647L);
                    return true;
                }
            }
        }
        return false;
    }
}
