package com.android.centrl.persistence.dao

import androidx.room.*
import com.android.centrl.persistence.entity.ChatsCountsEntity

/**
 * Data Access Object for the chats count table.
 */
@Dao
interface ChatsCountDao {

    /**
     * Insert a chats count in the database. If the chats count already exists, replace it.
     *
     * @param chatsCountsEntity the chats count to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCount(chatsCountsEntity: ChatsCountsEntity)

    /**
     * Update a chats count in the database.
     */
    @Query("UPDATE chatCounts SET count = :count WHERE groupID= :groupId")
    fun updateCount(count: Int, groupId: String)

    /**
     *
     * @return the chats count from the table
     */
    @Query("SELECT * FROM chatCounts")
    fun getAllCountID(): List<ChatsCountsEntity>

    /**
     *
     * @return the chats count from the table
     */
    @Query("SELECT count FROM chatCounts WHERE groupID = :groupId")
    fun getCount(groupId: String): Int

    /**
     *
     * @return the chats count from the table
     */
    @Query("SELECT * FROM chatCounts")
    fun getAllCount(): List<ChatsCountsEntity>

    /**
     * Delete chats count.
     */
    @Query("DELETE FROM chatCounts WHERE groupID = :groupId")
    fun deleteCount(groupId: String)
}