package com.android.centrl.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.interfaces.SelectChannel
import com.android.centrl.R
import com.android.centrl.adapter.KeyBoardTrayChannelListAdapter
import com.android.centrl.databinding.DialogFilterMessageBinding
import com.android.centrl.models.KeyboardAppTrayModel
import com.android.centrl.persistence.repositry.ChatsRepository
import com.android.centrl.persistence.repositry.KeyBoardAppsRepository
import java.util.*

/**
 * Filter channels for chats dialog
 */
class DialogChatFilterMessage : DialogFragment(), SelectChannel {

    private var dialogFilterMessageListener: DialogFilterMessageListener? = null

    override fun channelSelected(packCode: Int) {
        dialogFilterMessageListener!!.onMessageFilter(packCode)
        dismiss()
    }
    /**
     * Filter channels interface
     */
    interface DialogFilterMessageListener {
        fun onMessageFilter(packCode: Int)
    }

    private lateinit var binding: DialogFilterMessageBinding
    private val messageList = ArrayList<Int>()
    private val keyboardTray: ArrayList<KeyboardAppTrayModel> = ArrayList()
    private var chatRecyclerAdapter: KeyBoardTrayChannelListAdapter? = null
    private var selectIdentity: SelectChannel? = null
    private var keyBoardAppsRepository: KeyBoardAppsRepository? = null
    private lateinit var chatsRepository: ChatsRepository
    private var isFilterMSG = false
    private lateinit var groupId: String

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater,R.layout.dialog_filter_message, null, false)
        dialog.setContentView(binding.root)
        keyBoardAppsRepository = KeyBoardAppsRepository(requireActivity())
        chatsRepository = ChatsRepository(requireActivity())
        selectIdentity = this
        isFilterMSG = requireArguments().getBoolean("isfilter")
        groupId = requireArguments().getString("groupId", null)
        val activeChannels = chatsRepository.getGroupChannelsList(groupId)
        if (isFilterMSG) {
            val packCodeList = keyBoardAppsRepository!!.getAllKeyboardAppTrayPackCode()
            for (i in packCodeList.indices) {
                when (val packCode = packCodeList[i]) {
                    0, 30, 31 -> {
                        //
                    }
                    else -> {
                        messageList.add(packCode)
                    }
                }
            }
            messageList.add(40)
            for(i in activeChannels){
                keyboardTray.add(KeyboardAppTrayModel(i,1))
                if(messageList.contains(i)){
                    messageList.remove(i)
                }
            }
            for(i in messageList){
                keyboardTray.add(KeyboardAppTrayModel(i,0))
            }
        } else {
            val packCodeList = keyBoardAppsRepository!!.getAllKeyboardAppTrayPackCode()
            for (i in packCodeList.indices) {
                when (val packCode = packCodeList[i]) {
                    0, 30, 31 -> {
                        //
                    }
                    else -> {
                        messageList.add(packCode)
                    }
                }
            }
            for(i in activeChannels){
                keyboardTray.add(KeyboardAppTrayModel(i,1))
                if(messageList.contains(i)){
                    messageList.remove(i)
                }
            }
            for(i in messageList){
                keyboardTray.add(KeyboardAppTrayModel(i,0))
            }
        }

        val layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        binding.channelsRecyclerViewDialog.layoutManager = layoutManager
        chatRecyclerAdapter = KeyBoardTrayChannelListAdapter(keyboardTray, selectIdentity as DialogChatFilterMessage)
        binding.channelsRecyclerViewDialog.adapter = chatRecyclerAdapter
        binding.closeFilterChannel.setOnClickListener { dialog.dismiss() }
        return dialog
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogFilterMessageListener = activity as DialogFilterMessageListener?
    }

    companion object {
        var TAG = "DialogFilterMessage"
    }
}