package com.android.centrl.notificationservice;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationCompat.WearableExtender;
import androidx.core.app.NotificationCompat.Action;
import androidx.core.app.RemoteInput;

import com.android.centrl.persistence.entity.BlockedContactsEntity;
import com.android.centrl.persistence.repositry.ChatsRepository;
import com.android.centrl.persistence.repositry.PendingIntentManagerRepo;
import com.android.centrl.services.ChannelsPackageRefreshService;
import com.android.centrl.utils.CentrlUtil;
import com.android.centrl.utils.LogUtil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.content.Intent.FLAG_ACTIVITY_NO_USER_ACTION;
import static com.android.centrl.utils.AppConstant.SMS_CODE;

/**
 * Notifications pending intent for reply messages
 */
public class PendingIntentManager {
    public static final int MAX_SIZE = 1000;

    @SuppressLint("StaticFieldLeak")
    private static PendingIntentManager pendingIntentManager;

    private HashMap<String, String[]> stringHashMap;

    private PrefManager prefManager;

    private String string;

    private Context context;

    public PendingIntentManagerRepo pendingIntentManagerRepo;
    public ChatsRepository chatsRepository;

    public MaxSizeHashMap<String, PendingIntent> sPendingMap = new MaxSizeHashMap<>(MAX_SIZE);
    public MaxSizeHashMap<String, PendingIntentAction> sReplyMap = new MaxSizeHashMap<>(MAX_SIZE);

    public ArrayList<BlockedContactsEntity> blockedContactList = new ArrayList<>();
    public ArrayList<String> packageList = new ArrayList<>();


    /**
     * Pending intent action class
     */
    public static class PendingIntentAction implements Serializable {
        public PendingIntent actionIntent;
        public Bundle bundle;
        public RemoteInput[] remoteInputs;

        public PendingIntentAction() {
        }
    }

    private PendingIntentManager(Context context) {
        this.context = context;
        this.prefManager = PrefManager.getInstance(context);
        this.pendingIntentManagerRepo = new PendingIntentManagerRepo(context);
        this.chatsRepository = new ChatsRepository(context);
        refreshKeyBoardApps();
        setChatAndKeyMap(this.prefManager.loadStringValue(PrefManager.DEFAULT_CHAT_KEY, PrefManager.DEFAULT_CHAT_KEY_VALUE));
        getBlockedContact();
    }

    /**
     * Open other installed apps
     */
    private void startOtherApp(String str) {
        String str2 = "launchIntentForPackage";
        try {
            Intent launchIntentForPackage = this.context.getPackageManager().getLaunchIntentForPackage(str);
            if (launchIntentForPackage != null) {
                launchIntentForPackage.setFlags(FLAG_ACTIVITY_NEW_TASK);
            }
            // LogUtil.INSTANCE.mLOGE(str2, "success" + str);
            this.context.startActivity(launchIntentForPackage);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.INSTANCE.mLOGE(str2, "not found");
        }
    }


    public static PendingIntentManager getInstance(Context context) {
        synchronized (PendingIntentManager.class) {
            if (pendingIntentManager == null) {
                pendingIntentManager = new PendingIntentManager(context);
            }
        }
        return pendingIntentManager;
    }

    /**
     * Get notification reply info
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public PendingIntentAction getNotificationReplyInfo(StatusBarNotification statusBarNotification, int packCode) {
        if (packCode == SMS_CODE) {
            findResultKey(statusBarNotification.getNotification(), statusBarNotification.getPackageName());
        }

        if (this.stringHashMap == null) {
            setChatAndKeyMap("");
        }
        String[] strArr = this.stringHashMap.get(statusBarNotification.getPackageName());
        StringBuilder sb = new StringBuilder();
        sb.append("statusBarNotification.getPackageName()");
        sb.append(statusBarNotification.getPackageName());
        //LogUtil.LOGI(str, sb.toString());
        //LogUtil.LOGI(str, "strArr...." + Arrays.toString(strArr));
        if (strArr == null) {
            return null;
        }
        Notification notification = statusBarNotification.getNotification();
        Action b = actions(notification, strArr);
        StringBuilder sb2 = new StringBuilder();
        sb2.append(notification.extras.toString());
        // LogUtil.LOGI(str, sb2.toString());
        if (!(b == null || b.getRemoteInputs() == null)) {
            //LogUtil.INSTANCE.mLOGE("getRemoteInputs", "getRemoteInputs");
            PendingIntentAction aVar = new PendingIntentAction();
            aVar.actionIntent = b.actionIntent;
            aVar.remoteInputs = b.getRemoteInputs();
            aVar.bundle = notification.extras;
            if (aVar.actionIntent == null || aVar.remoteInputs == null) {
                return null;
            }
            return aVar;
        }
        return null;
    }

    /**
     * Get reply object from notification
     */
    private Action actions(Notification notification, String[] strArr) {
        Action c = getAction(notification, strArr);
        return c == null ? getWearable(notification, strArr) : c;
    }

    /**
     * Get reply object from notification
     */
    private Action getWearable(Notification notification, String[] strArr) {
        for (Action bVar : new WearableExtender(notification).getActions()) {
            if (bVar.getRemoteInputs() != null) {
                for (RemoteInput nVar : bVar.getRemoteInputs()) {
                    if (nVar != null) {
                        String sb = "keyyyy: " + nVar.getResultKey();
                         LogUtil.INSTANCE.mLOGE("KJ", sb);
                        if (isReply(nVar.getResultKey(), strArr)) {
                            return bVar;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Find and get result key for reply messages
     */
    private Action getAction(Notification notification, String[] strArr) {
        for (int i = 0; i < NotificationCompat.getActionCount(notification); i++) {
            Action action = NotificationCompat.getAction(notification, i);
            if (action.getRemoteInputs() != null) {
                for (RemoteInput nVar : action.getRemoteInputs()) {
                    if (nVar != null) {
                        String sb = "result key: " +
                                nVar.getResultKey();
                        LogUtil.INSTANCE.mLOGE("KJ", sb);
                        if (isReply(nVar.getResultKey(), strArr)) {
                            return action;
                        }
                    }
                }
            }
        }
        return null;
    }


    /**
     * Open other chat apps
     */
    public void goOtherApps(String str, String str2) {
        try {
            sendPendingIntent(str);
        } catch (Exception unused) {
            startOtherApp(str2);
        }
    }

    /**
     * Check reply info is available or not
     */
    public boolean isReplyInfo(String str) {
        return this.sReplyMap.get(str) != null;
    }

    /**
     * Store reply pending intent object from notification
     */
    public void putPendingIntent(String str, PendingIntent pendingIntent) {
        if (!TextUtils.isEmpty(str)) {
            this.sPendingMap.put(str, pendingIntent);
        }
    }

    /**
     * Store reply info object from notification
     */
    public void putReplyInfo(String str, PendingIntentAction aVar) {
        if (!TextUtils.isEmpty(str)) {
            this.sReplyMap.put(str, aVar);
        }
    }

    /**
     * Send reply message to users
     */
    public void reply(String str, String str2) throws CanceledException {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            PendingIntentAction aVar = this.sReplyMap.get(str);
            if (aVar != null) {
                String sb = "replychatKey:" +
                        str +
                        "\nnotificationReplyInfo:" +
                        aVar.actionIntent;
                LogUtil.INSTANCE.mLOGE("getChatKey", sb);
                Intent intent = new Intent();
                intent.addFlags(FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(FLAG_ACTIVITY_NO_USER_ACTION);
                Bundle bundle = aVar.bundle;
                for (RemoteInput resultKey : aVar.remoteInputs) {
                    bundle.putCharSequence(resultKey.getResultKey(), str2);
                    //LogUtil.INSTANCE.mLOGE("Reply@@@", "sent..."+resultKey.getResultKey());
                }
                RemoteInput.addResultsToIntent(aVar.remoteInputs, intent, bundle);
                try {
                    aVar.actionIntent.send(this.context, 0, intent);
                    // LogUtil.INSTANCE.mLOGE("Reply@@@", "sent");
                } catch (Exception unused) {
                    this.sReplyMap.remove(str);
                    throw new CanceledException();
                }
            }
        }
    }

    /**
     * Sending pending intent to open other apps
     */
    public void sendPendingIntent(String str) throws CanceledException {
        if (!TextUtils.isEmpty(str)) {
            PendingIntent pendingIntent = this.sPendingMap.get(str);
            if (pendingIntent != null) {
                try {
                    pendingIntent.send();
                    String sb = "" +
                            str;
                    LogUtil.INSTANCE.mLOGE("sendPendingIntent$$$", sb);
                } catch (CanceledException e) {
                    e.printStackTrace();
                    this.sPendingMap.remove(str);
                    throw e;
                }
            } else {
                //LogUtil.INSTANCE.mLOGE("sendPendingIntent@@@", "sendPendingIntent");
                throw new CanceledException("No reply");
            }
        } else {
            throw new CanceledException("No channel");
        }
    }

    /**
     * Config chat key and reply object
     */
    public void setChatAndKeyMap(String str) {
        //  LogUtil.LOGI("setChatAndKeyMap", str);
        if (TextUtils.isEmpty(this.string) || !this.string.equals(str)) {
            this.string = str;
            String[] split = str.split(";");
            this.stringHashMap = new HashMap<>();
            for (String split2 : split) {
                String[] split3 = split2.split(",");
                // Log.i("setChatAndKeyMap", ""+split3[0]);
                ///LogUtil.LOGI("setChatAndKeyMap", "gh.."+Arrays.toString(split3));
                this.stringHashMap.put(split3[0], Arrays.copyOfRange(split3, 1, split3.length));
            }
        }
    }

    /**
     * Find and get result key for reply messages
     */
    private void findResultKey(Notification notification, String packages) {
        for (int i = 0; i < NotificationCompat.getActionCount(notification); i++) {
            Action action = NotificationCompat.getAction(notification, i);
            if (action.getRemoteInputs() != null) {
                for (RemoteInput nVar : action.getRemoteInputs()) {
                    if (nVar != null) {
                        StringBuilder sb = new StringBuilder();
                        sb.append(packages);
                        sb.append(",");
                        sb.append(nVar.getResultKey());
                        LogUtil.INSTANCE.mLOGE("KJ", sb.toString());
                        setChatAndKeyMap(this.prefManager.loadStringValue(PrefManager.DEFAULT_CHAT_KEY, PrefManager.DEFAULT_CHAT_KEY_VALUE + ";" + sb.toString()));
                    }
                }
            }
        }
    }

    /**
     * Find and get result key for reply messages
     */
    /*public void getResultKey(Notification notification, String packages) {
        LogUtil.INSTANCE.mLOGE("getResultKey", "RemoteInput...." + NotificationCompat.getActionCount(notification));
        for (int i = 0; i < NotificationCompat.getActionCount(notification); i++) {
            Action action = NotificationCompat.getAction(notification, i);
            if (action.getRemoteInputs() != null) {
                for (RemoteInput nVar : action.getRemoteInputs()) {
                    if (nVar != null) {
                        String sb = packages +
                                "," +
                                nVar.getResultKey();
                        LogUtil.INSTANCE.mLOGE("getResultKey", sb);
                    } else {
                        LogUtil.INSTANCE.mLOGE("getResultKey", "RemoteInput actions null");
                    }
                }
            } else {
                LogUtil.INSTANCE.mLOGE("getResultKey", "RemoteInput null");
            }
        }
    }*/

    private boolean isReply(String str, String[] strArr) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        for (String equals : strArr) {
            if (str.equals(equals) || str.toLowerCase().contains("reply") || str.toLowerCase().contains("input")) {
                return true;
            }
        }
        return false;
    }

    public void refreshKeyBoardApps() {
        ArrayList<String> mAllPackages = pendingIntentManagerRepo.getAllInstalledPackageName();
        if (CentrlUtil.getChannelsLogo().size() != mAllPackages.size()) {
            Intent intent = new Intent(context, ChannelsPackageRefreshService.class);
            ChannelsPackageRefreshService.Companion.enqueueWork(context, intent);
        }
    }

    /**
     * Get all blocked contacts
     */
    public void getBlockedContact() {
        try {
            ExecutorService executor = Executors.newCachedThreadPool();
            Runnable runnable = () -> blockedContactList = pendingIntentManagerRepo.getAllBlockedList();
            executor.execute(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get all keyboard tray packages
     */
    public void getPackageList() {
        try {
            ExecutorService executor = Executors.newCachedThreadPool();
            Runnable runnable = () -> packageList = pendingIntentManagerRepo.getAllKeyboardAppTrayPackName();
            executor.execute(runnable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This is use to find containing blocked contacts
     */
    public boolean isBlockedContact(String groupKey) {
        try {
            for (BlockedContactsEntity chat : blockedContactList) {
                if (chat.getGroupID().equalsIgnoreCase(groupKey)) {
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * This is use to find containing packages
     */
    public boolean isContainPackage(String packageName) {
        try {
            ArrayList<String> mAllPackages = packageList;
            if (mAllPackages.contains(packageName)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

}
