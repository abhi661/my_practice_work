package com.android.centrl.notificationservice

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

/**
 * Centrl notification preference manager
 */
class PrefManager private constructor(context: Context) {
    private val context: Context?

    init {
        this.context = context
        defaultPref
    }
    private var sharedPreferences: SharedPreferences? = null
    private val defaultPref: Unit
        get() {
            try {
                if (sharedPreferences == null && context != null) {
                    sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    fun destroy() {
        prefManager = null
    }

    fun isEnabled(str: String?, z: Boolean): Boolean {
        defaultPref
        val sharedPreferences = sharedPreferences
        return sharedPreferences?.getBoolean(str, z) ?: z
    }

    fun loadStringValue(str: String?, str2: String?): String? {
        defaultPref
        val sharedPreferences = sharedPreferences
        return if (sharedPreferences != null) sharedPreferences.getString(str, str2) else str2
    }

    fun loadIntegerValue(str: String?, str2: Int): Int {
        defaultPref
        val sharedPreferences = sharedPreferences
        return if (sharedPreferences != null) sharedPreferences.getInt(str, str2) else str2
    }
    fun saveBoolean(str: String?, z: Boolean) {
        defaultPref
        val sharedPreferences = sharedPreferences
        sharedPreferences?.edit()?.putBoolean(str, z)?.apply()
    }

    fun saveStringValue(str: String?, str2: String?) {
        defaultPref
        val sharedPreferences = sharedPreferences
        sharedPreferences?.edit()?.putString(str, str2)?.apply()
    }
    fun saveIntegerValue(str: String?, str2: Int) {
        defaultPref
        val sharedPreferences = sharedPreferences
        sharedPreferences?.edit()?.putInt(str, str2)?.apply()
    }

    companion object {
        const val DEFAULT_CHAT_KEY = "default_chat_key"
        const val DEFAULT_CHAT_KEY_VALUE = "com.icq.mobile.client,extra_text_reply;com.discord,discord_notif_text_input;com.google.android.apps.fireball,reply;com.google.android.talk,android.intent.extra.TEXT;com.skype.raider,key_text_reply;com.viber.voip,remote_text_input;jp.naver.line.android,line.text;org.telegram.messenger,extra_voice_reply;com.instagram.android,DirectNotificationConstants.DirectReply;com.twitter.android,dm_text;com.facebook.orca,voice_reply;com.kakao.talk,extra_voice_reply,extra_direct_reply,reply_message;com.whatsapp,android_wear_voice_input;com.Slack,key_reply_text;com.samsung.android.messaging,key_reply_text;com.facebook.mlite,key_text_reply;com.facebook.orca,key_text_reply;com.facebook.lite,key_text_reply;com.hike.chat.stickers,notif_reply"
        const val IS_INIT = "IS_INIT"
        const val MAIN_SCREEN_PRESENT = "chat_present"
        private var prefManager: PrefManager? = null

        @JvmStatic
        fun getInstance(context: Context): PrefManager? {
            synchronized(PrefManager::class.java) {
                if (prefManager == null) {
                    prefManager = PrefManager(context.applicationContext)
                }
            }
            return prefManager
        }
    }

}