package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.centrl.R
import com.android.centrl.adapter.BlockedContactListAdapter
import com.android.centrl.databinding.ActivityBlockedContactBinding
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.persistence.entity.BlockedContactsEntity
import com.android.centrl.utils.AdsLoading
import com.google.android.gms.ads.AdView
import java.util.*

/**
 * Blocked contacts activity
 */
class BlockedContactListActivity : BaseActivity(), BlockedContactListAdapter.RemoveContactsInterface {

    private lateinit var binding: ActivityBlockedContactBinding
    private var blockedContactListAdapter: BlockedContactListAdapter? = null
    private var blockedChatList = ArrayList<BlockedContactsEntity>()
    private var mAdView: AdView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_blocked_contact)
        binding.handler = ClickHandler()

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.blockedRecycleView.layoutManager = layoutManager
        blockedContactListAdapter = BlockedContactListAdapter(this, null, this)
        binding.blockedRecycleView.adapter = blockedContactListAdapter

        binding.blockedAds.adViewContainer.post {
            //Loading ads
            mAdView = AdsLoading.loadBanner(this@BlockedContactListActivity, binding.blockedAds.adViewContainer)
        }

        mBlockedContactsViewModel!!.mBlockedContactsList.observe(this, androidx.lifecycle.Observer { blockedList ->
            blockedChatList = blockedList
            if (blockedChatList.size == 0) {
                binding.blockedRecycleView.visibility = View.GONE
                binding.blockedEmptyLl.visibility = View.VISIBLE
            } else {
                binding.blockedRecycleView.visibility = View.VISIBLE
                binding.blockedEmptyLl.visibility = View.GONE
                blockedContactListAdapter!!.updateList(blockedChatList)
            }
            PendingIntentManager.getInstance(this).getBlockedContact()
        })
    }
    /**
     * Views click handler class
     */
    inner class ClickHandler{
        /**
         * Views click handler
         */
        fun onClick(view: View){
            when(view.id){
                R.id.ignored_back -> { finish() }
                R.id.add_person_block -> {
                    startActivity(Intent(this@BlockedContactListActivity, SelectContactActivity::class.java))
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mBlockedContactsViewModel!!.getAllBlockedList()
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.blockedAds.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
    /**
     * Remove blocked contacts from database
     */
    override fun removeContacts(chat: BlockedContactsEntity?) {
        try {
            mBlockedContactsViewModel!!.deleteBlockedContact(chat!!.groupID)
            mBlockedContactsViewModel!!.getAllBlockedList()
        } catch (e: Exception) { }
    }
}