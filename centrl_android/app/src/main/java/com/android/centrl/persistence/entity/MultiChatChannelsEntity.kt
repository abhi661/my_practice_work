package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a multi chat channels in groups table
 */
@Entity(tableName = "multiChats")
data class MultiChatChannelsEntity(
    @PrimaryKey
    var packageCode: Int,
    var groupID: String,
    var chatKey: String
)