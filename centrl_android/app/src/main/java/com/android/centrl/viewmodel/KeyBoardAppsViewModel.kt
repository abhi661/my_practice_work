package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.centrl.persistence.datasource.KeyBoardAppsTrayDataSource
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import com.android.centrl.persistence.entity.RemovedKeyboardAppsEntity
import com.android.centrl.persistence.repositry.KeyBoardAppsRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

/**
 * View Model for the [@KeyBoardSettingsActivity]
 */
class KeyBoardAppsViewModel(
    application: Application,
    private val mDataSource: KeyBoardAppsTrayDataSource
) : BaseViewModel(application) {

    private var keyboardAppsRepository: KeyBoardAppsRepository = KeyBoardAppsRepository(application)

    private val mKeyboardAppsTrayPackages = MutableLiveData<ArrayList<Int>>()
    private val mRemovedAppsPackages = MutableLiveData<ArrayList<Int>>()
    private val keyBoardTrayInsertStatus = MutableLiveData<Boolean>()
    private val keyBoardInsertStatus = MutableLiveData<Boolean>()

    //Get the keyboard apps tray.
    val mAppsTrayPackages: LiveData<ArrayList<Int>>
        get() = mKeyboardAppsTrayPackages
    //Get the keyboard apps from removed list.
    val mRemovedAppsPackage: LiveData<ArrayList<Int>>
        get() = mRemovedAppsPackages

    /**
     * Get the removed keyboard apps package code.
     */
    fun getRemovedAppPackagesList(): ArrayList<Int>{
        return keyboardAppsRepository.getAllRemovedChatAppPackages()
    }
    /**
     * Get the keyboard apps tray package code.
     */
    fun getKeyboardAppTrayPackCodeList(): ArrayList<Int>{
        return keyboardAppsRepository.getAllKeyboardAppTrayPackCode()
    }

    /**
     * Get the keyboard apps tray.
     *
     * @return a [Flowable] that will emit every time the keyboard apps tray has been updated.
     */
    fun getAllKeyboardAppTrayPackCode() {
        disposables += mDataSource.getAllKeyboardAppTrayPackCode()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { keyboardApps ->
                mKeyboardAppsTrayPackages.value = keyboardApps as ArrayList<Int>?
            }
    }

    /**
     * Get the keyboard apps from removed list.
     *
     * @return a [Flowable] that will emit every time the apps removed list has been updated.
     */
    fun getAllRemovedChatAppPackages() {
        disposables += mDataSource.getAllRemovedChatAppPackages()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { keyboardApps ->
                mRemovedAppsPackages.value = keyboardApps as ArrayList<Int>?
            }
    }

    /**
     * Insert or update the removed apps from keyboard apps tray.
     *
     * @return a [Completable] that completes when the removed apps from keyboard apps tray
     */
    fun insertRemovedKeyboardApps(removedKeyboardAppsEntity: RemovedKeyboardAppsEntity) {
        disposables += mDataSource.insertRemovedKeyboardApps(removedKeyboardAppsEntity)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onComplete = {
                keyBoardInsertStatus.postValue(true)
            }, onError = {
                Timber.e(it)
                keyBoardInsertStatus.postValue(false)
            })
    }

    /**
     * Insert or update the keyboard apps tray.
     *
     * @return a [Completable] that completes when the keyboard apps tray is updated
     */
    fun insertKeyboardAppTray(keyboardAppsTrayEntity: KeyboardAppsTrayEntity) {
        disposables += mDataSource.insertKeyboardAppTray(keyboardAppsTrayEntity)
            .subscribeOn(Schedulers.io())
            .subscribeBy(onComplete = {
                keyBoardTrayInsertStatus.postValue(true)
            }, onError = {
                Timber.e(it)
                keyBoardTrayInsertStatus.postValue(false)
            })
    }
    /**
     * Delete all apps from keyboard tray.
     */
    fun deleteKeyboardAppTray(code: Int) {
        keyboardAppsRepository.deleteKeyboardAppTray(code)
    }
    /**
     * Delete all keyboard apps details from removed keyboard tray.
     */
    fun deleteKeyboardApps(code: Int) {
        keyboardAppsRepository.deleteKeyboardApps(code)
    }
}