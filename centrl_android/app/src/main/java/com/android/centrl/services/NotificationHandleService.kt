package com.android.centrl.services

import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService
import com.android.centrl.notificationservice.CentrlNotificationManager
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.persistence.entity.ChatsCountsEntity
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.persistence.entity.MultiChatChannelsEntity
import com.android.centrl.persistence.repositry.ChatsRepository
import com.android.centrl.persistence.repositry.GroupChannelsRepository
import com.android.centrl.persistence.repositry.MultiChannelCountRepository
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.LogUtil
import java.util.*

class NotificationHandleService : JobIntentService() {

    companion object {
        /**
         * Unique job ID for this service.
         */
        private const val JOB_ID = 2
        val TAG: String = LogUtil.makeLogTag(NotificationHandleService::class.java)
        const val INTENT_KEY = "noti"
        fun enqueueWork(context: Context, intent: Intent) {
            enqueueWork(context, NotificationHandleService::class.java, JOB_ID, intent)
        }
    }
    val context: Context
        get() = applicationContext
    private var mMutiChannelRepository: MultiChannelCountRepository? = null
    private var mGroupChannelsRepository: GroupChannelsRepository? = null
    private var chatsRepository: ChatsRepository? = null

    override fun onCreate() {
        super.onCreate()
        mMutiChannelRepository = MultiChannelCountRepository(context)
        mGroupChannelsRepository = GroupChannelsRepository(context)
        chatsRepository = ChatsRepository(context)
    }
    override fun onHandleWork(intent: Intent) {
        try {
            val extras = intent.extras
            if (extras != null) {
                val sVar = extras.getSerializable(INTENT_KEY) as NotificationData?
                if (sVar!!.uniqueKey!!.isNotEmpty())
                    LogUtil.mLOGI("databaseHelper...", "chat...$sVar")
                val chat = ChatsEntity(
                    0,
                    sVar.groupKey,
                    sVar.uniqueKey,
                    sVar.mailKey,
                    sVar.content,
                    sVar.packCode,
                    sVar.title,
                    sVar.notiAt,
                    ChatsEntity.MSG_TYPE_RECEIVED,
                    sVar.iconPath,
                    sVar.notiType,
                    sVar.phoneNumber
                )
                chatsRepository!!.insertChat(chat)
                val getChatKey = mGroupChannelsRepository!!.getChatKey(sVar.uniqueKey)
                if (getChatKey.isEmpty()) {
                    val rand = Random()
                    val randomId = rand.nextInt(100000)
                    val groupChat = GroupChannelsEntity(
                        randomId,
                        sVar.groupKey,
                        sVar.uniqueKey,
                        sVar.title,
                        sVar.packCode,
                        sVar.iconPath,
                        sVar.groupIconPath,
                        sVar.title,
                        sVar.phoneNumber,
                        0,
                        sVar.notiType,
                        0,
                        sVar.isGroupChat
                    )
                    mGroupChannelsRepository!!.insertGroup(groupChat)
                } else {
                    val groupSize = mGroupChannelsRepository!!.getGroupList(sVar.groupKey).size
                    if(groupSize == 1){
                        val isEdit = mGroupChannelsRepository!!.getIsEdited(sVar.groupKey)
                        if(isEdit == 0){
                            mGroupChannelsRepository!!.updateIcon(sVar.groupKey, sVar.iconPath, sVar.groupIconPath, sVar.groupTitle, sVar.title)
                        }
                    }
                }
                var countId = mMutiChannelRepository!!.getCount(sVar.groupKey!!)
                if (countId == 0) {
                    ++countId
                    val count = ChatsCountsEntity(sVar.groupKey!!, sVar.uniqueKey!!, countId)
                    mMutiChannelRepository!!.insertCount(count)
                    val packCode = mMutiChannelRepository!!.getPackageCode(sVar.packCode, sVar.groupKey!!)
                    if (packCode != sVar.packCode) {
                        mMutiChannelRepository!!.insertMultiChatIdentity(MultiChatChannelsEntity(
                            chat.packageCode,
                            chat.groupID,
                            chat.chatKey
                        ))
                    }
                } else {
                    ++countId
                    mMutiChannelRepository!!.updateCount(countId, sVar.groupKey!!)
                    val packCode = mMutiChannelRepository!!.getPackageCode(sVar.packCode, sVar.groupKey!!)
                    if (packCode != sVar.packCode) {
                        mMutiChannelRepository!!.insertMultiChatIdentity(MultiChatChannelsEntity(
                            chat.packageCode,
                            chat.groupID,
                            chat.chatKey
                        ))
                    }
                }
                if (!PrefManager.getInstance(this)!!.isEnabled(AppConstant.IS_CHAT_SCREEN, false)) {
                    val mute = mGroupChannelsRepository!!.getGroupDetails(sVar.groupKey)
                    if (mute.muteChat == 0) {
                        CentrlNotificationManager.getInstance(context)!!.showCentrlNotification(sVar)
                    }
                } else {
                    val groupID = PrefManager.getInstance(this)!!.loadStringValue(AppConstant.CHAT_GROUP_ID, null)
                    if(groupID!!.isNotEmpty() && groupID != sVar.groupKey){
                        val mute = mGroupChannelsRepository!!.getGroupDetails(sVar.groupKey)
                        if (mute.muteChat == 0) {
                            CentrlNotificationManager.getInstance(context)!!.showCentrlNotification(sVar)
                        }
                    }
                }

                if (PrefManager.getInstance(this)!!.isEnabled(AppConstant.IS_CHAT_SCREEN, false)) {
                    if(PrefManager.getInstance(this)!!.loadStringValue(AppConstant.CHAT_GROUP_ID, null) == sVar.groupKey){
                        val chatIntent = Intent(AppConstant.ACTION_BROADCAST_RECEIVER_CHAT)
                        chatIntent.putExtra(AppConstant.PACKAGE_CODE_KEY, sVar.packCode)
                        chatIntent.putExtra(AppConstant.GROUP_KEY, sVar.groupKey)
                        chatIntent.putExtra(AppConstant.NOTIFICATION_TYPE, sVar.notiType)
                        sendBroadcast(chatIntent)
                    }
                } else {
                    val listIntent = Intent(AppConstant.ACTION_BROADCAST_RECEIVER)
                    listIntent.putExtra(AppConstant.GROUP_KEY, sVar.groupKey)
                    listIntent.putExtra(AppConstant.NOTIFICATION_TYPE, sVar.notiType)
                    sendBroadcast(listIntent)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}