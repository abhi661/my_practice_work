package com.android.centrl.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.Telephony;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.android.centrl.R;
import com.android.centrl.persistence.entity.ChatsEntity;
import com.android.centrl.models.ChatModelObject;
import com.android.centrl.models.Contact;
import com.android.centrl.models.DateObject;
import com.android.centrl.models.ListObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static com.android.centrl.utils.AppConstant.DISCORD_CODE;
import static com.android.centrl.utils.AppConstant.GROUP_CODE;
import static com.android.centrl.utils.AppConstant.HANGOUTS_CODE;
import static com.android.centrl.utils.AppConstant.ICQ_CODE;
import static com.android.centrl.utils.AppConstant.MESSENGER_CODE;
import static com.android.centrl.utils.AppConstant.GMAIL_CODE;
import static com.android.centrl.utils.AppConstant.INSTAGRAM_CODE;
import static com.android.centrl.utils.AppConstant.KAKAO_CODE;
import static com.android.centrl.utils.AppConstant.KIK_CODE;
import static com.android.centrl.utils.AppConstant.LINE_CODE;
import static com.android.centrl.utils.AppConstant.OUTLOOK_CODE;
import static com.android.centrl.utils.AppConstant.QQ_CODE;
import static com.android.centrl.utils.AppConstant.REDDIT_CODE;
import static com.android.centrl.utils.AppConstant.SKYPE_CODE;
import static com.android.centrl.utils.AppConstant.SLACK_CODE;
import static com.android.centrl.utils.AppConstant.SMS_CODE;
import static com.android.centrl.utils.AppConstant.TELEGRAM_CODE;
import static com.android.centrl.utils.AppConstant.VIBER_CODE;
import static com.android.centrl.utils.AppConstant.WECHAT_CODE;
import static com.android.centrl.utils.AppConstant.WHATSAPP_CODE;

public class CentrlUtil {
    /**
     * @return package code
     */
    public static Map<String, Integer> getPackageCodeList(Context context) {
        Map<String, Integer> mapPackageList = new HashMap<>();
        mapPackageList.clear();
        mapPackageList.put("com.facebook.orca", MESSENGER_CODE);
        mapPackageList.put("com.facebook.lite", MESSENGER_CODE);
        mapPackageList.put("com.facebook.mlite", MESSENGER_CODE);
        mapPackageList.put("com.whatsapp", WHATSAPP_CODE);
        mapPackageList.put("org.telegram.messenger", TELEGRAM_CODE);
        mapPackageList.put(getDefaultSmsAppPackageName(context), SMS_CODE);
        mapPackageList.put("com.Slack", SLACK_CODE);
        mapPackageList.put("com.skype.raider", SKYPE_CODE);
        mapPackageList.put("com.skype.m2", SKYPE_CODE);
        mapPackageList.put("com.skype.insiders", SKYPE_CODE);
        mapPackageList.put("kik.android", KIK_CODE);
        mapPackageList.put("com.tencent.mm", WECHAT_CODE);
        mapPackageList.put("com.viber.voip", VIBER_CODE);
        mapPackageList.put("com.kakao.talk", KAKAO_CODE);
        mapPackageList.put("jp.naver.line.android", LINE_CODE);
        mapPackageList.put("com.linecorp.linelite", LINE_CODE);
        mapPackageList.put("com.instagram.android", INSTAGRAM_CODE);
        mapPackageList.put("com.google.android.talk", HANGOUTS_CODE);
        mapPackageList.put("com.discord", DISCORD_CODE);
        mapPackageList.put("com.icq.mobile.client", ICQ_CODE);
        mapPackageList.put("com.reddit.frontpage", REDDIT_CODE);
        mapPackageList.put("com.tencent.mobileqq", QQ_CODE);
        mapPackageList.put("com.google.android.gm", GMAIL_CODE);
        mapPackageList.put("com.microsoft.office.outlook", OUTLOOK_CODE);
        return mapPackageList;
    }
    /**
     * @return package name list
     */
    public static ArrayList<String> getAppPackageList(Context context) {
        ArrayList<String> mPackagesList = new ArrayList<>();
        mPackagesList.clear();
        mPackagesList.add("com.facebook.orca");
        mPackagesList.add("com.facebook.lite");
        mPackagesList.add("com.facebook.mlite");
        mPackagesList.add("com.whatsapp");
        mPackagesList.add("org.telegram.messenger");
        mPackagesList.add(getDefaultSmsAppPackageName(context));
        mPackagesList.add("com.Slack");
        mPackagesList.add("com.skype.raider");
        mPackagesList.add("com.skype.m2");
        mPackagesList.add("com.skype.insiders");
        mPackagesList.add("kik.android");
        mPackagesList.add("com.tencent.mm");
        mPackagesList.add("com.viber.voip");
        mPackagesList.add("com.kakao.talk");
        mPackagesList.add("jp.naver.line.android");
        mPackagesList.add("com.linecorp.linelite");
        mPackagesList.add("com.instagram.android");
        mPackagesList.add("com.google.android.talk");
        mPackagesList.add("com.discord");
        mPackagesList.add("com.icq.mobile.client");
        mPackagesList.add("com.reddit.frontpage");
        mPackagesList.add("com.tencent.mobileqq");
        mPackagesList.add("com.google.android.gm");
        mPackagesList.add("com.microsoft.office.outlook");
        return mPackagesList;
    }
    /**
     * @return device default SMS app package name
     */
    @Nullable
    public static String getDefaultSmsAppPackageName(@NonNull Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)
            return Telephony.Sms.getDefaultSmsPackage(context);
        else {
            Intent intent = new Intent(Intent.ACTION_VIEW)
                    .addCategory(Intent.CATEGORY_DEFAULT).setType("vnd.android-dir/mms-sms");
            final List<ResolveInfo> resolveInfos = context.getPackageManager().queryIntentActivities(intent, 0);
            if (resolveInfos != null && !resolveInfos.isEmpty())
                return resolveInfos.get(0).activityInfo.packageName;
            return null;
        }
    }
    /**
     * @return channel logo
     */
    public static HashMap<Integer, Integer> getChannelsLogo() {
        HashMap<Integer, Integer> identityHashMap = new HashMap<>();
        identityHashMap.clear();
        identityHashMap.put(MESSENGER_CODE, R.drawable.ic_messenger);
        identityHashMap.put(WHATSAPP_CODE, R.drawable.ic_whatsapp);
        identityHashMap.put(TELEGRAM_CODE, R.drawable.ic_telegram);
        identityHashMap.put(SMS_CODE, R.drawable.ic_sms);
        identityHashMap.put(SLACK_CODE, R.drawable.ic_slack);
        identityHashMap.put(SKYPE_CODE, R.drawable.ic_skype);
        identityHashMap.put(KIK_CODE, R.drawable.ic_kik);
        identityHashMap.put(WECHAT_CODE, R.drawable.ic_wechat);
        identityHashMap.put(VIBER_CODE, R.drawable.ic_viber);
        identityHashMap.put(KAKAO_CODE, R.drawable.ic_kakaotalk);
        identityHashMap.put(LINE_CODE, R.drawable.ic_line);
        identityHashMap.put(INSTAGRAM_CODE, R.drawable.ic_instagram);
        identityHashMap.put(HANGOUTS_CODE, R.drawable.ic_hangouts);
        identityHashMap.put(DISCORD_CODE, R.drawable.ic_discord);
        identityHashMap.put(ICQ_CODE, R.drawable.ic_icq);
        identityHashMap.put(REDDIT_CODE, R.drawable.ic_reddit);
        identityHashMap.put(QQ_CODE, R.drawable.ic_qq);
        identityHashMap.put(GMAIL_CODE, R.drawable.ic_gmail);
        identityHashMap.put(OUTLOOK_CODE, R.drawable.outlook);
        identityHashMap.put(GROUP_CODE, R.drawable.ic_group_icon);

        return identityHashMap;
    }
    /**
     * @return channel logo
     */
    public static HashMap<Integer, Integer> getChannelsUnActiveLogo() {
        HashMap<Integer, Integer> identityHashMap = new HashMap<>();
        identityHashMap.clear();
        identityHashMap.put(MESSENGER_CODE, R.drawable.ic_messanger_unactive);
        identityHashMap.put(WHATSAPP_CODE, R.drawable.ic_whatsapp_unactive);
        identityHashMap.put(TELEGRAM_CODE, R.drawable.ic_telegram_unactive);
        identityHashMap.put(SMS_CODE, R.drawable.ic_sms_unactive);
        identityHashMap.put(SLACK_CODE, R.drawable.ic_slack_unactive);
        identityHashMap.put(SKYPE_CODE, R.drawable.ic_skype_unactive);
        identityHashMap.put(KIK_CODE, R.drawable.ic_kik_unactive);
        identityHashMap.put(WECHAT_CODE, R.drawable.ic_wechat_unactive);
        identityHashMap.put(VIBER_CODE, R.drawable.ic_viber_unactive);
        identityHashMap.put(KAKAO_CODE, R.drawable.ic_kakotalk_unactive);
        identityHashMap.put(LINE_CODE, R.drawable.ic_line_unactive);
        identityHashMap.put(INSTAGRAM_CODE, R.drawable.ic_instagram_unactive);
        identityHashMap.put(HANGOUTS_CODE, R.drawable.ic_hangout_unactive);
        identityHashMap.put(DISCORD_CODE, R.drawable.ic_discord_unactive);
        identityHashMap.put(ICQ_CODE, R.drawable.ic_icq_unactive);
        identityHashMap.put(REDDIT_CODE, R.drawable.ic_reddit_unactive);
        identityHashMap.put(QQ_CODE, R.drawable.ic_qq_unactive);
        identityHashMap.put(GMAIL_CODE, R.drawable.ic_gmail_unactive);
        identityHashMap.put(OUTLOOK_CODE, R.drawable.ic_outlook_unactive);
        identityHashMap.put(GROUP_CODE, R.drawable.ic_group_icon);

        return identityHashMap;
    }
    /**
     * @return Channel name
     */
    public static HashMap<Integer, String> getChannelsNameHashMap() {
        HashMap<Integer, String> identityNameHashMap = new HashMap<>();
        identityNameHashMap.clear();
        identityNameHashMap.put(MESSENGER_CODE, "Messenger");
        identityNameHashMap.put(WHATSAPP_CODE, "WhatsApp");
        identityNameHashMap.put(TELEGRAM_CODE, "Telegram");
        identityNameHashMap.put(SMS_CODE, "SMS");
        identityNameHashMap.put(SLACK_CODE, "Slack");
        identityNameHashMap.put(SKYPE_CODE, "Skype");
        identityNameHashMap.put(KIK_CODE, "Kik");
        identityNameHashMap.put(WECHAT_CODE, "WeChat");
        identityNameHashMap.put(VIBER_CODE, "Viber");
        identityNameHashMap.put(KAKAO_CODE, "KakaoTalk");
        identityNameHashMap.put(LINE_CODE, "Line");
        identityNameHashMap.put(INSTAGRAM_CODE, "Instagram");
        identityNameHashMap.put(HANGOUTS_CODE, "Hangouts");
        identityNameHashMap.put(DISCORD_CODE, "Discord");
        identityNameHashMap.put(ICQ_CODE, "ICQ New");
        identityNameHashMap.put(REDDIT_CODE, "Reddit");
        identityNameHashMap.put(QQ_CODE, "QQ");
        identityNameHashMap.put(GMAIL_CODE, "Gmail");
        identityNameHashMap.put(OUTLOOK_CODE, "Outlook");
        return identityNameHashMap;
    }
    /**
     * @return channel package code
     */
    public static HashMap<String, Integer> getChannelsPackageCodeHashMap() {
        HashMap<String, Integer> identityNameHashMap = new HashMap<>();
        identityNameHashMap.clear();
        identityNameHashMap.put("Messenger", MESSENGER_CODE);
        identityNameHashMap.put("WhatsApp", WHATSAPP_CODE);
        identityNameHashMap.put("Telegram", TELEGRAM_CODE);
        identityNameHashMap.put("SMS", SMS_CODE);
        identityNameHashMap.put("Slack", SLACK_CODE);
        identityNameHashMap.put("Skype", SKYPE_CODE);
        identityNameHashMap.put("Kik", KIK_CODE);
        identityNameHashMap.put("WeChat", WECHAT_CODE);
        identityNameHashMap.put("Viber", VIBER_CODE);
        identityNameHashMap.put("KakaoTalk", KAKAO_CODE);
        identityNameHashMap.put("Line", LINE_CODE);
        identityNameHashMap.put("Instagram", INSTAGRAM_CODE);
        identityNameHashMap.put("Hangouts", HANGOUTS_CODE);
        identityNameHashMap.put("Discord", DISCORD_CODE);
        identityNameHashMap.put("ICQ New", ICQ_CODE);
        identityNameHashMap.put("Reddit", REDDIT_CODE);
        identityNameHashMap.put("QQ", QQ_CODE);
        identityNameHashMap.put("Gmail", GMAIL_CODE);
        identityNameHashMap.put("Outlook", OUTLOOK_CODE);
        return identityNameHashMap;
    }
    /**
     * @return phone contact details
     */
    public static Contact getContactDetailsByNumber(String number, Context context) {
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(number));
        Contact contact = new Contact();

        ContentResolver contentResolver = context.getContentResolver();
        Cursor contactLookup = contentResolver.query(uri, null, null, null, null);

        try {
            if (contactLookup != null && contactLookup.getCount() > 0) {
                contactLookup.moveToNext();
                String name = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                //String id = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.CONTACT_ID));
                String photo = contactLookup.getString(contactLookup.getColumnIndex(ContactsContract.Data.PHOTO_URI));
                String contactId = contactLookup.getString(contactLookup.getColumnIndex(BaseColumns._ID));
                //Log.i("contactId", "contactId..." + contactId+" ...contactDisplayName..."+name+" ...photoURI..."+photo);
                contact.setName(name);
                contact.setProfilePath(photo);
                contact.setId(contactId);
            } else {
                contact = null;
            }
        } finally {
            if (contactLookup != null) {
                contactLookup.close();
            }
        }

        return contact;
    }
    /**
     * @return grouped data list
     */
    public static List<ListObject> groupDataIntoHashMap(List<ChatsEntity> chatModelList) {
        LinkedHashMap<String, Set<ChatsEntity>> groupedHashMap = new LinkedHashMap<>();
        Set<ChatsEntity> list = null;
        for (ChatsEntity chatModel : chatModelList) {
            String hashMapKey = DateUtil.INSTANCE.convertDateToString(chatModel.getTimeStamp());
            if (groupedHashMap.containsKey(hashMapKey)) {
                // The key is already in the HashMap; add the pojo object
                // against the existing key.
                groupedHashMap.get(hashMapKey).add(chatModel);
            } else {
                // The key is not there in the HashMap; create a new key-value pair
                list = new LinkedHashSet<>();
                list.add(chatModel);
                groupedHashMap.put(hashMapKey, list);
            }
        }
        //Generate list from map
        return generateListFromMap(groupedHashMap);
    }
    /**
     * @return list from map
     */
    private static List<ListObject> generateListFromMap(LinkedHashMap<String, Set<ChatsEntity>> groupedHashMap) {
        // We linearly add every item into the consolidatedList.
        List<ListObject> consolidatedList = new ArrayList<>();
        for (String date : groupedHashMap.keySet()) {
            DateObject dateItem = new DateObject();
            dateItem.date = date;
            consolidatedList.add(dateItem);
            for (ChatsEntity chatModel : groupedHashMap.get(date)) {
                ChatModelObject generalItem = new ChatModelObject();
                generalItem.chatModel = chatModel;
                consolidatedList.add(generalItem);
            }
        }
        return consolidatedList;
    }
    /**
     * @return root path for store
     */
    public static String getRootDirPath(Context context) {
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            File file = ContextCompat.getExternalFilesDirs(context.getApplicationContext(),
                    null)[0];
            return file.getAbsolutePath();
        } else {
            return context.getApplicationContext().getFilesDir().getAbsolutePath();
        }
    }
    /**
     * @return return contacts image
     */
    public static String getContactImage(Context context, String imagePath, String id) {
        String cImagePath = null;
        try {
            Point screenSize = ImageUtils.getScreenSize(context);
            Bitmap scaledBitmap = null;
            try {
                scaledBitmap = ImageUtils.decodeUriToScaledBitmap(context, Uri.parse(imagePath), screenSize.x, screenSize.y);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap circularBitmap = ImageUtils.getCircularBitmap(scaledBitmap);
            cImagePath = saveImage(context, circularBitmap, id);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cImagePath;
    }
    /**
     * @return store the contacts image to temp folder
     */
    private static String saveImage(Context context, Bitmap finalBitmap, String fileName) {
        String appPath = context.getFilesDir().getAbsolutePath();
        File myDir = new File(appPath + "/contacts_images");
        myDir.mkdirs();
        String fName = fileName + ".png";
        File file = new File(myDir, fName);
        String profilePath = file.getAbsolutePath();
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.PNG, 0, out);
            out.flush();
            out.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return profilePath;
    }

    /**
     * @return return the file path for wallpaper
     */
    public static String getCropPath(Context context) {
        String appPath = context.getFilesDir().getAbsolutePath();
        File myDir = new File(appPath + "/wallpaper_images");
        myDir.mkdirs();
        String fName = "centrl_wallpaper" + ".png";
        File file = new File(myDir, fName);
        String profilePath = file.getAbsolutePath();
        if (file.exists()) file.delete();
        return profilePath;
    }

    /**
     * detect duplicate in array by comparing size of List and Set
     * since Set doesn't contain duplicate, size must be less for an array which contains duplicates
     */
    public static boolean checkDuplicateUsingSet(ArrayList<String> input) {
        Set inputSet = new HashSet(input);
        return inputSet.size() < input.size();
    }
    /**
     * Get the unique number for checking premium users
     */
    public synchronized static String getUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Tells if internet is currently available on the device
     *
     * @param currentContext
     * @return
     */
    public static boolean isInternetAvailable(Context currentContext) {
        ConnectivityManager conectivityManager = (ConnectivityManager) currentContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = conectivityManager.getActiveNetworkInfo();
        if (networkInfo != null) {
            if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
                if (networkInfo.isConnected()) {
                    return true;
                }
            } else if (networkInfo.getType() == ConnectivityManager.TYPE_MOBILE) {
                if (networkInfo.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static String getJsonFromAssets(Context context, String fileName) {
        String jsonString;
        try {
            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            jsonString = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return jsonString;
    }
}
