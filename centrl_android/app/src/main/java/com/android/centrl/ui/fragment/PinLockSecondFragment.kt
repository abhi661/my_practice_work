package com.android.centrl.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.android.centrl.R
import com.android.centrl.databinding.FragmentPinLockSecondBinding
import com.android.centrl.ui.activity.InteractionsActivity
import com.android.centrl.ui.views.pinlockview.IndicatorDots
import com.android.centrl.ui.views.pinlockview.PinLockListener
import com.android.centrl.utils.AppConstant
import com.google.android.material.snackbar.Snackbar

/**
 * Confirm pin lock fragment
 */
class PinLockSecondFragment : Fragment() {

    private lateinit var binding: FragmentPinLockSecondBinding
    private var mEnteredPin = ""
    private var editor: SharedPreferences.Editor? = null
    /**
     * Pin lock listener
     */
    private val mPinLockListener: PinLockListener = object : PinLockListener {
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onComplete(pin: String) {
            if (mFirstPin.equals(pin, ignoreCase = true)) {
                mEnteredPin = pin
                editor!!.putString(AppConstant.PIN_LOCK, pin)
                editor!!.putInt(AppConstant.LOCK_TYPE, AppConstant.PIN_LOCK_CODE)
                binding.pinConfirmSp.setBackgroundResource(R.drawable.allow_btn_bg)
                binding.pinConfirmSp.isEnabled = true
                binding.pinMsgTxtSp.text = resources.getString(R.string.set_up_pin_lock_confirm_txt)
                binding.pinMsgTxtSp.setTextColor(resources.getColor(R.color.security_text, null))
            } else {
                binding.pinLockViewSp.resetPinLockView()
                binding.pinMsgTxtSp.text =  resources.getString(R.string.not_match)
                binding.pinMsgTxtSp.setTextColor(resources.getColor(R.color.security_error, null))
            }
        }

        @RequiresApi(Build.VERSION_CODES.M)
        override fun onEmpty() {
            mEnteredPin = ""
            binding.pinConfirmSp.setBackgroundResource(R.drawable.disallow_btn_bg)
            binding.pinConfirmSp.isEnabled = false
            binding.pinMsgTxtSp.text = resources.getString(R.string.set_up_pin_lock_confirm_txt)
            binding.pinMsgTxtSp.setTextColor(resources.getColor(R.color.security_text, null))
        }

        override fun onPinChange(pinLength: Int, intermediatePin: String) {}
    }

    @SuppressLint("CommitPrefEdits")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val preferences = requireActivity().getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
        editor = preferences!!.edit()
        if (arguments != null) {
            mFirstPin = requireArguments().getString(ARG_PARAM1)
            isRestPin = requireArguments().getBoolean(ARG_PARAM2)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pin_lock_second, container, false)
        val view = binding.root

        binding.pinConfirmSp.setBackgroundResource(R.drawable.disallow_btn_bg)
        binding.pinConfirmSp.isEnabled = false
        binding.pinLockViewSp.attachIndicatorDots(binding.indicatorDotsSp)
        binding.pinLockViewSp.setPinLockListener(mPinLockListener)
        //mPinLockView.setCustomKeySet(new int[]{2, 3, 1, 5, 9, 6, 7, 0, 8, 4});
        //mPinLockView.enableLayoutShuffling();
        binding.pinLockViewSp.pinLength = 4
        binding.pinLockViewSp.textColor = ContextCompat.getColor(requireActivity(), R.color.security_text)
        binding.pinLockViewSp.deleteButtonDrawable = resources.getDrawable(R.drawable.ic_delete, null)
        binding.indicatorDotsSp.indicatorType = IndicatorDots.IndicatorType.FILL_WITH_ANIMATION
        binding.pinConfirmSp.setOnClickListener {
            if (mEnteredPin.isNotEmpty()) {
                if (mEnteredPin.length == 4) {
                    if (mEnteredPin.equals(mFirstPin, ignoreCase = true)) {
                        editor!!.commit()
                        if(isRestPin){
                            val intent = Intent(requireActivity(), InteractionsActivity::class.java)
                            startActivity(intent)
                        }
                        requireActivity().finish()
                    } else {
                        binding.pinLockViewSp.resetPinLockView()
                        binding.pinMsgTxtSp.text = resources.getString(R.string.not_match)
                    }
                } else {
                    Snackbar.make(binding.pinLockViewSp, resources.getString(R.string.enter_correct), Snackbar.LENGTH_LONG).show()
                }
            }
        }
        binding.pinCancelSp.setOnClickListener {
            editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
            editor!!.commit()
            requireActivity().finish()
        }
        return view
    }

    companion object {
        const val TAG = "PinLockSecondFragment"
        private const val ARG_PARAM1 = "param1"
        private const val ARG_PARAM2 = "param2"
        private var mFirstPin: String? = null
        private var isRestPin: Boolean = false
        fun newInstance(pin: String?, resetPin: Boolean): PinLockSecondFragment {
            mFirstPin = pin
            isRestPin = resetPin
            val fragment = PinLockSecondFragment()
            val args = Bundle()
            args.putString(ARG_PARAM1, pin)
            args.putBoolean(ARG_PARAM2, resetPin)
            fragment.arguments = args
            return fragment
        }
    }
}