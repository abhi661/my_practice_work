package com.android.centrl.ui.activity

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.android.centrl.R
import com.android.centrl.databinding.ActivityPermissionBinding
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.Utilities

/**
 * Permissions screen for Notification, SMS, Contact, Camera and Storage access.
 */
class PermissionActivity : BaseActivity() {
    private var utilities: Utilities? = null
    private var enableNotificationListenerAlertDialog: AlertDialog? = null
    private var isFirstTime = false
    private lateinit var binding: ActivityPermissionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_permission)
        binding.clickHandler = PermissionClickHandlers()
        utilities = Utilities()
    }

    inner class PermissionClickHandlers{
        fun onClickListener(view: View) {
            when(view.id){
                R.id.allow_btn ->{
                    isFirstTime = true
                    if (requestContacts()) {
                        if (!utilities!!.isNotificationServiceEnabled(this@PermissionActivity)) {
                            enableNotificationListenerAlertDialog = buildNotificationServiceAlertDialog(this@PermissionActivity)
                            enableNotificationListenerAlertDialog!!.show()
                            enableNotificationListenerAlertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                enableNotificationListenerAlertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                            } else {
                                enableNotificationListenerAlertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor(AppConstant.CONTINUE_BUTTON))
                            }
                        } else {
                            startActivity(Intent(this@PermissionActivity, AccountSetupActivity::class.java))
                            finish()
                        }
                    }
                }
                R.id.quit_btn ->{
                    editor!!.putBoolean(AppConstant.PERMISSION_SCREEN, false)
                    editor!!.commit()
                    finish()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (!utilities!!.isNotificationServiceEnabled(this)) {
            if (isFirstTime) {
                enableNotificationListenerAlertDialog = buildNotificationServiceAlertDialog(this)
                enableNotificationListenerAlertDialog!!.show()
                enableNotificationListenerAlertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    enableNotificationListenerAlertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                } else {
                    enableNotificationListenerAlertDialog!!.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                }
            }
        } else {
            if (requestReceiveSMS()) {
                startActivity(Intent(this@PermissionActivity, AccountSetupActivity::class.java))
                finish()
            }
        }
    }
    /**
     * Request permissions for Notification, SMS, Contact, Camera and Storage access.
     */
    private fun requestContacts(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                arrayOf(
                    Manifest.permission.READ_CONTACTS,
                    Manifest.permission.WRITE_CONTACTS,
                    Manifest.permission.SEND_SMS,
                    Manifest.permission.RECEIVE_SMS,
                    Manifest.permission.READ_PHONE_STATE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), PERMISSIONS_REQUEST_READ_CONTACTS
            )
            false
        } else {
            true
        }
    }
    /**
     * Request permissions for SMS access.
     */
    private fun requestReceiveSMS(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)) {
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.sms_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                    PERMISSIONS_REQUEST_READ_CONTACTS.openPermissionSettings(this@PermissionActivity)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor(AppConstant.CONTINUE_BUTTON))
                }
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.READ_SMS,
                        Manifest.permission.RECEIVE_SMS,
                        Manifest.permission.SEND_SMS
                    ),
                    PERMISSIONS_REQUEST_READ_CONTACTS
                )
            }
            false
        } else {
            true
        }
    }
    /**
     * Open permissions screen forcefully for SMS access.
     */
    private fun Int.openPermissionSettings(activity: Activity) {
        if (this == PERMISSIONS_REQUEST_READ_CONTACTS) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.parse("package:" + activity.packageName)
            intent.data = uri
            if (intent.resolveActivity(activity.packageManager) != null)
                activity.startActivity(intent)
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            editor!!.putBoolean(AppConstant.PERMISSION_SCREEN, false)
            editor!!.commit()
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {
        private const val PERMISSIONS_REQUEST_READ_CONTACTS = 100
    }
}