package com.android.centrl.persistence.injections

import android.app.Application
import android.content.Context
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.datasource.BlockedContactsDataSource
import com.android.centrl.persistence.repositry.BlockedContactsRepository
import com.android.centrl.viewmodel.BlockedContactsViewModelFactory

/**
 * Enables injection of data sources.
 */
object BlockedContactsInjection {
    /**
     * Provide centrl data source
     */
    private fun provideCentrlDataSource(context: Context?): BlockedContactsDataSource {
        val database = CentrlDatabase.getInstance(context!!)
        return BlockedContactsRepository(database.bockedContactsDao())
    }
    /**
     * Provide view model factory
     */
    fun provideViewModelFactory(application: Application, context: Context?): BlockedContactsViewModelFactory {
        val dataSource = provideCentrlDataSource(context)
        return BlockedContactsViewModelFactory(application, dataSource)
    }
}