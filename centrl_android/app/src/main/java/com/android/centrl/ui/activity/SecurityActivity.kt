package com.android.centrl.ui.activity

import android.Manifest
import android.app.KeyguardManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import com.an.biometric.BiometricCallback
import com.an.biometric.BiometricManager
import com.an.biometric.BiometricManager.BiometricBuilder
import com.android.centrl.R
import com.android.centrl.databinding.ActivitySecurityBinding
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.google.android.gms.ads.AdView
import me.aflak.libraries.view.Fingerprint

/**
 * Security lock setup activity.
 */
class SecurityActivity : BaseActivity(), BiometricCallback {

    private lateinit var binding: ActivitySecurityBinding
    private var mAdView: AdView? = null
    private var mBiometricManager: BiometricManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_security)
        binding.clickHandler = ClickHandlers()

        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
        if (checkBiometricSupport()) {
            binding.fingerLockLl.visibility = View.VISIBLE
        }

        binding.adsContainerSecurity.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@SecurityActivity, binding.adsContainerSecurity.adViewContainer)
        }
    }
    /**
     * View click switch handler class.
     */
    inner class ClickHandlers {
        /**
         * Handling click event.
         */
        @RequiresApi(Build.VERSION_CODES.M)
        fun onClick(view: View) {
            when (view.id) {
                R.id.security_back -> { finish() }
                R.id.pin_lock_ll -> {
                    val pinIntent = Intent(this@SecurityActivity, PinLockActivity::class.java)
                    startActivity(pinIntent)
                }
                R.id.pattern_lock_ll -> {
                    val patternIntent = Intent(this@SecurityActivity, PatternLockActivity::class.java)
                    startActivity(patternIntent)
                }
                R.id.finger_lock_ll -> {
                    try {
                        if (Fingerprint.isAvailable(this@SecurityActivity)) {
                            if(preferences!!.getString(AppConstant.PIN_LOCK, null).isNullOrEmpty()){
                                editor!!.putBoolean(AppConstant.SETUP_FINGER, true)
                                editor!!.commit()
                                val pinIntent = Intent(this@SecurityActivity, PinLockActivity::class.java)
                                startActivity(pinIntent)
                            } else {
                                mBiometricManager = BiometricBuilder(this@SecurityActivity)
                                    .setTitle(getString(R.string.biometric_title))
                                    .setSubtitle("")
                                    .setDescription(getString(R.string.biometric_description))
                                    .setNegativeButtonText(getString(R.string.biometric_negative_button_text))
                                    .build()
                                //start authentication
                                mBiometricManager!!.authenticate(this@SecurityActivity)
                            }
                        } else {
                            mBiometricManager = BiometricBuilder(this@SecurityActivity)
                                .setTitle(getString(R.string.biometric_title))
                                .setSubtitle("")
                                .setDescription(getString(R.string.biometric_description))
                                .setNegativeButtonText(getString(R.string.biometric_negative_button_text))
                                .build()
                            //start authentication
                            mBiometricManager!!.authenticate(this@SecurityActivity)
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                R.id.face_lock_ll -> {
                    //
                }
                R.id.lock_none_ll -> {
                    editor!!.putInt(AppConstant.LOCK_TYPE, AppConstant.NONE_LOCK_CODE)
                    editor!!.putString(AppConstant.PIN_LOCK, "")
                    editor!!.putString(AppConstant.PATTERN_LOCK, "")
                    editor!!.commit()
                    binding.noneRadioBtn.isChecked = true
                    binding.pinRadioBtn.isChecked = false
                    binding.patternRadio.isChecked = false
                    binding.fingerRadio.isChecked = false
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        try {
            if(preferences!!.getBoolean(AppConstant.SETUP_FINGER, false)){
                try {
                    mBiometricManager = BiometricBuilder(this@SecurityActivity)
                        .setTitle(getString(R.string.biometric_title))
                        .setSubtitle("")
                        .setDescription(getString(R.string.biometric_description))
                        .setNegativeButtonText(getString(R.string.biometric_negative_button_text))
                        .build()
                        //start authentication
                    mBiometricManager!!.authenticate(this@SecurityActivity)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                when (preferences!!.getInt(AppConstant.LOCK_TYPE, 0)) {
                    AppConstant.NONE_LOCK_CODE -> {
                        binding.noneRadioBtn.isChecked = true
                        binding.pinRadioBtn.isChecked = false
                        binding.patternRadio.isChecked = false
                        binding.fingerRadio.isChecked = false
                    }
                    AppConstant.PIN_LOCK_CODE -> {
                        binding.noneRadioBtn.isChecked = false
                        binding.pinRadioBtn.isChecked = true
                        binding.patternRadio.isChecked = false
                        binding.fingerRadio.isChecked = false
                    }
                    AppConstant.PATTERN_LOCK_CODE -> {
                        binding.noneRadioBtn.isChecked = false
                        binding.pinRadioBtn.isChecked = false
                        binding.patternRadio.isChecked = true
                        binding.fingerRadio.isChecked = false
                    }
                    AppConstant.FINGER_LOCK_CODE -> {
                        binding.noneRadioBtn.isChecked = false
                        binding.pinRadioBtn.isChecked = false
                        binding.patternRadio.isChecked = false
                        binding.fingerRadio.isChecked = true
                    }
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adsContainerSecurity.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
    /**
     * Check biometric available or not in device
     */
    private fun checkBiometricSupport(): Boolean {
        try{
            val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            val packageManager = this.packageManager
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                //This Android version does not support fingerprint authentication.
                return false
            }
            if (!packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
                //Fingerprint Sensor not supported.
                return false
            }
            if (!keyguardManager.isKeyguardSecure) {
                //Lock screen security not enabled in Settings
                return false
            }
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                ActivityCompat.checkSelfPermission(this, Manifest.permission.USE_BIOMETRIC) == PackageManager.PERMISSION_GRANTED
            } else {
                return false
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
      return false
    }
    /**
     * Finger version version not supported.
     */
    override fun onSdkVersionNotSupported() {
        binding.fingerLockLl.visibility = View.GONE
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication not supported.
     */
    override fun onBiometricAuthenticationNotSupported() {
        binding.fingerLockLl.visibility = View.GONE
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication not available.
     */
    override fun onBiometricAuthenticationNotAvailable() {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val intent = Intent(Settings.ACTION_SECURITY_SETTINGS)
            startActivityForResult(intent, REQUEST_CODE_SECURITY_SETTINGS)
        }
    }
    /**
     * Finger authentication permission not granted.
     */
    override fun onBiometricAuthenticationPermissionNotGranted() {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication internal error.
     */
    override fun onBiometricAuthenticationInternalError(error: String) {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication failed.
     */
    override fun onAuthenticationFailed() {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication cancelled.
     */
    override fun onAuthenticationCancelled() {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication successful.
     */
    override fun onAuthenticationSuccessful() {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.putInt(AppConstant.LOCK_TYPE, AppConstant.FINGER_LOCK_CODE)
        editor!!.commit()
        binding.noneRadioBtn.isChecked = false
        binding.pinRadioBtn.isChecked = false
        binding.patternRadio.isChecked = false
        binding.fingerRadio.isChecked = true
    }
    /**
     * Finger authentication help.
     */
    override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence) {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }
    /**
     * Finger authentication error.
     */
    override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
        editor!!.putBoolean(AppConstant.SETUP_FINGER, false)
        editor!!.commit()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {
        private const val REQUEST_CODE_SECURITY_SETTINGS = 100
    }
}