package com.android.centrl.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.centrl.persistence.entity.InstalledAppsEntity

/**
 * Data Access Object for the installed Apps table.
 */
@Dao
interface InstalledAppsDao {
    /**
     * Insert a installed apps details in the database. If the installed apps details already exists, replace it.
     *
     * @param installedAppsEntity the installed apps details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertInstalledApps(installedAppsEntity: InstalledAppsEntity)

    /**
     *
     * @return the package name from the table
     */
    @Query("SELECT packagesName FROM installedApps WHERE packageCode = :code")
    fun getInstalledPackageName(code: Int): String

    /**
     * @return the installed app package name from the table
     */
    @Query("SELECT packagesName FROM installedApps")
    fun getAllInstalledPackageName(): List<String>

    /**
     * Delete package from table.
     */
    @Query("DELETE FROM installedApps WHERE packageCode = :code")
    fun deleteInstalledApps(code: Int)
}