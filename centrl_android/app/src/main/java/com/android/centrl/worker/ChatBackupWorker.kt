package com.android.centrl.worker

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.android.centrl.persistence.entity.ChatBackupEntity
import com.android.centrl.gdrive.DriveServiceHelper
import com.android.centrl.persistence.database.CentrlBackupDatabase
import com.android.centrl.persistence.repositry.BackupRepository
import com.android.centrl.utils.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.gson.Gson
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import org.json.JSONException
import org.json.JSONObject
import io.reactivex.schedulers.Schedulers

/**
 * Backup daily or weekly in background
 */
class ChatBackupWorker(val context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters) {

    private var mDriveServiceHelper: DriveServiceHelper? = null
    private var account: GoogleSignInAccount? = null
    private var backupRepository: BackupRepository = BackupRepository(context)
    private lateinit var chatBackupEntity: ChatBackupEntity
    private var mDataBaseSize: String? = null

    private var mBackupOldId: String? = null
    private var mBackupTime: String? = null
    var preferences: SharedPreferences? = null
    var editor: SharedPreferences.Editor? = null
    private lateinit var mContext: Context

    @SuppressLint("CommitPrefEdits")
    override fun doWork(): Result {
        try {
            mContext = context
            chatBackupEntity = backupRepository.getBackupDetails()
            preferences = context.getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
            editor = preferences!!.edit()
            mDataBaseSize = chatBackupEntity.dbSize
            account = GoogleSignIn.getLastSignedInAccount(context)
            mBackupOldId = chatBackupEntity.backupId
            mBackupTime = chatBackupEntity.backupType

            if (account != null) {
                mDriveServiceHelper = DriveServiceHelper(DriveServiceHelper.getGoogleDriveService(context, account!!, "Centrl"))
                if(CentrlUtil.isInternetAvailable(mContext)){
                    if(preferences!!.getBoolean(AppConstant.FIRST_BACKUP, false)){
                        editor!!.putBoolean(AppConstant.FIRST_BACKUP, false)
                        editor!!.commit()
                    } else {
                        uploadBackUp(chatBackupEntity.emailID, chatBackupEntity.userName)
                    }
                }
            }

        } catch (unused: Exception) {
            unused.printStackTrace()
        }
        return Result.success()
    }

    /**
     * This will upload backup database to google drive
     */
    private fun uploadBackUp(emailId: String?, userName: String?) {
        try {
            mDataBaseSize = bytesIntoHumanReadable(mContext.getDatabasePath(AppConstant.DATA_BASE).length())
            if (mDriveServiceHelper == null) {
                return
            }
            mDriveServiceHelper!!.uploadFile(mContext.getDatabasePath(AppConstant.DATA_BASE), DriveServiceHelper.MIME_TYPE, null)
                .addOnSuccessListener { googleDriveFileHolder ->
                    val result = Gson()
                    val getResult = result.toJson(googleDriveFileHolder)
                    try {
                        val jsonObject = JSONObject(getResult)
                        val id = jsonObject.getString("id")
                        deleteUploadedFile(id, emailId, userName, mBackupOldId)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                .addOnFailureListener { }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * This will delete previews backup database from google drive
     */
    private fun deleteUploadedFile(newFileId: String, emailId: String?, userName: String?, oldID: String?){
        try {
            val chatBackup = ChatBackupEntity(DateUtil.currentDateTime!!, emailId!!, userName!!, mBackupTime!!, newFileId, mDataBaseSize!!)
            if(oldID != null){
                mDriveServiceHelper!!.deleteFolderFile(oldID)
                    .addOnSuccessListener {
                        editor!!.putString(AppConstant.OLD_BACKUP_ID, newFileId)
                        editor!!.commit()
                        updateBackupDetails(chatBackup)
                    }
                    .addOnFailureListener {
                        editor!!.putString(AppConstant.OLD_BACKUP_ID, newFileId)
                        editor!!.commit()
                        updateBackupDetails(chatBackup)
                    }
            } else {
                editor!!.putString(AppConstant.OLD_BACKUP_ID, newFileId)
                editor!!.commit()
                saveBackupDetails(chatBackup)
            }

        }catch (e: Exception){
            e.printStackTrace()
        }
    }
    /**
     * This will store backup database details to server
     */
    private fun saveBackupDetails(chatBackup: ChatBackupEntity){
        try{
            Completable.fromAction {

                CentrlBackupDatabase.getInstance(mContext).chatBackupDao().insertBackupDetails(chatBackup)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
    /**
     * This will store backup database details to server
     */
    private fun updateBackupDetails(chatBackup: ChatBackupEntity){
        try{
            Completable.fromAction {
                CentrlBackupDatabase.getInstance(mContext).chatBackupDao().updateChatBackupDetailsBG(chatBackup.lastBackupTime, chatBackup.emailID, chatBackup.userName, chatBackup.backupType, chatBackup.backupId, chatBackup.dbSize)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        }catch (e: Exception){
            e.printStackTrace()
        }
    }

    /**
     * This will convert to KB,MB,GB format
     */
    private fun bytesIntoHumanReadable(bytes: Long): String {
        val kilobyte: Long = 1024
        val megabyte = kilobyte * 1024
        val gigabyte = megabyte * 1024
        val terabyte = gigabyte * 1024
        return when {
            bytes in 0 until kilobyte -> {
                bytes.toString() + "B"
            }
            bytes in kilobyte until megabyte -> {
                (bytes / kilobyte).toString() + "KB"
            }
            bytes in megabyte until gigabyte -> {
                (bytes / megabyte).toString() + "MB"
            }
            bytes in gigabyte until terabyte -> {
                (bytes / gigabyte).toString() + "GB"
            }
            bytes >= terabyte -> {
                (bytes / terabyte).toString() + "TB"
            }
            else -> {
                "$bytes Bytes"
            }
        }
    }
}