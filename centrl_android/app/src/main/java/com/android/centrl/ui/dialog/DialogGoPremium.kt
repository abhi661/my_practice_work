package com.android.centrl.ui.dialog

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.android.centrl.R
import com.android.centrl.databinding.DialogGoPremiumBinding
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.ui.activity.CheckOutActivity
import com.android.centrl.utils.AppConstant

/**
 * Premium dialog
 */
class DialogGoPremium : DialogFragment() {

    private lateinit var binding: DialogGoPremiumBinding
    private lateinit var dialogGoPremiumListener: DialogGoPremiumListener
    /**
     * Go Premium interface
     */
    interface DialogGoPremiumListener {
        fun onCancel()
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater, R.layout.dialog_go_premium, null, false)
        dialog.setContentView(binding.root)

        binding.closeGoPremium.setOnClickListener {
            dialogGoPremiumListener.onCancel()
            PrefManager.getInstance(requireActivity())!!.saveBoolean(AppConstant.PREMIUM, false)
            dialog.dismiss()
        }
        binding.subscriptionBtn.setOnClickListener {
            val intent = Intent(activity, CheckOutActivity::class.java)
            requireActivity().startActivity(intent)
            dialog.dismiss()
        }
        return dialog
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogGoPremiumListener = activity as DialogGoPremiumListener
    }

    companion object {
        var TAG = "DialogGoPremium"
    }
}