package com.android.centrl.persistence.repositry

import com.android.centrl.persistence.dao.KeyboardAppTrayDao
import com.android.centrl.persistence.dao.RemovedKeyboardAppsDao
import com.android.centrl.persistence.datasource.KeyBoardAppsTrayDataSource
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import com.android.centrl.persistence.entity.RemovedKeyboardAppsEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Using the Room database as a data source.
 */
class KeyboardAppsTrayRepository(
    private val keyboardAppTrayDao: KeyboardAppTrayDao,
    private val removedKeyboardAppsDao: RemovedKeyboardAppsDao
) : KeyBoardAppsTrayDataSource {
    /**
     * @return the keyboard apps details from the table
     */
    override fun getAllKeyboardAppTrayPackName(): Flowable<List<String>> {
        return keyboardAppTrayDao.getAllKeyboardAppTrayPackName()
    }

    /**
     * @return the keyboard apps details from the table
     */
    override fun getAllKeyboardAppTrayPackCode(): Flowable<List<Int>> {
        return keyboardAppTrayDao.getAllKeyboardAppTrayPackCode()
    }

    /**
     * Insert a keyboard apps details in the database. If the keyboard apps details already exists, replace it.
     *
     * @param keyboardAppsTrayEntity the keyboard apps details to be inserted.
     */
    override fun insertKeyboardAppTray(keyboardAppsTrayEntity: KeyboardAppsTrayEntity): Completable {
        return keyboardAppTrayDao.insertKeyboardAppTray(keyboardAppsTrayEntity)
    }

    /**
     * Delete apps from keyboard apps tray.
     */
    override fun deleteKeyboardAppsTray(code: Int) {
        keyboardAppTrayDao.deleteKeyboardAppsTray(code)
    }

    /**
     * @return the removed keyboard apps details from the table
     */
    override fun getAllRemovedChatAppPackages(): Flowable<List<Int>> {
        return removedKeyboardAppsDao.getAllRemovedChatAppPackages()
    }

    /**
     * Insert a removed keyboard apps details in the database. If the removed keyboard apps details already exists, replace it.
     *
     * @param removedKeyboardAppsEntity the removed keyboard apps details to be inserted.
     */
    override fun insertRemovedKeyboardApps(removedKeyboardAppsEntity: RemovedKeyboardAppsEntity): Completable {
        return removedKeyboardAppsDao.insertRemovedKeyboardApps(removedKeyboardAppsEntity)
    }

    /**
     * Delete apps from removed keyboard tray.
     */
    override fun deleteKeyboardApps(code: Int) {
        removedKeyboardAppsDao.deleteKeyboardApps(code)
    }
}