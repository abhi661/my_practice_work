package com.android.centrl.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * View Model for the [com.android.centrl.ui.activity.GeneralActivity]
 */
class SwitchEDViewModel : ViewModel() {

    private val mPremium = MutableLiveData<Boolean>()
    private val mVibration = MutableLiveData<Boolean>()
    private val mLightUp = MutableLiveData<Boolean>()
    private val mSound = MutableLiveData<Boolean>()
    private val mShowPublic = MutableLiveData<Boolean>()

    fun setGoPremium(isPremium: Boolean) {
        mPremium.value = isPremium
    }

    val goPremium: LiveData<Boolean>
        get() = mPremium

    fun setNotiVibration(isVibrate: Boolean) {
        mVibration.value = isVibrate
    }

    val notiVibration: LiveData<Boolean>
        get() = mVibration

    fun setNotiLightUp(isLightUp: Boolean) {
        mLightUp.value = isLightUp
    }

    val notiLightUp: LiveData<Boolean>
        get() = mLightUp

    fun setNotiSound(isSound: Boolean) {
        mSound.value = isSound
    }

    val notiSound: LiveData<Boolean>
        get() = mSound

    fun setNotiVisibility(isVisible: Boolean) {
        mShowPublic.value = isVisible
    }

    val notiVisibility: LiveData<Boolean>
        get() = mShowPublic
}