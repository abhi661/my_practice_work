package com.android.centrl.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.interfaces.SelectChannel
import com.android.centrl.R
import com.android.centrl.adapter.KeyBoardChannelListAdapter.MessageItemViewHolder
import com.android.centrl.utils.CentrlUtil
import java.util.*
/**
 * Keyboard tray channels list adapter
 */
class KeyBoardChannelListAdapter(private val messageList: ArrayList<Int>, var selectIdentity: SelectChannel) : RecyclerView.Adapter<MessageItemViewHolder>() {

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MessageItemViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.chat_item, viewGroup, false)
        return MessageItemViewHolder(itemView)
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: MessageItemViewHolder, position: Int) {
        val packCode = messageList[position]
        if (packCode == 40) {
            holder.imageLeft.setImageResource(R.drawable.ic_un_filter)
        } else {
            if(packCode != 0){
                holder.imageLeft.setImageResource(CentrlUtil.getChannelsLogo()[packCode]!!)
            }
        }
        holder.imageLeft.setOnClickListener { selectIdentity.channelSelected(messageList[position])
        }
    }

    override fun getItemCount(): Int {
        return messageList.size
    }
    /**
     * Adapter view holder
     */
    class MessageItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageLeft: ImageView = itemView.findViewById<View>(R.id.ivProfileLeft) as ImageView
    }
}