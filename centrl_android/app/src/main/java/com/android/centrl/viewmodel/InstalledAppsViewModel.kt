package com.android.centrl.viewmodel

import android.app.Application
import com.android.centrl.persistence.repositry.InstalledAppsRepository
/**
 * View model for get all installed apps from device
 */
class InstalledAppsViewModel(application: Application): BaseViewModel(application) {

    private var installedAppsRepository: InstalledAppsRepository = InstalledAppsRepository(application)
    /**
     * Get all installed package names
     */
    fun getInstalledPackageName(code :Int): String{
        return installedAppsRepository.getInstalledPackageName(code)
    }
}