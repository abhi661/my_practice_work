package com.android.centrl.ui.fragment

import android.Manifest
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.loader.content.CursorLoader
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.android.centrl.interfaces.RecyclerViewClickListener
import com.android.centrl.R
import com.android.centrl.adapter.ContactListAdapter
import com.android.centrl.adapter.ContactListAdapter.ContactFilterSearchInterface
import com.android.centrl.databinding.FragmentContactBinding
import com.android.centrl.models.Contact
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.ui.activity.ChatsRoomActivity
import com.android.centrl.ui.views.RecyclerViewTouchListener
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.viewmodel.SearchViewModel
import com.google.android.gms.ads.AdView
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration
import kotlinx.coroutines.*
import java.util.*
import kotlin.coroutines.CoroutineContext
/**
 * Contacts list fragment
 */
class ContactFragment : BaseFragment(), ContactFilterSearchInterface {

    private lateinit var binding: FragmentContactBinding
    private lateinit var contactListAdapter: ContactListAdapter
    private var mContactList = ArrayList<Contact>()
    private lateinit var cursor: Cursor
    private var mAdView: AdView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_contact, container, false)
        val root = binding.root
        binding.noResultFoundTxtContact.visibility = View.GONE
        binding.contactsProgressBar.visibility = View.VISIBLE
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        binding.contactListRecyclerView.layoutManager = linearLayoutManager
        contactListAdapter = ContactListAdapter(requireActivity(), mContactList, this)
        binding.contactListRecyclerView.adapter = contactListAdapter
        // Add the sticky headers decoration
        val headersDecor = StickyRecyclerHeadersDecoration(contactListAdapter)
        binding.contactListRecyclerView.addItemDecoration(headersDecor)

        binding.fab.setOnClickListener {
            val createContact = Intent(ContactsContract.Intents.Insert.ACTION)
            createContact.type = ContactsContract.RawContacts.CONTENT_TYPE
            createContact.putExtra(ContactsContract.Intents.Insert.PHONE, "")
            startActivityForResult(createContact, REQUEST_ADD_CONTACTS)
        }
        binding.contactSyncBtn.setOnClickListener { requestContacts() }

        binding.contactListRecyclerView.addOnItemTouchListener(RecyclerViewTouchListener(activity, binding.contactListRecyclerView, object : RecyclerViewClickListener {
                    override fun onClick(view: View, position: Int) {
                        val contact = contactListAdapter.contactArrayList!![position]
                        val profilePath = if(contact.profilePath != null){
                            CentrlUtil.getContactImage(context, contact.profilePath, contact.id)
                        } else {
                            ""
                        }

                        val phoneNumber = contact.phoneNumber
                        val name = contact.name
                        if (phoneNumber != null && name != null) {
                            val nameUniqueKey = NotificationData.md5(contact.name!!.toLowerCase(
                                Locale.getDefault()) + "@0" + AppConstant.SMS_CODE)
                            var groupId: String? = null
                            var chatKey: String? = null
                            for (lastChats in mChatsViewModel!!.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                                if (nameUniqueKey.equals(lastChats.chatKey, ignoreCase = true)) {
                                    groupId = lastChats.groupID
                                    chatKey = lastChats.chatKey
                                }
                            }
                            if (groupId == null && chatKey == null) {
                                val numberUniqueKey = NotificationData.md5(contact.phoneNumber + "@0" + AppConstant.SMS_CODE)
                                for (lastChats in mChatsViewModel!!.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                                    if (numberUniqueKey.equals(lastChats.chatKey, ignoreCase = true)) {
                                        groupId = lastChats.groupID
                                        chatKey = lastChats.chatKey
                                    }
                                }
                            }
                            if (groupId == null && chatKey == null) {
                                groupId = contact.id
                                chatKey = nameUniqueKey
                                val getChatKey = mGroupChannelsViewModel!!.getChatKey(nameUniqueKey)
                                if (getChatKey.isEmpty()) {
                                    val rand = Random()
                                    val randomId = rand.nextInt(100000)
                                    val groupChat =
                                        GroupChannelsEntity(
                                            randomId,
                                            groupId!!,
                                            chatKey,
                                            contact.name!!,
                                            AppConstant.SMS_CODE,
                                            profilePath!!,
                                            profilePath,
                                            contact.name!!,
                                            contact.phoneNumber!!,
                                            0,
                                            AppConstant.CHAT_SCREEN,
                                            0,
                                            0
                                        )
                                    mGroupChannelsViewModel!!.insertGroup(groupChat)
                                }
                            }
                            val intent = Intent(activity, ChatsRoomActivity::class.java)
                            intent.putExtra(AppConstant.GROUP_KEY, groupId)
                            intent.putExtra(AppConstant.PACKAGE_CODE_KEY, AppConstant.SMS_CODE)
                            intent.putExtra(AppConstant.CHAT_KEY, chatKey)
                            intent.putExtra(AppConstant.IS_FROM_NOTIFICATION, false)
                            intent.putExtra(AppConstant.SHARED_TEXT_KEY, "")
                            activity!!.startActivity(intent)
                        }
                    }

                    override fun onLongClick(view: View, position: Int) {
                        //
                    }
                })
        )
        contactListAdapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onChanged() {
                headersDecor.invalidateHeaders()
            }
        })
        binding.adViewContainerContact.adViewContainer.post {
            mAdView = AdsLoading.loadBanner(requireActivity(), binding.adViewContainerContact.adViewContainer)
        }
        if (checkPermission()) {
            try {
                loadContactList()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        return root
    }

    override fun onResume() {
        super.onResume()
        refreshList()
        if (mAdView != null) {
            AdsLoading.resumeAds(requireActivity(), mAdView, binding.adViewContainerContact.adViewContainer)
        }
    }
    /**
     * Get contacts from device and update UI
     */
    private fun refreshList(){
        try{
            if (mContactList.size == 0) {
                binding.emptyContactLl.visibility = View.VISIBLE
                binding.contactListRecyclerView.visibility = View.GONE
                binding.contactsProgressBar.visibility = View.VISIBLE
                if (checkPermission()) {
                    binding.fab.visibility = View.VISIBLE
                    binding.contactSyncBtn.visibility = View.GONE
                } else {
                    binding.fab.visibility = View.VISIBLE
                    binding.contactSyncBtn.visibility = View.VISIBLE
                }
            } else {
                binding.contactsProgressBar.visibility = View.GONE
                binding.emptyContactLl.visibility = View.GONE
                binding.contactListRecyclerView.visibility = View.VISIBLE
                binding.fab.visibility = View.VISIBLE
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroyView() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        Presenter().cancel()
        super.onDestroyView()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        Presenter().cancel()
        super.onDestroy()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).contact!!.observe(requireActivity(), Observer { message: String? ->
                    if (message!!.isNotEmpty()) {
                        contactListAdapter.filter.filter(message)
                    } else {
                        binding.noResultFoundTxtContact.visibility = View.GONE
                    }
                }
            )
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).refresh!!.observe(requireActivity(), Observer { message: Int ->
                if (message == 2) {
                    try {
                        binding.contactsProgressBar.visibility = View.VISIBLE
                        loadContactList()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
    }
    /**
     * Get contacts from device in background
     */
    private inner class Presenter : CoroutineScope {
        private var job: Job = Job()
        override val coroutineContext: CoroutineContext
            get() = Dispatchers.Main + job // to run code in Main(UI) Thread

        // call this method to cancel a coroutine when you don't need it anymore,
        // e.g. when user closes the screen
        fun cancel() {
            job.cancel()
        }

        fun execute() = launch {
            onPreExecute()
            doInBackground() // runs in background thread without blocking the Main Thread
            onPostExecute()
        }

        private suspend fun doInBackground(): String = withContext(Dispatchers.IO) { // to run code in Background Thread
            // do async work
           // delay(1000) // simulate async work
            try {
                if (cursor.moveToFirst()) {
                    val idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID)
                    val nameIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY)
                    val photoIndex = cursor.getColumnIndex(ContactsContract.Data.PHOTO_URI)
                    val phNumber = cursor.getColumnIndex(ContactsContract.Data.HAS_PHONE_NUMBER)
                    do {
                        val contact = Contact()
                        val contactId = cursor.getString(idIndex).trim { it <= ' ' }
                        val contactDisplayName = cursor.getString(nameIndex).trim { it <= ' ' }
                        val photoURI = cursor.getString(photoIndex)
                        val hasPhoneNumber = cursor.getString(phNumber)
                        var phoneNumber = ""
                        if (hasPhoneNumber.equals("1", ignoreCase = true)) {
                            val phones = requireActivity().contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                                null,
                                null
                            )
                            while (phones!!.moveToNext()) {
                                phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            }
                            phones.close()
                        }
                        if (phoneNumber.isNotEmpty()) {
                            val name: String = contactDisplayName.substring(0, 1).toUpperCase(Locale.getDefault()) + contactDisplayName.substring(1)
                            contact.id = contactId
                            contact.name = name
                            contact.profilePath = photoURI
                            contact.phoneNumber = phoneNumber
                            mContactList.add(contact)
                        }
                    } while (cursor.moveToNext())
                }
                cursor.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return@withContext "SomeResult"
        }

        // Runs on the Main(UI) Thread
        private fun onPreExecute() {
            // show progress
        }

        // Runs on the Main(UI) Thread
        private fun onPostExecute() {
            // hide progress
            try {
                mContactList.sortWith(Comparator { o1: Contact, o2: Contact ->
                    o1.name!!.compareTo(o2.name!!)
                })
                contactListAdapter.updateList(mContactList)
                refreshList()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
    /**
     * Get contacts from device
     */
    private fun loadContactList() {
        try {
            binding.contactsProgressBar.visibility = View.VISIBLE
            mContactList.clear()
            val projectionFields = arrayOf(
                ContactsContract.Contacts._ID,
                ContactsContract.Contacts.DISPLAY_NAME_PRIMARY,
                ContactsContract.Contacts.HAS_PHONE_NUMBER,
                ContactsContract.Data.PHOTO_URI
            )
            val cursorLoader = CursorLoader(requireActivity(),
                    ContactsContract.Contacts.CONTENT_URI,
                    projectionFields,  // the columns to retrieve
                    null,  // the selection criteria (none)
                    null,  // the selection args (none)
                    null // the sort order (default)
                )
            cursor = cursorLoader.loadInBackground()!!
            Presenter().execute()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * Check contact permission
     */
    private fun checkPermission(): Boolean {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M || requireActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED
    }
    /**
     * Request contact permission
     */
    private fun requestContacts() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && requireActivity().checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    requireActivity(),
                    Manifest.permission.READ_CONTACTS
                )
            ) {
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(requireActivity())
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.contact_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _: DialogInterface?, _: Int ->
                    openPermissionSettings(PERMISSIONS_REQUEST_READ_CONTACTS)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                }
            } else {
                requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS), PERMISSIONS_REQUEST_READ_CONTACTS)
            }
        } else {
            loadContactList()
        }
    }

    private fun openPermissionSettings(requestCode: Int) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            val intent =
                Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.parse("package:" + requireActivity().packageName)
            intent.data = uri
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                requireActivity().startActivity(intent)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS && checkPermission()) {
            loadContactList()
        }
    }
    /**
     * Search filter result
     */
    override fun filterResult(size: Int) {
        if (size == 0) {
            binding.noResultFoundTxtContact.visibility = View.VISIBLE
            binding.contactListRecyclerView.visibility = View.GONE
        } else {
            binding.noResultFoundTxtContact.visibility = View.GONE
            binding.contactListRecyclerView.visibility = View.VISIBLE
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ADD_CONTACTS) {
            if (resultCode == Activity.RESULT_OK) {
                loadContactList()
                Toast.makeText(activity, resources.getString(R.string.contact_added), Toast.LENGTH_SHORT).show()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(activity, resources.getString(R.string.contact_cancelled), Toast.LENGTH_SHORT).show()
            }
        }
    }



    companion object {
        private const val PERMISSIONS_REQUEST_READ_CONTACTS = 100
        private const val REQUEST_ADD_CONTACTS = 5
    }
}