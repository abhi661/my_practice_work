package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a removed keyboard apps table
 * */
@Entity(tableName = "removedKeyboardApps")
data class RemovedKeyboardAppsEntity(
    @PrimaryKey
    var packageCode: Int
)