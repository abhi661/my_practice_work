package com.android.centrl.persistence.repositry

import com.android.centrl.persistence.dao.ChatBackupDao
import com.android.centrl.persistence.entity.ChatBackupEntity
import com.android.centrl.persistence.datasource.ChatBackupDataSource
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Using the Room database as a data source.
 */
class ChatBackupRepository(private val mUserDao: ChatBackupDao) : ChatBackupDataSource {
    /**
     * @return the chatBackup details from the table
     */
    override fun getBackup(): Flowable<ChatBackupEntity> {
        return mUserDao.getBackupDetails()
    }
    /**
     * Insert a backup details in the database. If the backup details already exists, replace it.
     *
     * @param chatBackup the backup details to be inserted.
     */
    override fun insertOrUpdateBackupDetails(chatBackup: ChatBackupEntity): Completable {
        return mUserDao.insertChatBackupDetails(chatBackup)
    }

    override fun updateChatBackupDetails(chatBackup: ChatBackupEntity): Completable {
        return mUserDao.updateChatBackupDetails(chatBackup.lastBackupTime, chatBackup.emailID, chatBackup.userName, chatBackup.backupType, chatBackup.backupId, chatBackup.dbSize)
    }

    /**
     * Delete all backup details.
     */
    override fun deleteAllBackupDetails(fileId: String) {
        try{
            Completable.fromAction {
                mUserDao.deleteAllBackupDetails(fileId)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}