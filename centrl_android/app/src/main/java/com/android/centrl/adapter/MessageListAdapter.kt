package com.android.centrl.adapter

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.models.AdsObject
import com.android.centrl.models.ChatModelObject
import com.android.centrl.models.DateObject
import com.android.centrl.models.ListObject
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.utils.DateUtil
import com.google.android.gms.ads.formats.MediaView
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import java.util.*


/**
 * Chat conversation list adapter
 */
class MessageListAdapter(
    private val mContext: Context,
    private var listObjects: List<ListObject>?,
    private val preferences: SharedPreferences,
    private val mNativeAppInterface: GoToNativeAppInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var previousPosition = -1
    private var isMultiSelect = false
    var lastSelectedPosition = ArrayList<String>()
    var mChatGroupId = ArrayList<ChatsEntity>()
    var mChatTitle = ArrayList<String>()
    val mapColorList: HashMap<String, Int> = HashMap()
    private var loggedInUserId = 0
    private var isGroupChat = 0

    override fun getItemCount(): Int {
        return listObjects!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    // Determines the appropriate ViewType according to the sender of the message.
    override fun getItemViewType(position: Int): Int {
        return listObjects!![position].getType(loggedInUserId)
    }

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return when (viewType) {
            ListObject.TYPE_DATE -> {
                view = LayoutInflater.from(parent.context).inflate(
                    R.layout.item_message_date,
                    parent,
                    false
                )
                DateMessageHolder(view)
            }
            ListObject.TYPE_ADS -> {
                view = LayoutInflater.from(parent.context).inflate(
                    R.layout.ad_unified,
                    parent,
                    false
                )
                UnifiedNativeAdViewHolder(view)
            }
            ListObject.TYPE_GENERAL_LEFT -> {
                view = LayoutInflater.from(parent.context).inflate(
                    R.layout.item_message_received,
                    parent,
                    false
                )
                ReceivedMessageHolder(view)
            }
            else -> {
                view = LayoutInflater.from(parent.context).inflate(
                    R.layout.item_message_sent,
                    parent,
                    false
                )
                SentMessageHolder(view)
            }
        }
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ListObject.TYPE_GENERAL_RIGHT -> {
                val generalItem = listObjects!![position] as ChatModelObject
                val chatViewHolder = holder as SentMessageHolder
                chatViewHolder.bind(generalItem.chatModel, position)
            }
            ListObject.TYPE_GENERAL_LEFT -> {
                val generalItemLeft = listObjects!![position] as ChatModelObject
                val chatLeftViewHolder = holder as ReceivedMessageHolder
                chatLeftViewHolder.bind(generalItemLeft.chatModel, position)
            }
            ListObject.TYPE_DATE -> {
                val dateItem = listObjects!![position] as DateObject
                val dateViewHolder = holder as DateMessageHolder
                dateViewHolder.bind(dateItem)
            }
            ListObject.TYPE_ADS -> {
                val nativeAd = listObjects!![position] as AdsObject
                val adsViewHolder = holder as UnifiedNativeAdViewHolder
                populateUnifiedNativeAdView(nativeAd.ads, adsViewHolder.adView)
            }
        }
    }

    /**
     * Populates a [UnifiedNativeAdView] object with data from a given
     * [UnifiedNativeAd].
     *
     * @param nativeAd the object containing the ad's assets
     * @param adView the view to be populated
     */
    private fun populateUnifiedNativeAdView(nativeAd: UnifiedNativeAd, adView: UnifiedNativeAdView) {
        // Set the media view.
        adView.mediaView = adView.findViewById(R.id.ad_media)

        // Set other ad assets.
        adView.headlineView = adView.findViewById(R.id.ad_headline)
        adView.bodyView = adView.findViewById(R.id.ad_body)
        adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)

        // The headline and media content are guaranteed to be in every UnifiedNativeAd.
        (adView.headlineView as TextView).text = nativeAd.headline
        adView.mediaView.setMediaContent(nativeAd.mediaContent)

        // These assets aren't guaranteed to be in every UnifiedNativeAd, so it's important to
        // check before trying to display them.
        if (nativeAd.body == null) {
            adView.bodyView.visibility = View.INVISIBLE
        } else {
            adView.bodyView.visibility = View.VISIBLE
            (adView.bodyView as TextView).text = nativeAd.body
        }

        if (nativeAd.callToAction == null) {
            adView.callToActionView.visibility = View.INVISIBLE
        } else {
            adView.callToActionView.visibility = View.VISIBLE
            (adView.callToActionView as Button).text = nativeAd.callToAction
        }

        // This method tells the Google Mobile Ads SDK that you have finished populating your
        // native ad view with this native ad.
        adView.setNativeAd(nativeAd)
    }

    fun setUser(userId: Int) {
        loggedInUserId = userId
    }

    fun setDataChange(asList: List<ListObject>, groupChat: Int) {
        listObjects = asList
        isGroupChat = groupChat
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
    /**
     * Adapter date view holder
     */
    private inner class DateMessageHolder constructor(view: View) :
        RecyclerView.ViewHolder(view) {
        var mDate: TextView = view.findViewById(R.id.msg_date)
        fun bind(chat: DateObject) {
            val currentDate = DateUtil.convertDateToString(DateUtil.currentDateTime!!)
            val yesterdayDate = DateUtil.convertDateToString(DateUtil.yesterdayDateTime!!)
            when {
                chat.date.equals(currentDate, ignoreCase = true) -> {
                    mDate.text = mContext.resources.getString(R.string.today)
                }
                chat.date.equals(yesterdayDate, ignoreCase = true) -> {
                    mDate.text = mContext.resources.getString(R.string.yesterday)
                }
                else -> {
                    mDate.text = chat.date
                }
            }
        }

    }


    class UnifiedNativeAdViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        var adView: UnifiedNativeAdView = view.findViewById<View>(R.id.ad_view) as UnifiedNativeAdView
        init {

            // The MediaView will display a video asset if one is present in the ad, and the
            // first image asset otherwise.
            adView.mediaView = adView.findViewById<View>(R.id.ad_media) as MediaView

            // Register the view used for each individual asset.
            adView.headlineView = adView.findViewById(R.id.ad_headline)
            adView.bodyView = adView.findViewById(R.id.ad_body)
            adView.callToActionView = adView.findViewById(R.id.ad_call_to_action)
        }

    }

    /**
     * Adapter sender view holder
     */
    private inner class SentMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById<View>(R.id.text_message_body_sender) as TextView
        var timeText: TextView = itemView.findViewById<View>(R.id.text_message_time_sender) as TextView
        var timeText1: TextView = itemView.findViewById<View>(R.id.text_message_time_sender1) as TextView
        var msgTypeImage: ImageView = itemView.findViewById<View>(R.id.msg_type_image_sender) as ImageView
        var mSenderRootLL: FrameLayout = itemView.findViewById<View>(R.id.sender_root_ll) as FrameLayout

        fun bind(message: ChatsEntity, position: Int) {
            try{
                val fontSize = preferences.getString(AppConstant.FONT_SIZE, null)
                if (fontSize != null) {
                    when {
                        fontSize.equals(
                            mContext.resources.getString(R.string.small),
                            ignoreCase = true
                        ) -> {
                            messageText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font16
                                )
                            )
                            timeText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font12
                                )
                            )
                            timeText1.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font12
                                )
                            )
                        }
                        fontSize.equals(
                            mContext.resources.getString(R.string.medium),
                            ignoreCase = true
                        ) -> {
                            messageText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font18
                                )
                            )
                            timeText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font14
                                )
                            )
                            timeText1.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font14
                                )
                            )
                        }
                        fontSize.equals(
                            mContext.resources.getString(R.string.large),
                            ignoreCase = true
                        ) -> {
                            messageText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font22
                                )
                            )
                            timeText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font18
                                )
                            )
                            timeText1.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font18
                                )
                            )
                        }
                    }
                } else {
                    messageText.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                            R.dimen.font16
                        )
                    )
                    timeText.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                            R.dimen.font12
                        )
                    )
                    timeText1.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                            R.dimen.font12
                        )
                    )
                }

                messageText.text = message.messages

                if (message.timeStamp.isNotEmpty()) {
                    timeText.text = DateUtil.convertAMFormat(message.timeStamp).toLowerCase(Locale.getDefault())
                    timeText1.text = DateUtil.convertAMFormat(message.timeStamp).toLowerCase(Locale.getDefault())
                } else {
                    timeText.text = ""
                    timeText1.text = ""
                }

                if(message.packageCode != 0){
                    try{
                        msgTypeImage.setBackgroundResource(CentrlUtil.getChannelsLogo()[message.packageCode]!!)
                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }

                if(message.messages.length < 40){
                    timeText.visibility = View.GONE
                    timeText1.visibility = View.VISIBLE
                } else{
                    timeText.visibility = View.VISIBLE
                    timeText1.visibility = View.GONE
                }
                if (!isMultiSelect) {
                    if (position == previousPosition) {
                        mSenderRootLL.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.select_highlight
                            )
                        )
                    } else {
                        mSenderRootLL.setBackgroundColor(0)
                    }
                } else {
                    if (message.isSelected) {
                        mSenderRootLL.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.select_highlight
                            )
                        )
                    } else {
                        mSenderRootLL.setBackgroundColor(0)
                    }
                }

                mSenderRootLL.setOnClickListener {
                    mNativeAppInterface.onClick(position, message.packageCode, message.chatKey)
                }

                mSenderRootLL.setOnLongClickListener {
                    mNativeAppInterface.onLongClick(position, message.packageCode, message.chatKey)
                    return@setOnLongClickListener false
                }

            }catch (e: Exception){
                e.printStackTrace()
            }
        }

    }
    /**
     * Adapter receiver view holder
     */
    private inner class ReceivedMessageHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var messageText: TextView = itemView.findViewById<View>(R.id.text_message_body) as TextView
        var messageTextTitle: TextView = itemView.findViewById<View>(R.id.text_message_title) as TextView
        var timeText: TextView = itemView.findViewById<View>(R.id.text_message_time) as TextView
        var timeText1: TextView = itemView.findViewById<View>(R.id.text_message_time1) as TextView
        var msgTypeImage: ImageView = itemView.findViewById<View>(R.id.msg_type_image) as ImageView
        var mReceivedRootRL: FrameLayout = itemView.findViewById<View>(R.id.noti_receiver_ll) as FrameLayout
        var mReceivedLL: LinearLayout = itemView.findViewById<View>(R.id.received_rl) as LinearLayout
        fun bind(message: ChatsEntity, position: Int) {
            try{
                val fontSize = preferences.getString(AppConstant.FONT_SIZE, null)
                if (fontSize != null) {
                    when {
                        fontSize.equals(
                            mContext.resources.getString(R.string.small),
                            ignoreCase = true
                        ) -> {
                            messageText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font16
                                )
                            )
                            timeText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font12
                                )
                            )
                            timeText1.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font12
                                )
                            )
                        }
                        fontSize.equals(
                            mContext.resources.getString(R.string.medium),
                            ignoreCase = true
                        ) -> {
                            messageText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font18
                                )
                            )
                            timeText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font14
                                )
                            )
                            timeText1.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font14
                                )
                            )
                        }
                        fontSize.equals(
                            mContext.resources.getString(R.string.large),
                            ignoreCase = true
                        ) -> {
                            messageText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font22
                                )
                            )
                            timeText.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font18
                                )
                            )
                            timeText1.setTextSize(
                                TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                                    R.dimen.font18
                                )
                            )
                        }
                    }
                } else {
                    messageText.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                            R.dimen.font16
                        )
                    )
                    timeText.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                            R.dimen.font12
                        )
                    )
                    timeText1.setTextSize(
                        TypedValue.COMPLEX_UNIT_PX, mContext.resources.getDimension(
                            R.dimen.font12
                        )
                    )
                }
                if(isGroupChat == 1){
                    val tempMSG = message.messages.split("@@")
                    val title = tempMSG[0]
                    val msg: String
                    msg = if(tempMSG[1].startsWith(" ", 0)){
                        tempMSG[1].replaceFirst(" ", "")
                    } else {
                        tempMSG[1]
                    }

                    if(!mChatTitle.contains(title)){
                        mChatTitle.add(title)
                        mapColorList[title] = getRandomColor()
                    }
                    messageTextTitle.visibility = View.VISIBLE
                    messageTextTitle.text = title
                    messageTextTitle.setTextColor(mapColorList[title]!!)
                    messageText.text = msg
                } else {
                    messageTextTitle.visibility = View.GONE
                    messageText.text = message.messages
                }
                if (message.timeStamp.isNotEmpty()) {
                    timeText.text = DateUtil.convertAMFormat(message.timeStamp).toLowerCase(Locale.getDefault())
                    timeText1.text = DateUtil.convertAMFormat(message.timeStamp).toLowerCase(Locale.getDefault())
                } else {
                    timeText.text = ""
                    timeText1.text = ""
                }

                if(message.packageCode != 0){
                    try{
                        msgTypeImage.setBackgroundResource(CentrlUtil.getChannelsLogo()[message.packageCode]!!)
                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }

                if(message.messages.length < 40){
                    timeText.visibility = View.GONE
                    timeText1.visibility = View.VISIBLE
                } else{
                    timeText.visibility = View.VISIBLE
                    timeText1.visibility = View.GONE
                }
                if (!isMultiSelect) {
                    if (position == previousPosition) {
                        mReceivedRootRL.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.select_highlight
                            )
                        )
                    } else {
                        mReceivedRootRL.setBackgroundColor(0)
                    }
                } else {
                    if (message.isSelected) {
                        mReceivedRootRL.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.select_highlight
                            )
                        )
                    } else {
                        mReceivedRootRL.setBackgroundColor(0)
                    }
                }

                mReceivedLL.setOnClickListener {
                    mNativeAppInterface.onClick(position, message.packageCode, message.chatKey)
                }

                mReceivedLL.setOnLongClickListener {
                    mNativeAppInterface.onLongClick(position, message.packageCode, message.chatKey)
                    return@setOnLongClickListener false
                }

            }catch (e: Exception){
                e.printStackTrace()
            }

        }

    }

    fun getRandomColor(): Int {
        val rnd = Random()
        return Color.argb(
            255,
            rnd.nextInt(200),
            rnd.nextInt(200),
            rnd.nextInt(200)
        )
    }
    /**
     * This is use to remove highlight for selected messages
     */
    fun selectList(position: Int) {
        isMultiSelect = false
        previousPosition = position
        for (chat in mChatGroupId) {
            chat.isSelected = false
        }
        mChatGroupId.clear()
        notifyDataSetChanged()
    }
    /**
     * Multi select for delete, copy
     */
    fun multiSelectList(position: Int) {
        isMultiSelect = true
        if (lastSelectedPosition.contains(position.toString())) {
            lastSelectedPosition.remove(position.toString())
            val chat = listObjects!![position] as ChatModelObject
            mChatGroupId.remove(chat.chatModel)
            previousPosition = -1
            chat.chatModel.isSelected = false
            notifyItemChanged(position)
        } else {
            lastSelectedPosition.add(position.toString())
            val chat = listObjects!![position] as ChatModelObject
            mChatGroupId.add(chat.chatModel)
            chat.chatModel.isSelected = true
            previousPosition = position
            notifyItemChanged(position)
        }
    }

    /**
     * Navigate to native app interface
     */
    interface GoToNativeAppInterface {
        fun onClick(position: Int, packageCode: Int, chatKey: String)
        fun onLongClick(position: Int, packageCode: Int, chatKey: String)
    }
}