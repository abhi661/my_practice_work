package com.android.centrl.ui.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.centrl.interfaces.ChatContactSelectedListener
import com.android.centrl.interfaces.MultiSelectChatList
import com.android.centrl.interfaces.RecyclerViewClickListener
import com.android.centrl.R
import com.android.centrl.adapter.ChatContactListAdapter
import com.android.centrl.databinding.FragmentMailBinding
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.ui.activity.MailChatActivity
import com.android.centrl.viewmodel.SearchViewModel
import com.android.centrl.ui.views.RecyclerViewTouchListener
import com.android.centrl.utils.*
import com.google.android.gms.ads.AdView
import kotlin.collections.ArrayList

/**
 * Mail Listing fragment
 */
class MailFragment : BaseFragment(), ChatContactListAdapter.FilterSearchInterface {

    companion object {
        private const val mailsScreen = "mails"
    }
    private lateinit var binding: FragmentMailBinding
    private var mailAppMsgAdapter: ChatContactListAdapter? = null
    private var multiSelectChatList: MultiSelectChatList? = null
    private var mMailList = ArrayList<ChatsEntity>()
    private var mChatBadgeGroupList = ArrayList<String>()
    private var notificationReceiver: NotificationReceiverFragment? = null
    private var chatContactSelectedListener: ChatContactSelectedListener? = null
    private var mContext: Context? = null
    private var mAdView: AdView? = null

    private var isMultiSelect = false
    private var isCheckboxMultiSelect = false
    private var isBlockedContactSelected = false
    private var isSameChannelSelected = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_mail, container, false)
        val root = binding.root

        binding.noResultFoundTxtMail.visibility = View.GONE
        val linearLayoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        val divider = DividerItemDecoration( binding.chatListRecyclerViewMail.context, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.layer)!!)
        binding.chatListRecyclerViewMail.addItemDecoration(divider)
        //binding.chatListRecyclerViewMail.addItemDecoration(DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL))
        binding.chatListRecyclerViewMail.layoutManager = linearLayoutManager
        mailAppMsgAdapter = ChatContactListAdapter(requireActivity(), mMailList, this)
        binding.chatListRecyclerViewMail.adapter = mailAppMsgAdapter
        binding.chatListRecyclerViewMail.addOnItemTouchListener(RecyclerViewTouchListener(activity, binding.chatListRecyclerViewMail, object : RecyclerViewClickListener {
                    override fun onClick(view: View, position: Int) {
                       try {
                           val chatContactDetails = mailAppMsgAdapter!!.chatArrayList[position]
                           if (!isCheckboxMultiSelect) {
                               if (mailAppMsgAdapter!!.lastSelectedPosition.size == 0) {
                                   isMultiSelect = false
                               }
                               if (mailAppMsgAdapter!!.mBlockedContactList.size == 0) {
                                   isBlockedContactSelected = false
                               }
                               if (mailAppMsgAdapter!!.mSameChannelList.size == 0) {
                                   isSameChannelSelected = false
                               }
                           }
                           if (isMultiSelect) {
                               mailAppMsgAdapter!!.multiSelectList(position)
                               isBlockedContactSelected = when (mailAppMsgAdapter!!.mBlockedContactList.size) {
                                   0 -> {
                                       false
                                   }
                                   else -> {
                                       true
                                   }
                               }
                               isSameChannelSelected = CentrlUtil.checkDuplicateUsingSet(mailAppMsgAdapter!!.mSameChannelList)
                               multiSelectChatList!!.onMultiSelect(true, isCheckboxMultiSelect, mailAppMsgAdapter!!.mChatGroupId, isBlockedContactSelected, isSameChannelSelected, false)
                           } else {
                               if(isMultiSelect){
                                   mailAppMsgAdapter!!.selectList(-1)
                               }
                               val intent = Intent(activity, MailChatActivity::class.java)
                               intent.putExtra(AppConstant.GROUP_KEY, chatContactDetails.groupID)
                               intent.putExtra(AppConstant.PACKAGE_CODE_KEY, chatContactDetails.packageCode)
                               intent.putExtra(AppConstant.CHAT_KEY, chatContactDetails.chatKey)
                               intent.putExtra(AppConstant.IS_FROM_NOTIFICATION, false)
                               intent.putExtra(AppConstant.SHARED_TEXT_KEY, "")
                               activity!!.startActivity(intent)
                               chatContactSelectedListener!!.chatContactSelected()
                           }
                       }catch (e: Exception){
                           e.printStackTrace()
                       }
                    }

                    override fun onLongClick(view: View, position: Int) {
                        try {
                            if (!isCheckboxMultiSelect) {
                                isMultiSelect = true
                                mailAppMsgAdapter!!.multiSelectList(position)
                                isBlockedContactSelected = when (mailAppMsgAdapter!!.mBlockedContactList.size) {
                                    0 -> {
                                        false
                                    }
                                    else -> {
                                        true
                                    }
                                }
                                multiSelectChatList!!.onMultiSelect(true, isCheckboxMultiSelect, mailAppMsgAdapter!!.mChatGroupId, isBlockedContactSelected, isSameChannelSelected, false)
                            }
                        }catch (e: Exception){
                            e.printStackTrace()
                        }
                    }
                })
        )
        binding.adViewContainerMail.adViewContainer.post {
            mAdView = AdsLoading.loadBanner(requireActivity(), binding.adViewContainerMail.adViewContainer)
        }
        return root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).mail!!.observe(requireActivity(), Observer { message: String? ->
                    if (message!!.isNotEmpty()) {
                        mailAppMsgAdapter!!.filter.filter(message)
                    } else {
                        binding.noResultFoundTxtMail.visibility = View.GONE
                        mChatsViewModel!!.getChatContactsList(AppConstant.MAIL_SCREEN)
                    }
                }
            )
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).refresh!!.observe(requireActivity(), Observer {page ->
            if(page == 1){
                mChatsViewModel!!.getChatContactsList(AppConstant.MAIL_SCREEN)
                if (isMultiSelect) {
                    mailAppMsgAdapter!!.selectList(-1)
                    mailAppMsgAdapter!!.lastSelectedPosition.clear()
                    mailAppMsgAdapter!!.mSameChannelList.clear()
                    mailAppMsgAdapter!!.mBlockedContactList.clear()
                    isMultiSelect = false
                    isBlockedContactSelected = false
                    isSameChannelSelected = false
                }
            }
            })
        ViewModelProvider(requireActivity()).get(SearchViewModel::class.java).merge!!.observe(requireActivity(), Observer { integer: Int ->
                if (integer == 1) {
                    isCheckboxMultiSelect = true
                    isMultiSelect = true
                    isBlockedContactSelected = false
                    isSameChannelSelected = false
                    mailAppMsgAdapter!!.mSameChannelList.clear()
                    mailAppMsgAdapter!!.mBlockedContactList.clear()
                    mailAppMsgAdapter!!.chooseMergeOption(true)
                } else {
                    isCheckboxMultiSelect = false
                    isMultiSelect = false
                    isBlockedContactSelected = false
                    isSameChannelSelected = false
                    mailAppMsgAdapter!!.chooseMergeOption(false)
                    mailAppMsgAdapter!!.selectList(-1)
                    mailAppMsgAdapter!!.mSameChannelList.clear()
                    mailAppMsgAdapter!!.mBlockedContactList.clear()
                    mailAppMsgAdapter!!.lastSelectedPosition.clear()
                }
            })

        mChatsViewModel!!.mChatContactsList.observe(requireActivity(), Observer { result ->
            try{
                mChatBadgeGroupList.clear()
                mMailList = result
                if (mMailList.size == 0) {
                    binding.chatListRecyclerViewMail.visibility = View.GONE
                    binding.emptyImageChatMail.visibility = View.VISIBLE
                    mChatBadgeGroupList.clear()
                } else {
                    mMailList.sortWith(Comparator sort@{ o1: ChatsEntity, o2: ChatsEntity ->
                        try {
                            return@sort DateUtil.convertDateFormat(o2.timeStamp).compareTo(DateUtil.convertDateFormat(o1.timeStamp))
                        } catch (e: Exception) {
                            e.printStackTrace()
                            return@sort 0
                        }
                    })
                    mailAppMsgAdapter!!.updateList(mMailList)
                    binding.chatListRecyclerViewMail.visibility = View.VISIBLE
                    binding.emptyImageChatMail.visibility = View.GONE
                    for (chat in mMailList) {
                        val count = mMutiChannelViewModel!!.getCount(chat.groupID)
                        if (count != 0) {
                            val groupId = chat.groupID
                            if (!mChatBadgeGroupList.contains(groupId)) {
                                mChatBadgeGroupList.add(chat.groupID)
                            }
                        }
                    }
                }
                multiSelectChatList!!.onReceived(1, mChatBadgeGroupList)
            } catch (e: Exception){
                e.printStackTrace()
            }

        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        multiSelectChatList = context as MultiSelectChatList
        chatContactSelectedListener = context as ChatContactSelectedListener
        mContext = context
        if (notificationReceiver == null) {
            notificationReceiver = NotificationReceiverFragment()
            val filter = IntentFilter()
            filter.addAction(AppConstant.ACTION_BROADCAST_RECEIVER)
            context.registerReceiver(notificationReceiver, filter)
        }
    }
    override fun onStart() {
        super.onStart()
        mChatsViewModel!!.getChatContactsList(AppConstant.MAIL_SCREEN)
    }
    override fun onResume() {
        super.onResume()
        if (mAdView != null) {
            AdsLoading.resumeAds(requireActivity(), mAdView, binding.adViewContainerMail.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroyView() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroyView()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    override fun onDetach() {
        super.onDetach()
        try {
            if (notificationReceiver != null) {
                mContext!!.unregisterReceiver(notificationReceiver)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Search filter result
     */
    override fun filterResult(size: Int) {
        if (size == 0) {
            binding.noResultFoundTxtMail.visibility = View.VISIBLE
            binding.chatListRecyclerViewMail.visibility = View.GONE
        } else {
            binding.noResultFoundTxtMail.visibility = View.GONE
            binding.chatListRecyclerViewMail.visibility = View.VISIBLE
        }
    }
    /**
     * Notification receiver for update the UI
     */
    internal inner class NotificationReceiverFragment : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                val groupKey = intent.getStringExtra(AppConstant.GROUP_KEY)
                val receivedType = intent.getStringExtra(AppConstant.NOTIFICATION_TYPE)
                if(receivedType == mailsScreen) {
                    val chatLast =
                        mChatsViewModel!!.getLastChat(groupKey!!, AppConstant.MAIL_SCREEN)
                    if (chatLast.groupID.isNotEmpty()) {
                        var position = -1
                        for (i in mMailList.indices) {
                            if (mMailList[i].groupID.equals(chatLast.groupID, ignoreCase = true)) {
                                position = i
                            }
                        }
                        if (position != -1) {
                            mMailList.removeAt(position)
                            mMailList.add(0, chatLast)
                            mailAppMsgAdapter!!.updateList(mMailList)
                        } else {
                            if (mMailList.size == 0) {
                                mChatsViewModel!!.getChatContactsList(AppConstant.MAIL_SCREEN)
                                //Presenter().execute()
                            } else {
                                mMailList.add(0, chatLast)
                                mailAppMsgAdapter!!.updateList(mMailList)
                            }
                        }
                        for (chat in mMailList) {
                            val count = mMutiChannelViewModel!!.getCount(chat.groupID)
                            if (count != 0) {
                                val groupId = chat.groupID
                                if (!mChatBadgeGroupList.contains(groupId)) {
                                    mChatBadgeGroupList.add(chat.groupID)
                                }
                            }
                        }
                        multiSelectChatList!!.onReceived(1, mChatBadgeGroupList)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}