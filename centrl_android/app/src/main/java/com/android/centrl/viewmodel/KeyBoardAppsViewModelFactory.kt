package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.persistence.datasource.KeyBoardAppsTrayDataSource

/**
 * Factory for ViewModels
 */
class KeyBoardAppsViewModelFactory(private val application: Application, private val mDataSource: KeyBoardAppsTrayDataSource) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(KeyBoardAppsViewModel::class.java)) {
            return KeyBoardAppsViewModel(application, mDataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}