package com.android.centrl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.interfaces.ReadMoreClickListener
import com.android.centrl.models.ChatModelObject
import com.android.centrl.models.DateObject
import com.android.centrl.models.ListObject
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.ui.views.ReadMoreOption
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.utils.DateUtil
import java.util.*

/**
 * Mail conversation list adapter
 */
class MailListAdapter(
    private val mContext: Context,
    messageList: ArrayList<ListObject>?,
    mailListAdapterInterface: MailListAdapterInterface
) : RecyclerView.Adapter<RecyclerView.ViewHolder>(), ReadMoreClickListener {

    private var mMessageList: List<ListObject>? = null
    private val mailListAdapterInterface: MailListAdapterInterface
    var lastSelectedPosition = ArrayList<String>()
    var mChatGroupId = ArrayList<ChatsEntity>()
    private val readMoreOption  = ReadMoreOption.Builder(mContext).build()
    private var previousPosition = -1
    private var isMultiSelect = false
    private var loggedInUserId = 0
    private var mPosition: Int = 0
    private var isExpanded = false

    init {
        mMessageList = messageList
        this.mailListAdapterInterface = mailListAdapterInterface
        readMoreOption.onClickListener(this)
    }

    interface MailListAdapterInterface {
        fun replyMail(chatKey: String, packCode: Int)
    }

    override fun getItemCount(): Int {
        return mMessageList!!.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return mMessageList!![position].getType(loggedInUserId)
    }

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View
        return if (viewType == ListObject.TYPE_DATE) {
            view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_message_date,
                parent,
                false
            )
            DateMessageHolder(view)
        } else {
            view = LayoutInflater.from(parent.context).inflate(
                R.layout.item_mail_list,
                parent,
                false
            )
            MailListViewHolder(view)
        }
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == ListObject.TYPE_DATE) {
            val dateItem = mMessageList!![position] as DateObject
            val dateViewHolder = holder as DateMessageHolder
            dateViewHolder.bind(dateItem)
        } else {
            val message = mMessageList!![position] as ChatModelObject
            val mailListViewHolder = holder as MailListViewHolder
            mailListViewHolder.bind(message.chatModel, position)

        }
    }

    fun setDataChange(asList: List<ListObject>) {
        mMessageList = asList
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }

    fun setUser(userId: Int) {
        loggedInUserId = userId
    }
    /**
     * Adapter mali view holder
     */
    inner class MailListViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(
        itemView
    ) {
        private var messageText: TextView = itemView.findViewById<View>(R.id.text_message_body_iml) as TextView
        private var subjectTxt: TextView = itemView.findViewById<View>(R.id.text_message_subject) as TextView
        private var timeText: TextView = itemView.findViewById<View>(R.id.text_message_time_iml) as TextView
        private var replyBtn: RelativeLayout = itemView.findViewById<View>(R.id.mail_reply_btn) as RelativeLayout
        private var mailChatRL: FrameLayout = itemView.findViewById<View>(R.id.mail_chat_root_rl) as FrameLayout
        private var mailType: ImageView = itemView.findViewById<View>(R.id.mail_chat_type_view) as ImageView

        fun bind(message: ChatsEntity, position: Int) {
            try {
                val temp = message.messages
                val tempStr = temp.split("\n".toRegex()).toTypedArray()
                val mailSubject = tempStr[0]
                val mailSB = StringBuffer()
                for (i in 1 until tempStr.size) {
                    mailSB.append("\n")
                    mailSB.append(tempStr[i])
                }
                val content = mailSB.toString()
                subjectTxt.text = mailSubject
                if(!isExpanded){
                    if (position % 2 == 0) {
                        val rmo = readMoreOption.addReadMoreTo(messageText, content)
                        if(rmo == 0){
                            replyBtn.visibility = View.VISIBLE
                        } else {
                            replyBtn.visibility = View.GONE
                        }
                    } else {
                        val rmo = readMoreOption.addReadMoreTo(messageText, content)
                        if(rmo == 0){
                            replyBtn.visibility = View.VISIBLE
                        } else {
                            replyBtn.visibility = View.GONE
                        }
                    }
                }

                if(isExpanded){
                    if(position == mPosition){
                        replyBtn.visibility = View.VISIBLE
                    } else {
                        if (position % 2 == 0) {
                            val rmo = readMoreOption.addReadMoreTo(messageText, content)
                            if(rmo == 0){
                                replyBtn.visibility = View.VISIBLE
                            } else {
                                replyBtn.visibility = View.GONE
                            }
                        } else {
                            val rmo = readMoreOption.addReadMoreTo(messageText, content)
                            if(rmo == 0){
                                replyBtn.visibility = View.VISIBLE
                            } else {
                                replyBtn.visibility = View.GONE
                            }
                        }
                    }
                }
                timeText.text = DateUtil.convertAMFormat(message.timeStamp).toLowerCase(Locale.getDefault())

                if(message.packageCode != 0){
                    mailType.setBackgroundResource(CentrlUtil.getChannelsLogo()[message.packageCode]!!)
                }
                replyBtn.setOnClickListener {
                    mailListAdapterInterface.replyMail(message.mailKey, message.packageCode)
                }
                if (!isMultiSelect) {
                    if (position == previousPosition) {
                        mailChatRL.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.select_highlight
                            )
                        )
                    } else {
                        mailChatRL.setBackgroundColor(0)
                    }
                } else {
                    if (message.isSelected) {
                        mailChatRL.setBackgroundColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.select_highlight
                            )
                        )
                    } else {
                        mailChatRL.setBackgroundColor(0)
                    }
                }
                messageText.setOnClickListener {
                    mPosition = position
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
    /**
     * Adapter date view holder
     */
    private inner class DateMessageHolder constructor(view: View) :
        RecyclerView.ViewHolder(view) {
        private var mDate: TextView = view.findViewById(R.id.msg_date)
        fun bind(chat: DateObject) {
            val currentDate = DateUtil.convertDateToString(DateUtil.currentDateTime!!)
            val yesterdayDate = DateUtil.convertDateToString(DateUtil.yesterdayDateTime!!)
            when {
                chat.date.equals(currentDate, ignoreCase = true) -> {
                    mDate.text = mContext.resources.getString(R.string.today)
                }
                chat.date.equals(yesterdayDate, ignoreCase = true) -> {
                    mDate.text = mContext.resources.getString(R.string.yesterday)
                }
                else -> {
                    mDate.text = chat.date
                }
            }
        }
    }
    /**
     * This is use to remove highlight for mails
     */
    fun selectList(position: Int) {
        isMultiSelect = false
        previousPosition = position
        for (chat in mChatGroupId) {
            chat.isSelected = false
        }
        mChatGroupId.clear()
        notifyDataSetChanged()
    }
    /**
     * Multi select for delete, copy
     */
    fun multiSelectList(position: Int) {
        isMultiSelect = true
        if (lastSelectedPosition.contains(position.toString())) {
            lastSelectedPosition.remove(position.toString())
            val chat = mMessageList!![position] as ChatModelObject
            mChatGroupId.remove(chat.chatModel)
            previousPosition = -1
            chat.chatModel.isSelected = false
            notifyItemChanged(position)
        } else {
            lastSelectedPosition.add(position.toString())
            val chat = mMessageList!![position] as ChatModelObject
            mChatGroupId.add(chat.chatModel)
            chat.chatModel.isSelected = true
            previousPosition = position
            notifyItemChanged(position)
        }
    }

    override fun onClick(isExpand: Boolean) {
        isExpanded = isExpand
        notifyDataSetChanged()
    }
}