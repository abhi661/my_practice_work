package com.android.centrl.ui.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.app.PendingIntent.getActivity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.KeyEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import com.android.centrl.R
import com.android.centrl.databinding.ActivityChatsBinding
import com.android.centrl.gdrive.DriveServiceHelper
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.ChatBackupEntity
import com.android.centrl.persistence.injections.Injection
import com.android.centrl.ui.dialog.ProgressDialog
import com.android.centrl.utils.*
import com.android.centrl.viewmodel.ChatBackupViewModel
import com.android.centrl.viewmodel.ChatBackupViewModelFactory
import com.android.centrl.worker.ChatBackupWorker
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker.Companion.getFilePath
import com.github.dhaval2404.imagepicker.ImagePicker.Companion.with
import com.google.android.gms.ads.AdView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.drive.Drive.SCOPE_APPFOLDER
import com.google.android.gms.drive.Drive.SCOPE_FILE
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.api.services.drive.Drive
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_chats.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Chat backup config activity
 */
class ChatsBackUpActivity : BaseActivity() {

    private var mDriveServiceHelper: DriveServiceHelper? = null
    private var mViewModel: ChatBackupViewModel? = null
    private lateinit var mViewModelFactory: ChatBackupViewModelFactory
    private var account: GoogleSignInAccount? = null
    private var mAdView: AdView? = null
    private lateinit var wallpaperChangeBehavior: BottomSheetBehavior<*>
    private lateinit var dialog: Dialog
    private lateinit var binding: ActivityChatsBinding
    private lateinit var chatBackupEntity: ChatBackupEntity

    private var mBackupTime: String? = null
    private var mFontSize: String? = null
    private var mDataBaseSize: String? = null
    private var mBackupOldId: String? = null
    private var mIsSelectedTime: Boolean = false
    private var mUpdateNever: Boolean = false

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = ProgressDialog.progressDialog(this)
        mViewModelFactory = Injection.provideViewModelFactory(application, this)
        mViewModel = ViewModelProvider(this, mViewModelFactory).get(ChatBackupViewModel::class.java)
        account = GoogleSignIn.getLastSignedInAccount(applicationContext)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_chats)
        binding.handler = ClickHandlers()
        binding.chatBackupViewModel = mViewModel
        binding.lifecycleOwner = this

        wallpaperChangeBehavior = BottomSheetBehavior.from(binding.bottomSheets.bottomSheet)
        wallpaperChangeBehavior.isHideable = true
        wallpaperChangeBehavior.state = BottomSheetBehavior.STATE_HIDDEN

        binding.adsChatBackup.adViewContainer.post {
            mAdView = AdsLoading.loadBanner(this@ChatsBackUpActivity, binding.adsChatBackup.adViewContainer)
        }
        mViewModel!!.mBackupDetails.observe(this, Observer { backupEntity ->
            try {
                mBackupOldId = backupEntity.backupId
                chatBackupEntity = backupEntity
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
        mViewModel!!.backupInsertStatus.observe(this, Observer { status ->
            when (status) {
                true -> {
                    when {
                        mBackupTime.equals(resources.getString(R.string.daily)) -> {
                            CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.backup_success))
                            scheduleRepeatingTasks(1)
                        }
                        mBackupTime.equals(resources.getString(R.string.weekly)) -> {
                            CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.backup_success))
                            scheduleRepeatingTasks(7)
                        }
                        else -> {
                            stopChatBackup()
                        }
                    }
                }
                false -> {
                    CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.backup_failed))
                }
            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onResume() {
        super.onResume()
        if (checkStoragePermission()) {
            PrefManager.getInstance(this)!!.saveBoolean(AppConstant.SET_IMAGE, false)
            account = GoogleSignIn.getLastSignedInAccount(applicationContext)
            if (account != null) {
                mViewModel!!.setmEmail(account!!.email!!)
                mViewModel!!.getBackupDetails()
               val backupDetails = mViewModel!!.getBackupsDetails()
                if(backupDetails.backupType.isEmpty() && !mIsSelectedTime){
                    mViewModel!!.setmNever(true)
                }
            } else {
                mViewModel!!.setmNever(true)
                mViewModel!!.setmEmail(resources.getString(R.string.no_account))
            }
            if (binding.neverBackupBtn.isChecked) {
                binding.chatBackupBtn.setBackgroundResource(R.drawable.backup_unsync_account)
                binding.chatImportBtn.setBackgroundResource(R.drawable.import_bg_unscync_account)
                binding.chatImportBtn.setTextColor(resources.getColor(R.color.import_txt, null))
            } else {
                binding.chatBackupBtn.setBackgroundResource(R.drawable.backup_btn_bg)
                binding.chatImportBtn.setBackgroundResource(R.drawable.import_btn_bg)
                binding.chatImportBtn.setTextColor(resources.getColor(R.color.btn_bg, null))
            }
            val wallpaperPath = preferences!!.getString(AppConstant.WALLPAPER_PATH, null)
            if (wallpaperPath != null) {
                val file = File(wallpaperPath)
                val drawable = Drawable.createFromPath(file.absolutePath)
                binding.chatWallpaper.setImageDrawable(drawable)
            } else {
                binding.chatWallpaper.setBackgroundResource(R.drawable.ic_wallpaper_default)
            }

            val fontSize = preferences!!.getString(AppConstant.FONT_SIZE, null)
            if (fontSize != null) {
                when {
                    fontSize.equals(resources.getString(R.string.small), ignoreCase = true) -> {
                        mViewModel!!.setmSmall(true)
                    }
                    fontSize.equals(resources.getString(R.string.medium), ignoreCase = true) -> {
                        mViewModel!!.setmMedium(true)
                    }
                    fontSize.equals(resources.getString(R.string.large), ignoreCase = true) -> {
                        mViewModel!!.setmLarge(true)
                    }
                }
            } else {
                mViewModel!!.setmSmall(true)
            }
        }
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adsChatBackup.adViewContainer)
        }
    }

    /**
     * Click and Radio button handler class
     */
    inner class ClickHandlers {
        /**
         * Views click handler
         */
        fun onClickListener(view: View) {
            when (view.id) {
                R.id.chats_back -> {
                    finish()
                }
                R.id.chat_backup_btn -> {
                    if(CentrlUtil.isInternetAvailable(this@ChatsBackUpActivity)){
                        if (!binding.neverBackupBtn.isChecked) {
                            if (account != null) {
                                dialog.show()
                                uploadBackUp(account!!.email, account!!.displayName)
                            } else {
                                dialog.show()
                                mIsSelectedTime = true
                                signIn()
                            }
                        }
                    } else {
                        CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.internet))
                    }
                }
                R.id.chat_import_btn -> {
                    if(CentrlUtil.isInternetAvailable(this@ChatsBackUpActivity)){
                        if (!binding.neverBackupBtn.isChecked) {
                            if (account != null) {
                                dialog.show()
                                downloadFile()
                            } else {
                                dialog.show()
                                signIn()
                            }
                        }
                    } else {
                        CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.internet))
                    }
                }
                R.id.chat_wallpaper -> {
                    if (wallpaperChangeBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                        wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                    } else {
                        wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                    }
                }
                R.id.default_ll -> {
                    binding.chatWallpaper.setImageResource(R.drawable.ic_wallpaper_default)
                    editor!!.putString(AppConstant.WALLPAPER_PATH, null)
                    editor!!.commit()
                    if (wallpaperChangeBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                        wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                    } else {
                        wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                    }
                }
                R.id.gallery_ll -> {
                    if (checkStoragePermission()) {
                        pickImage()
                        if (wallpaperChangeBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                            wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                        } else {
                            wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                        }
                    }
                }
                R.id.camera_ll -> {
                    if (checkCameraPermission()) {
                        if (wallpaperChangeBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
                            wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                        } else {
                            wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                        }
                        try {
                            with(this@ChatsBackUpActivity)
                                .crop(5F,9F)
                                .saveDir(CentrlUtil.getCropPath(this@ChatsBackUpActivity))
                                .cameraOnly()
                                .start()
                            PrefManager.getInstance(this@ChatsBackUpActivity)!!.saveBoolean(AppConstant.SET_IMAGE, true)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
                R.id.bottom_sheet_close -> {
                    wallpaperChangeBehavior.isHideable = true
                    wallpaperChangeBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
                }
            }
        }


        /**
         * Radio button click handler
         */
        @RequiresApi(Build.VERSION_CODES.M)
        fun saveChatBackup(view: View, isChecked: Boolean) {
            when (view.id) {
                R.id.daily_backup_btn -> {
                    if (isChecked) {
                        setBackupTime(resources.getString(R.string.daily))
                       // scheduleRepeatingTasks(1)
                    }
                }
                R.id.weekly_backup_btn -> {
                    if (isChecked) {
                        setBackupTime(resources.getString(R.string.weekly))
                        //scheduleRepeatingTasks(7)
                    }
                }
                R.id.never_backup_btn -> {
                    if (isChecked) {
                        setBackupTime(resources.getString(R.string.never))
                    }
                }
                R.id.smallRadioBtn -> {
                    if (isChecked) {
                        mFontSize = binding.smallRadioBtn.text.toString()
                        editor!!.putString(AppConstant.FONT_SIZE, mFontSize)
                        editor!!.commit()
                    }
                }
                R.id.mediumRadioBtn -> {
                    if (isChecked) {
                        mFontSize = binding.mediumRadioBtn.text.toString()
                        editor!!.putString(AppConstant.FONT_SIZE, mFontSize)
                        editor!!.commit()
                    }
                }
                R.id.largeRadioBtn -> {
                    if (isChecked) {
                        mFontSize = binding.largeRadioBtn.text.toString()
                        editor!!.putString(AppConstant.FONT_SIZE, mFontSize)
                        editor!!.commit()
                    }
                }
            }
        }
    }

    /**
     * This is use to open gallery for select wallpaper
     */
    private fun pickImage() {
        with(this)
            .crop(5F,9F)
            .saveDir(CentrlUtil.getCropPath(this))
            .galleryOnly()
            .start()
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.SET_IMAGE, true)
    }

    /**
     * This is use to setup back time
     */
    @RequiresApi(Build.VERSION_CODES.M)
    private fun setBackupTime(backTime: String) {
        try {
            mBackupTime = backTime
            if (backTime == resources.getString(R.string.never)) {
                editor!!.putString(AppConstant.BACKUP_TIME, mBackupTime)
                editor!!.commit()
                binding.chatBackupBtn.setBackgroundResource(R.drawable.backup_unsync_account)
                binding.chatImportBtn.setBackgroundResource(R.drawable.import_bg_unscync_account)
                binding.chatImportBtn.setTextColor(resources.getColor(R.color.import_txt, null))
                if(account != null && chatBackupEntity.lastBackupTime.isNotEmpty()){
                    mUpdateNever = true
                    val chatBackup = ChatBackupEntity(chatBackupEntity.lastBackupTime, chatBackupEntity.emailID, chatBackupEntity.userName, mBackupTime!!, chatBackupEntity.backupId, chatBackupEntity.dbSize)
                    mViewModel!!.updateChatBackupDetails(chatBackup)
                }
            } else {
                editor!!.putString(AppConstant.BACKUP_TIME, mBackupTime)
                editor!!.commit()
                binding.chatBackupBtn.setBackgroundResource(R.drawable.backup_btn_bg)
                binding.chatImportBtn.setBackgroundResource(R.drawable.import_btn_bg)
                binding.chatImportBtn.setTextColor(resources.getColor(R.color.btn_bg, null))
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This is use to schedule the backup in background
     */
    private fun scheduleRepeatingTasks(repeatInterval: Long) {
        val constraints = Constraints.Builder().apply {
            setRequiredNetworkType(NetworkType.CONNECTED)
            setRequiresBatteryNotLow(true)
        }.build()
        val repeatingWork = PeriodicWorkRequestBuilder<ChatBackupWorker>(repeatInterval, TimeUnit.DAYS)
            .setConstraints(constraints)
            .build()
        WorkManager.getInstance(this).enqueue(repeatingWork)
        //WorkManager.getInstance(this).enqueueUniquePeriodicWork("backupTag", ExistingPeriodicWorkPolicy.KEEP, repeatingWork)
    }

    /**
     * This is use to stop the background backup process
     */
    private fun stopChatBackup() {
        WorkManager.getInstance(this).cancelAllWork()
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    /**
     * Getting data base size as KB, MB
     */
    private fun bytesIntoHumanReadable(bytes: Long): String {
        val kilobyte: Long = 1024
        val megabyte = kilobyte * 1024
        val gigabyte = megabyte * 1024
        val terabyte = gigabyte * 1024
        return when {
            bytes in 0 until kilobyte -> {
                bytes.toString() + "B"
            }
            bytes in kilobyte until megabyte -> {
                (bytes / kilobyte).toString() + "KB"
            }
            bytes in megabyte until gigabyte -> {
                (bytes / megabyte).toString() + "MB"
            }
            bytes in gigabyte until terabyte -> {
                (bytes / gigabyte).toString() + "GB"
            }
            bytes >= terabyte -> {
                (bytes / terabyte).toString() + "TB"
            }
            else -> {
                "$bytes Bytes"
            }
        }
    }

    /*private fun searchFolder(){
        try{
            mDriveServiceHelper!!.searchFile(AppConstant.DATA_BASE, DriveServiceHelper.MIME_TYPE).addOnSuccessListener { result ->
                LogUtil.mLOGE("searchFolder","searchFolder"+result.size)
            }
        }catch (e: Exception){ }
    }*/

    /**
     * This is use to upload backup database to google drive
     */
    private fun uploadBackUp(emailId: String?, userName: String?) {
        try {
            editor!!.putBoolean(AppConstant.FIRST_BACKUP, true)
            editor!!.commit()
            val appDatabase: CentrlDatabase = CentrlDatabase.getInstance(this)
            appDatabase.close()
            mDataBaseSize = bytesIntoHumanReadable(getDatabasePath(AppConstant.DATA_BASE).length())
            if (mDriveServiceHelper == null) {
                return
            }
            mDriveServiceHelper!!.uploadFile(getDatabasePath(AppConstant.DATA_BASE), DriveServiceHelper.MIME_TYPE, null)
                .addOnSuccessListener { googleDriveFileHolder ->
                    val result = Gson()
                    val getResult = result.toJson(googleDriveFileHolder)
                    try {
                        val jsonObject = JSONObject(getResult)
                        val id = jsonObject.getString("id")
                        // Delete old backup file
                        deleteUploadedFile(id, emailId, userName, mBackupOldId)
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                .addOnFailureListener {
                    if (!this.isFinishing) {
                        dialog.dismiss()
                    }
                    CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.backup_fail))
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This is use to delete last backup from google drive
     */
    private fun deleteUploadedFile(newFileId: String, emailId: String?, userName: String?, oldID: String?) {
        try {
            val chatBackup = ChatBackupEntity(DateUtil.currentDateTime!!, emailId!!, userName!!, mBackupTime!!, newFileId, mDataBaseSize!!)
            if (oldID != null) {
                mDriveServiceHelper!!.deleteFolderFile(oldID)
                    .addOnSuccessListener {
                        mViewModel!!.updateChatBackupDetails(chatBackup)
                        editor!!.putString(AppConstant.OLD_BACKUP_ID, newFileId)
                        editor!!.commit()
                        if (!this.isFinishing) {
                            dialog.dismiss()
                        }
                    }
                    .addOnFailureListener {
                        mViewModel!!.updateChatBackupDetails(chatBackup)
                        editor!!.putString(AppConstant.OLD_BACKUP_ID, newFileId)
                        editor!!.commit()
                        if (!this.isFinishing) {
                            dialog.dismiss()
                        }
                    }
            } else {
                mViewModel!!.insertChatBackup(chatBackup)
                editor!!.putString(AppConstant.OLD_BACKUP_ID, newFileId)
                editor!!.commit()
                if (!this.isFinishing) {
                    dialog.dismiss()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This is use to import the backup database from google drive
     */
    private fun downloadFile() {
        try {
            val appDatabase: CentrlDatabase = CentrlDatabase.getInstance(this)
            appDatabase.close()
            val file = CentrlUtil.getRootDirPath(this)
            if (mDriveServiceHelper == null) {
                return
            }
            mDriveServiceHelper!!.downloadFile(File(file, AppConstant.DATA_BASE), mBackupOldId)
                .addOnSuccessListener {
                    val imported = DataBaseExportImport.restoreDb(this)
                    if(imported){
                        if(File(file).exists()){
                            File(file).delete()
                        }
                        CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.import_success))
                    } else {
                        CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.import_fail))
                    }

                    if (!this.isFinishing) {
                        dialog.dismiss()
                    }
                }
                .addOnFailureListener {
                    if (!this.isFinishing) {
                        dialog.dismiss()
                    }
                    CentrlSnackBar.showSnackBar(binding.chatBackupSnackar.snackbarCl, resources.getString(R.string.import_fail))
                }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    public override fun onStart() {
        super.onStart()
        if (account != null) {
            mViewModel!!.setmEmail(account!!.email!!)
            mDriveServiceHelper = DriveServiceHelper(DriveServiceHelper.getGoogleDriveService(applicationContext, account!!, "Centrl"))
        }
    }

    /**
     * Open google sing in option
     */
    private fun signIn() {
        val mGoogleSignInClient = buildGoogleSignInClient()
        startActivityForResult(mGoogleSignInClient.signInIntent, REQUEST_CODE_SIGN_IN)
    }

    /**
     * Config the google sing in
     */
    private fun buildGoogleSignInClient(): GoogleSignInClient {
        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestScopes(SCOPE_FILE)
            .requestEmail()
            .build()
        return GoogleSignIn.getClient(applicationContext, signInOptions)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        super.onActivityResult(requestCode, resultCode, resultData)
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            handleSignInResult(resultData)
        } else {
            val imageUri = resultData!!.data
            if (getFilePath(resultData) != null) {
                Glide.with(this)
                    .load(imageUri)
                    .error(R.drawable.ic_wallpaper_default)
                    .into(binding.chatWallpaper)
                editor!!.putString(AppConstant.WALLPAPER_PATH, getFilePath(resultData))
                editor!!.commit()
                PrefManager.getInstance(this)!!.saveBoolean(AppConstant.SET_IMAGE, false)
            }
        }
    }

    /**
     * Helper method to trigger retrieving the server auth code if we've signed in.
     */
    private fun handleSignInResult(result: Intent?) {
        GoogleSignIn.getSignedInAccountFromIntent(result)
            .addOnSuccessListener { googleSignInAccount ->
                mViewModel!!.setmEmail(googleSignInAccount.email!!)
                mDriveServiceHelper = DriveServiceHelper(DriveServiceHelper.getGoogleDriveService(applicationContext, googleSignInAccount, "Centrl"))
                if(chat_backup_btn.isPressed) {
                    uploadBackUp(googleSignInAccount.email, googleSignInAccount.displayName)
                }
                else{
                    downloadFile()
                }
            }
            .addOnFailureListener {
                if (!this.isFinishing) {
                    dialog.dismiss()
                }
            }
    }

    /**
     * Check camera permission
     */
    private fun checkCameraPermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.camera_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                    openPermissionSettings(this@ChatsBackUpActivity, MY_PERMISSIONS_REQUEST_CAMERA)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                }
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    MY_PERMISSIONS_REQUEST_CAMERA
                )
            }
            false
        } else {
            true
        }
    }

    /**
     * Check storage permission
     */
    private fun checkStoragePermission(): Boolean {
        return if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                // sees the explanation, try again to request the permission.
                // sees the explanation, try again to request the permission.
                val builder = AlertDialog.Builder(this)
                builder.setTitle(resources.getString(R.string.permission))
                builder.setMessage(resources.getString(R.string.storage_txt))
                builder.setCancelable(false)
                builder.setPositiveButton(resources.getString(R.string.continue_txt)) { _, _ ->
                    openPermissionSettings(this@ChatsBackUpActivity, MY_PERMISSIONS_REQUEST_CAMERA)
                }
                val dialog = builder.create()
                dialog.show()
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
                } else {
                    dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
                }

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_CAMERA)

            }
            false
        } else {
            true
        }
    }

    /**
     * Open permissions setting forcefully
     */
    @Suppress("SameParameterValue")
    private fun openPermissionSettings(activity: Activity, requestCode: Int) {
        if (requestCode == MY_PERMISSIONS_REQUEST_CAMERA) {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            val uri = Uri.parse("package:" + activity.packageName)
            intent.data = uri
            if (intent.resolveActivity(activity.packageManager) != null)
                activity.startActivity(intent)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            MY_PERMISSIONS_REQUEST_CAMERA -> {
                return
            }
        }
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    companion object {
        const val MY_PERMISSIONS_REQUEST_CAMERA = 99
        private const val REQUEST_CODE_SIGN_IN = 100
    }
}