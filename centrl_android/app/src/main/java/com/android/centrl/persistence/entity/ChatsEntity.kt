package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.android.centrl.utils.DateUtil

/**
 * Entity model class for a notification chats table
 */
@Entity(tableName = "chats")
data class ChatsEntity(
    @PrimaryKey(autoGenerate = true)
    var chatID: Int = 0,
    var groupID: String,
    var chatKey: String,
    var mailKey: String,
    var messages: String,
    var packageCode: Int,
    var title: String,
    var timeStamp: String,
    var messageType: String,
    var icon: String,
    var notiType: String,
    var number: String

) : Comparable<ChatsEntity> {

    var groupTitle: String? = null
    var groupIconPath: String? = null
    var isSelected = false
    var muteChat = 0
    var groupChat = 0

    override fun compareTo(other: ChatsEntity): Int {
        return DateUtil.convertDateFormat(timeStamp)
            .compareTo(DateUtil.convertDateFormat(other.timeStamp))
    }

    companion object {
        const val MSG_TYPE_SENT = "SENT"
        const val MSG_TYPE_RECEIVED = "RECEIVED"
    }


}