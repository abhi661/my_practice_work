package com.android.centrl.ui.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.android.centrl.R
import com.android.centrl.databinding.DialogSigninEmailBinding
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.utils.AppConstant
/**
 * Google sing-in dialog
 */
class DialogSignInEmail : DialogFragment() {

    private lateinit var binding: DialogSigninEmailBinding
    private lateinit var dialogSingInEmail: DialogSignInEmailListener
    /**
     * Confirm Email interface
     */
    interface DialogSignInEmailListener {
        fun onNext()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater, R.layout.dialog_signin_email, null, false)
        dialog.setContentView(binding.root)

        binding.signinSkipBtn.setOnClickListener{
            PrefManager.getInstance(requireActivity())!!.saveBoolean(AppConstant.GOOGLE_SIGN_SKIP, true)
            dialog.dismiss()
        }

        binding.closeEmailDialog.setOnClickListener {
            PrefManager.getInstance(requireActivity())!!.saveBoolean(AppConstant.GOOGLE_SIGN_SKIP, true)
            dialog.dismiss()
        }

        binding.signinNextBtn.setOnClickListener {

            dialogSingInEmail.onNext()
            dialog.dismiss()
        }

        return dialog
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogSingInEmail = activity as DialogSignInEmailListener
    }

    companion object {
        var TAG = "DialogSignInEmail"
    }
}