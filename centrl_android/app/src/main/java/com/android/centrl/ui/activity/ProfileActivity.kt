package com.android.centrl.ui.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Patterns
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.PopupMenu
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.centrl.R
import com.android.centrl.adapter.GroupChannelsListAdapter
import com.android.centrl.adapter.GroupParticipantsListAdapter
import com.android.centrl.databinding.ActivityProfileBinding
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.ui.dialog.DialogAddNewChannels
import com.android.centrl.ui.dialog.DialogAddNewChannels.DialogAddChannelsListener
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import com.bumptech.glide.Glide
import com.google.android.gms.ads.AdView
import jp.wasabeef.glide.transformations.CropCircleTransformation
import java.util.*

/**
 * Group or Single profile activity.
 */
class ProfileActivity : BaseActivity(), PopupMenu.OnMenuItemClickListener, DialogAddChannelsListener,
    GroupChannelsListAdapter.RemoveChannelInterface {

    private lateinit var binding: ActivityProfileBinding
    private var identityList = ArrayList<GroupChannelsEntity>()
    private var participantsList = ArrayList<String>()
    private var groupIdentityListAdapter: GroupChannelsListAdapter? = null
    private var participantsListAdapter: GroupParticipantsListAdapter? = null
    private lateinit var mPhoneNumber: String
    private var mPackCode = 0
    private var mAdView: AdView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile)
        binding.handler = ClickHandler()
        val bundle = intent.extras
        if (bundle != null) {
            groupKey = bundle.getString(AppConstant.GROUP_KEY)
            participantsList = bundle.getStringArrayList(AppConstant.PARTICIPANTS)!!
        }

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.groupIdentityListRecyclerView.layoutManager = layoutManager

        groupIdentityListAdapter = GroupChannelsListAdapter(this, identityList, this)
        binding.groupIdentityListRecyclerView.adapter = groupIdentityListAdapter

        binding.adViewContainerProfile.adViewContainer.post {
            // Loading ads
            mAdView = AdsLoading.loadBanner(this@ProfileActivity, binding.adViewContainerProfile.adViewContainer)
        }

        mGroupChannelsViewModel!!.mGroupDetails.observe(this, androidx.lifecycle.Observer { chat ->
            try {
                if(chat.groupIconPath.isEmpty()){
                    binding.groupProfileImage.setImageResource(R.drawable.default_user_image)
                } else {
                    val profileImage: Bitmap = BitmapFactory.decodeFile(chat.groupIconPath)
                    binding.groupProfileImage.setImageBitmap(profileImage)
                }

                if (chat.groupTitle.isNotEmpty()) {
                    if (isValidMobile(chat.groupTitle)) {
                        binding.groupProfileTitleTxt.text = resources.getString(R.string.unknown)
                    } else {
                        binding.groupProfileTitleTxt.text = chat.groupTitle
                    }
                }
                mPhoneNumber = chat.groupTitle
                mPackCode = chat.packageCode

                if(chat.isGroup == 0){
                    binding.addNewIdentity.visibility = View.VISIBLE
                    identityList.clear()
                    for (group in mGroupChannelsViewModel!!.getGroupList(groupKey!!)) {
                        identityList.add(group)
                    }
                    groupIdentityListAdapter!!.notifyDataSetChanged()
                } else {
                    binding.addNewIdentity.visibility = View.GONE
                    participantsListAdapter = GroupParticipantsListAdapter(this, participantsList)
                    binding.groupIdentityListRecyclerView.adapter = participantsListAdapter
                }
            }catch (e: Exception){
                e.printStackTrace()
            }
        })
    }
    /**
     * View Click handler claa
     */
    inner class ClickHandler{
        fun onClick(view: View){
            when(view.id){
                R.id.group_profile_menu -> {
                    if (isValidMobile(mPhoneNumber)) {
                        val popupMenu = PopupMenu(this@ProfileActivity, binding.groupProfileMenu)
                        popupMenu.menuInflater.inflate(R.menu.profile_add_menu, popupMenu.menu)
                        popupMenu.setOnMenuItemClickListener(this@ProfileActivity)
                        popupMenu.show()
                    } else {
                        val popupMenu = PopupMenu(this@ProfileActivity, binding.groupProfileMenu)
                        popupMenu.menuInflater.inflate(R.menu.profile_menu, popupMenu.menu)
                        popupMenu.setOnMenuItemClickListener(this@ProfileActivity)
                        popupMenu.show()
                    }
                }
                R.id.group_profile_back_layout -> { finish() }
                R.id.add_new_identity -> {
                    if (mPackCode <= AppConstant.TOTAL_CHATS_APP) {
                        val dialogAddNewChannels = DialogAddNewChannels()
                        val bundle = Bundle()
                        bundle.putString("group_key", groupKey)
                        bundle.putBoolean("ischat", true)
                        dialogAddNewChannels.arguments = bundle
                        dialogAddNewChannels.show(supportFragmentManager, DialogAddNewChannels.TAG)
                    } else {
                        val dialogAddNewChannels = DialogAddNewChannels()
                        val bundle = Bundle()
                        bundle.putString("group_key", groupKey)
                        bundle.putBoolean("ischat", false)
                        dialogAddNewChannels.arguments = bundle
                        dialogAddNewChannels.show(supportFragmentManager, DialogAddNewChannels.TAG)
                    }
                }
            }
        }
    }

    /**
     * Use this to validate the phone number.
     */
    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }

    override fun onResume() {
        super.onResume()
        // Getting chat contact details from local database
        mGroupChannelsViewModel!!.groupDetails(groupKey!!)
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adViewContainerProfile.adViewContainer)
        }
    }

    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }

    /**
     * Handling clicking option for menu.
     */
    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_edit_profile -> {
                val edit = Intent(this, ProfileEditActivity::class.java)
                edit.putExtra(AppConstant.GROUP_KEY, groupKey)
                startActivity(edit)
                true
            }
            R.id.action_settings, R.id.action_new_settings -> {
                startActivity(Intent(this@ProfileActivity, SettingsActivity::class.java))
                true
            }
            R.id.action_profile_add -> {
                val intentInsertEdit = Intent(Intent.ACTION_INSERT_OR_EDIT)
                intentInsertEdit.type = ContactsContract.Contacts.CONTENT_ITEM_TYPE
                intentInsertEdit.putExtra(ContactsContract.Intents.Insert.PHONE, mPhoneNumber)
                startActivityForResult(intentInsertEdit, 1)
                true
            }
            else -> false
        }
    }
    /**
     * Adding new channel to group.
     */
    override fun onAddIdentity(groupKey: String, packCode: Int) {
        Thread.sleep(AppConstant.SLEEP)
        identityList.clear()
        this.groupKey = groupKey
        for (group in mGroupChannelsViewModel!!.getGroupList(groupKey)) {
            identityList.add(group)
        }
        groupIdentityListAdapter!!.notifyDataSetChanged()
        val chat = mGroupChannelsViewModel!!.getGroupDetails(groupKey)
        Glide.with(this)
            .load(chat.groupIconPath)
            .error(R.drawable.default_user_image)
            .bitmapTransform(CropCircleTransformation(this))
            .into(binding.groupProfileImage)
        binding.groupProfileTitleTxt.text = chat.groupTitle
    }
    /**
     * Remove channels from activity.
     */
    override fun removeIdentity(chat: GroupChannelsEntity) {
        removeChannel(chat)
    }
    /**
     * Remove channel alert dialog.
     */
    private fun removeChannel(chat: GroupChannelsEntity) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(resources.getString(R.string.delete_title))
        builder.setMessage(resources.getString(R.string.delete_des))
        builder.setCancelable(true)
        builder.setNegativeButton(resources.getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
        builder.setPositiveButton(resources.getString(R.string.delete)) { dialog, _ ->
            try {
                // create instance of Random class
                val rand = Random()
                // Generate random integers in range 0 to 999
                val groupID = rand.nextInt(10000).toString()
                mGroupChannelsViewModel!!.updateGroup(chat.chatKey, groupID, chat.iconPath, chat.iconPath, chat.title)
                mChatsViewModel!!.updateChat(chat.chatKey, groupID)
                mGroupChannelsViewModel!!.groupDetails(groupKey!!)
                dialog.dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.continue_color, null))
        } else {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#4FC3B8"))
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                fetchAllContact()
                Toast.makeText(this, resources.getString(R.string.contact_added), Toast.LENGTH_SHORT).show()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                fetchAllContact()
                Toast.makeText(this, resources.getString(R.string.contact_cancelled), Toast.LENGTH_SHORT).show()
            }
        }
    }
    /**
     * Fetch added contact details from phone and updating for unknown chat contact.
     */
    private fun fetchAllContact() {
        try {
            val contact = CentrlUtil.getContactDetailsByNumber(mPhoneNumber, this)
            val uniqueKey = NotificationData.md5(contact.phoneNumber + "@" + mPackCode)
            mChatsViewModel!!.updateTitleChat(contact.id!!, contact.name!!, uniqueKey,  mPhoneNumber)
            mGroupChannelsViewModel!!.updateTitleGroup(contact.name!!, contact.id!!, uniqueKey, mPhoneNumber)
            groupKey = contact.id
            chatKey = uniqueKey
            mGroupChannelsViewModel!!.groupDetails(groupKey!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
}