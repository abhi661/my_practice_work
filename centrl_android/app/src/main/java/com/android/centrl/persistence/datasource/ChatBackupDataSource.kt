package com.android.centrl.persistence.datasource

import com.android.centrl.persistence.entity.ChatBackupEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Access point for managing backup data.
 */
interface ChatBackupDataSource {
    /**
     * Inserts the backup into the data source.
     *
     * @param chatBackup the backup to be inserted or updated.
     */
    fun insertOrUpdateBackupDetails(chatBackup: ChatBackupEntity): Completable

    /**
     * @param chatBackup the backup to be updated
     */
    fun updateChatBackupDetails(chatBackup: ChatBackupEntity): Completable
    /**
     * Gets the backup from the data source.
     *
     * @return the backup from the data source.
     */
    fun getBackup(): Flowable<ChatBackupEntity>

    /**
     * Deletes all backup from the data source.
     */
    fun deleteAllBackupDetails(fileId: String)
}