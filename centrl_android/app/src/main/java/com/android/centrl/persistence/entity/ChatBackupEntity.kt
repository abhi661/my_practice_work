package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a Chat backup table
 */
@Entity(tableName = "chatBackup")
data class ChatBackupEntity(
    @PrimaryKey()
    var lastBackupTime: String,
    var emailID: String,
    var userName: String,
    var backupType: String,
    var backupId: String,
    var dbSize: String
)