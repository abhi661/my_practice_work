package com.android.centrl.utils

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import com.android.centrl.application.CentrlApplication
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Date converter
 */
object DateUtil {

    /**
     * Convert date AM and PM format
     */
    fun convertAMFormat(str: String?): String {
        val simpleDateFormat: SimpleDateFormat
        return try {
            val parse = localYYYYMMDDFormat().parse(str!!)
            val str2 = "hh:mm a"
            simpleDateFormat = if (Build.VERSION.SDK_INT >= 24) {
                SimpleDateFormat(str2, CentrlApplication.appContext.resources.configuration.locales[0])
            } else {
                SimpleDateFormat(str2, CentrlApplication.appContext.resources.configuration.locale)
            }
            simpleDateFormat.format(parse!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }
    /**
     * Convert date format
     */
    fun convertDateFormat(str: String): String {
        val simpleDateFormat: SimpleDateFormat
        return try {
            if(str.isNotEmpty()){
                val parse = localYYYYMMDDFormat().parse(str)
                //String str2 = "h:mm";
                val str2 = "yyyy-MM-dd HH:mm:ss a"
                simpleDateFormat = if (Build.VERSION.SDK_INT >= 24) {
                    SimpleDateFormat(str2, CentrlApplication.appContext.resources.configuration.locales[0])
                } else {
                    SimpleDateFormat(str2, CentrlApplication.appContext.resources.configuration.locale)
                }
                simpleDateFormat.format(parse!!)
            } else {
                ""
            }

        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }

    /**
     * Current time format
     */

    fun currentTimeFormat(context: Context): String {
        val simpleDateFormat: SimpleDateFormat
        val date = Date(System.currentTimeMillis())
        val str = "yyyy-MM-dd HH:mm:ss"
        simpleDateFormat = if (Build.VERSION.SDK_INT >= 24) {
            SimpleDateFormat(str, context.resources.configuration.locales[0])
        } else {
            SimpleDateFormat(str, context.resources.configuration.locale)
        }
        return simpleDateFormat.format(date)
    }
    /**
     * Current date format
     */

    fun getCurrentDate(context: Context): String {
        val simpleDateFormat: SimpleDateFormat
        val date = Date(System.currentTimeMillis())
        val str = "yyyy-MM-dd"
        simpleDateFormat = if (Build.VERSION.SDK_INT >= 24) {
            SimpleDateFormat(str, context.resources.configuration.locales[0])
        } else {
            SimpleDateFormat(str, context.resources.configuration.locale)
        }
        return simpleDateFormat.format(date)
    }
    /**
     * Convert date format
     */
    private fun dateFormatYYYYMMDDTIMEFormat(): SimpleDateFormat {
        val str = "yyyy-MM-dd HH:mm:ss.SSSSSS"
        return if (Build.VERSION.SDK_INT >= 24) {
            SimpleDateFormat(str, CentrlApplication.appContext.resources.configuration.locales[0])
        } else SimpleDateFormat(str, CentrlApplication.appContext.resources.configuration.locale)
    }

    fun format(simpleDateFormat: SimpleDateFormat, date: Date?): String {
        return simpleDateFormat.format(date!!)
    }

    /**
     * Get diff day
     */
    fun getDiffDay(calendar: Calendar): Int {
        val instance = Calendar.getInstance()
        instance[calendar[Calendar.YEAR], calendar[Calendar.MONTH]] = calendar[Calendar.DATE]
        val instance2 = Calendar.getInstance()
        instance2[calendar[Calendar.YEAR], calendar[Calendar.MONTH]] = calendar[Calendar.DATE]
        return java.lang.Long.valueOf((instance2.timeInMillis - instance.timeInMillis) / 86400000)
            .toInt() + 1
    }

    /**
     * Get local date formate
     */
    private fun localYYYYMMDDFormat(): SimpleDateFormat {
        val str = "yyyy-MM-dd HH:mm:ss"
        return if (Build.VERSION.SDK_INT >= 24) {
            SimpleDateFormat(str, CentrlApplication.appContext.resources.configuration.locales[0])
        } else SimpleDateFormat(str, CentrlApplication.appContext.resources.configuration.locale
        )
    }
    /**
     * Parse date
     */
    fun parse(str: String?): Date {
        val date = Date()
        return try {
            dateFormatYYYYMMDDTIMEFormat().parse(str!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            date
        }
    }
    /**
     * Get notification date and time
     */
    fun getDbNotiAt(j: Long): String {
        val simpleDateFormat: SimpleDateFormat
        val str = "yyyy-MM-dd HH:mm:ss.SSSSSS"
        simpleDateFormat = if (Build.VERSION.SDK_INT >= 24) {
            SimpleDateFormat(str, CentrlApplication.appContext.resources.configuration.locales[0])
        } else {
            SimpleDateFormat(str, CentrlApplication.appContext.resources.configuration.locale)
        }
        val instance = Calendar.getInstance()
        instance.timeInMillis = j
        return simpleDateFormat.format(instance.time)
    }
    /**
     * Get current date and time
     */
    val currentDateTime: String?
        @SuppressLint("SimpleDateFormat")
        get() {
            try {
                val c = Calendar.getInstance()
               // val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss a")
                val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                return df.format(c.time)
            } catch (e: Exception) {
            }
            return null
        }
    /**
     * Get day before date and time
     */
    val yesterdayDateTime: String?
        @SuppressLint("SimpleDateFormat")
        get() {
            try {
                val c = Calendar.getInstance()
                c.add(Calendar.DAY_OF_YEAR, -1)
                val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss a")
                return df.format(c.time)
            } catch (e: Exception) {
            }
            return null
        }

    /**
     * Convert date to string
     */
    fun convertDateToString(date: String): String {
        val simpleDateFormat: SimpleDateFormat
        return try {
            val parse = localYYYYMMDDFormat().parse(date)
            // String str2 = "dd/MM/yyyy";
            val str2 = "d MMMM yyyy"
            simpleDateFormat = if (Build.VERSION.SDK_INT >= 24) {
                SimpleDateFormat(str2, CentrlApplication.appContext.resources.configuration.locales[0])
            } else {
                SimpleDateFormat(str2, CentrlApplication.appContext.resources.configuration.locale)
            }
            simpleDateFormat.format(parse!!)
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }
}