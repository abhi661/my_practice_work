package com.android.centrl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.android.centrl.R
import kotlin.collections.ArrayList

/**
 * On boarding intro screen adapter
 */
class OnBoardingPagerAdapter : PagerAdapter() {

    private var mPages: ArrayList<String> = ArrayList()
    private var totalViewsCreated = 0
    private var mListener: SimplePagerAdapterListener? = null

    private val onBoarding = intArrayOf(
        R.drawable.intro_one,
        R.drawable.intro_two,
        R.drawable.intro_three
    )

    interface SimplePagerAdapterListener {
        fun onViewsInMemoryChanged(totalViewsInMemory: Int)
    }

    fun addPage(context: Context) {
        mPages.add(0, context.resources.getString(R.string.page_one))
        mPages.add(1, context.resources.getString(R.string.page_two))
        mPages.add(2, context.resources.getString(R.string.page_threee))
    }

    override fun getCount(): Int {
        return mPages.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val inflater = LayoutInflater.from(container.context)
        val view = inflater.inflate(R.layout.pager_item, container, false)
        (view.findViewById<View>(R.id.intro_text) as TextView).text = mPages[position]
        val introImage = view.findViewById<ImageView>(R.id.intro_images)
        introImage.setImageResource(onBoarding[position])
        container.addView(view)
        totalViewsCreated++
        notifyListener()
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
        totalViewsCreated--
        notifyListener()
    }

    fun setListener(activity: SimplePagerAdapterListener?) {
        mListener = activity
    }

    private fun notifyListener() {
        if (null == mListener) {
            return
        }
        mListener!!.onViewsInMemoryChanged(totalViewsCreated)
    }
}