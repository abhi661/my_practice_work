package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a blocked contacts table
 */
@Entity(tableName = "blockedContacts")
data class BlockedContactsEntity(
    @PrimaryKey
    var groupID: String,
    var chatKey: String,
    var number: String
)