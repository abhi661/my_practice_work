package com.android.centrl.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Data Access Object for the keyboard apps tray table.
 */
@Dao
interface KeyboardAppTrayDao {

    /**
     * Insert a keyboard apps details in the database. If the keyboard apps details already exists, replace it.
     *
     * @param keyboardAppsTrayEntity the keyboard apps details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeyboardAppTray(keyboardAppsTrayEntity: KeyboardAppsTrayEntity): Completable

    /**
     * Get the keyboard apps details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the keyboard apps details from the table
     */
    @Query("SELECT packagesName FROM keyboardAppsTray")
    fun getAllKeyboardAppTrayPackName(): Flowable<List<String>>


    /**
     * Get the keyboard apps details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the keyboard apps details from the table
     */
    @Query("SELECT packageCode FROM keyboardAppsTray")
    fun getAllKeyboardAppTrayPackCode(): Flowable<List<Int>>

    /**
     * Delete all keyboard apps details.
     */
    @Query("DELETE FROM keyboardAppsTray WHERE packageCode = :code")
    fun deleteKeyboardAppsTray(code: Int)

    /**
     * Insert a keyboard apps details in the database. If the keyboard apps details already exists, replace it.
     *
     * @param keyboardAppsTrayEntity the keyboard apps details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKeyboardAppsTray(keyboardAppsTrayEntity: KeyboardAppsTrayEntity)

    /**
     * Get the keyboard apps details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the keyboard apps details from the table
     */
    @Query("SELECT packagesName FROM keyboardAppsTray")
    fun getAllKeyboardAppsTrayPackName(): List<String>

    /**
     * Get the keyboard apps details from the table. Since for simplicity we only have one backup details in the database,
     *
     * @return the keyboard apps details from the table
     */
    @Query("SELECT packageCode FROM keyboardAppsTray")
    fun getAllKeyboardAppsTrayPackCode(): List<Int>
}