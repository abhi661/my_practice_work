package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.persistence.datasource.BlockedContactsDataSource

/**
 * Factory for ViewModels
 */
class BlockedContactsViewModelFactory(
    private val application: Application,
    private val mDataSource: BlockedContactsDataSource
) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BlockedContactsViewModel::class.java)) {
            return BlockedContactsViewModel(application, mDataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}