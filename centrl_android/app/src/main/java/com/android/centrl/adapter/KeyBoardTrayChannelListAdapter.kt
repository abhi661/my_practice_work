package com.android.centrl.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.interfaces.SelectChannel
import com.android.centrl.R
import com.android.centrl.models.KeyboardAppTrayModel
import com.android.centrl.utils.CentrlUtil
import java.util.*
/**
 * Keyboard tray channels list adapter
 */
class KeyBoardTrayChannelListAdapter(private val messageList: ArrayList<KeyboardAppTrayModel>, var selectIdentity: SelectChannel) : RecyclerView.Adapter<KeyBoardTrayChannelListAdapter.MessageItemViewHolder>() {

    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MessageItemViewHolder {
        val itemView = LayoutInflater.from(viewGroup.context).inflate(R.layout.chat_item, viewGroup, false)
        return MessageItemViewHolder(itemView)
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: MessageItemViewHolder, position: Int) {
        val keyboardAppTrayModel = messageList[position]
        if (keyboardAppTrayModel.packageCode == 40) {
            holder.imageLeft.setImageResource(R.drawable.ic_un_filter)
        } else {
            if(keyboardAppTrayModel.packageCode != 0){
                if(keyboardAppTrayModel.isActive == 1){
                    holder.imageLeft.setImageResource(CentrlUtil.getChannelsLogo()[keyboardAppTrayModel.packageCode]!!)
                } else {
                    holder.imageLeft.setImageResource(CentrlUtil.getChannelsUnActiveLogo()[keyboardAppTrayModel.packageCode]!!)
                }
            }
        }
        holder.imageLeft.setOnClickListener { selectIdentity.channelSelected(keyboardAppTrayModel.packageCode)
        }
    }

    override fun getItemCount(): Int {
        return messageList.size
    }
    /**
     * Adapter view holder
     */
    class MessageItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageLeft: ImageView = itemView.findViewById<View>(R.id.ivProfileLeft) as ImageView
    }
}