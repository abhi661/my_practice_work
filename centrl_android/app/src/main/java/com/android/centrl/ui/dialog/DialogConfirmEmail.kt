package com.android.centrl.ui.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.inputmethod.InputMethodManager
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.android.centrl.R
import com.android.centrl.databinding.DialogConfirmEmailBinding
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlSnackBar
import com.google.android.material.textfield.TextInputLayout
import java.lang.reflect.Field
import java.lang.reflect.Method


/**
 * Confirm email dialog
 */
class DialogConfirmEmail : DialogFragment() {

    private lateinit var dialogConfirmEmail: DialogConfirmEmailListener
    private lateinit var binding: DialogConfirmEmailBinding
    /**
     * Confirm Email interface
     */
    interface DialogConfirmEmailListener {
        fun onSentLink()
    }

    @SuppressLint("ResourceType")
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater, R.layout.dialog_confirm_email, null, false)
        dialog.setContentView(binding.root)
        val email = requireArguments().getString(AppConstant.EMAIL).toString()
        val imm = (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
        imm.hideSoftInputFromWindow(binding.emailTxt.windowToken, 0)
        if(email.isNotEmpty()){
            setInputTextLayoutColor(requireActivity().resources.getColor(R.color.colorPrimary, null),  binding.confirmDialogTil)
            binding.emailTxt.setText(email)
            binding.emailTxt.setSelection(binding.emailTxt.text!!.length)
        } else {
            setInputTextLayoutColor(requireActivity().resources.getColor(R.color.chat_msg, null),  binding.confirmDialogTil)
        }

        binding.confirmCancelBtn.setOnClickListener {
            dialog.dismiss()
        }

        binding.closeConfirmDialog.setOnClickListener {
            dialog.dismiss()
        }

        binding.confirmSendBtn.setOnClickListener {
            if(binding.emailTxt.text.toString().isEmpty()) {
                CentrlSnackBar.showSnackBar(binding.snackbarConfirmEmail.snackbarCl, resources.getString(R.string.enter_email))
            }else {
                if (isValidEmail(binding.emailTxt.text!!)) {
                    dialogConfirmEmail.onSentLink()
                    dialog.dismiss()
                } else {
                    CentrlSnackBar.showSnackBar(binding.snackbarConfirmEmail.snackbarCl, resources.getString(R.string.invalid_email))
                }
            }
        }

        return dialog
    }
    private fun isValidEmail(target: CharSequence): Boolean {
        return !(TextUtils.isEmpty(target) || !Patterns.EMAIL_ADDRESS.matcher(target)
            .matches())
    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        dialogConfirmEmail = activity as DialogConfirmEmailListener
    }

    private fun setInputTextLayoutColor(
        color: Int,
        textInputLayout: TextInputLayout
    ) {
        try {
            val field: Field = textInputLayout.javaClass.getDeclaredField("mFocusedTextColor")
            field.isAccessible = true
            val states = arrayOf(intArrayOf())
            val colors = intArrayOf(
                color
            )
            val myList = ColorStateList(states, colors)
            field.set(textInputLayout, myList)

            val fDefaultTextColor: Field = TextInputLayout::class.java.getDeclaredField("mDefaultTextColor")
            fDefaultTextColor.isAccessible = true
            fDefaultTextColor.set(textInputLayout, myList)
            val method: Method = textInputLayout.javaClass.getDeclaredMethod("updateLabelState", Boolean::class.javaPrimitiveType)
            method.isAccessible = true
            method.invoke(textInputLayout, true)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        var TAG = "DialogConfirmEmail"
    }
}