package com.android.centrl.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.centrl.interfaces.RecyclerViewClickListener
import com.android.centrl.R
import com.android.centrl.adapter.SelectContactListAdapter
import com.android.centrl.databinding.ActivitySelectContactBinding
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.persistence.entity.BlockedContactsEntity
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.ui.views.RecyclerViewTouchListener
import com.android.centrl.utils.AdsLoading
import com.android.centrl.utils.DrawableClickListener
import com.google.android.gms.ads.AdView
import java.util.*

/**
 * Select contacts to block activity.
 */
class SelectContactActivity : BaseActivity(), SelectContactListAdapter.FilterSearchInterface {

    private lateinit var binding: ActivitySelectContactBinding
    private var selectContactListAdapter: SelectContactListAdapter? = null
    private var mAllChatContactList = ArrayList<ChatsEntity>()
    private var mChatContactList = ArrayList<ChatsEntity>()
    private var animation: Animation? = null
    private var mAdView: AdView? = null
    private var isSearchOpen = false
    private var mSearchedTxt = ""

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_select_contact)

        binding.selectContactTitleRl.visibility = View.VISIBLE
        binding.searchLlSelectContact.visibility = View.GONE

        animation = AnimationUtils.loadAnimation(this, R.anim.search_right_to_left)
        val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.selectContactRecycleView.layoutManager = linearLayoutManager
        selectContactListAdapter = SelectContactListAdapter(this, mChatContactList, this)
        binding.selectContactRecycleView.adapter = selectContactListAdapter

        binding.selectContactBack.setOnClickListener { finish() }

        binding.selectContactRecycleView.addOnItemTouchListener(RecyclerViewTouchListener(this, binding.selectContactRecycleView,
                object : RecyclerViewClickListener {
                    override fun onClick(view: View, position: Int) {
                        val chat = selectContactListAdapter!!.chatArrayList!![position]
                        mBlockedContactsViewModel!!.insertBlockedContacts(
                            BlockedContactsEntity(
                                chat.groupID,
                                chat.chatKey,
                                chat.groupTitle!!
                            )
                        )
                        PendingIntentManager.getInstance(this@SelectContactActivity).getBlockedContact()
                        finish()
                        val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                        imm.hideSoftInputFromWindow(binding.searchBackSc.windowToken, 0)
                    }
                    override fun onLongClick(view: View, position: Int) {
                        //
                    }
                })
        )

        binding.selectContactSearch.setOnClickListener {
            isSearchOpen = true
            binding.searchLlSelectContact.startAnimation(animation)
            binding.searchLlSelectContact.visibility = View.VISIBLE
            binding.selectContactTitleRl.visibility = View.GONE
            binding.searchEditTxtSc.requestFocus()
            binding.searchEditTxtSc.isCursorVisible = true
            (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).showSoftInput(binding.searchEditTxtSc, InputMethodManager.SHOW_FORCED)
        }
        binding.searchBackSc.setOnClickListener {
            isSearchOpen = false
            binding.selectContactTitleRl.startAnimation(animation)
            binding.selectContactTitleRl.visibility = View.VISIBLE
            binding.searchLlSelectContact.visibility = View.GONE
            binding.searchEditTxtSc.setText("")
            binding.searchEditTxtSc.hint = resources.getString(R.string.search)
            (Objects.requireNonNull(getSystemService(Context.INPUT_METHOD_SERVICE)) as InputMethodManager).hideSoftInputFromWindow(binding.searchBackSc.windowToken, 0)
        }

        binding.searchEditTxtSc.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                //
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                mSearchedTxt = s.toString().trim { it <= ' ' }
                selectContactListAdapter!!.filter.filter(mSearchedTxt)

            }

            override fun afterTextChanged(s: Editable) {
                //
            }
        })
        binding.searchEditTxtSc.setOnTouchListener(object : DrawableClickListener.RightDrawableClickListener(binding.searchEditTxtSc) {
            override fun onDrawableClick(): Boolean {
                binding.searchEditTxtSc.setText("")
                selectContactListAdapter!!.filter.filter("")
                binding.searchEditTxtSc.hint = resources.getString(R.string.search)
                return false
            }
        })
        binding.searchEditTxtSc.setOnEditorActionListener { _: TextView?, actionId: Int, _: KeyEvent? ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                selectContactListAdapter!!.filter.filter(mSearchedTxt)
                val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                imm.hideSoftInputFromWindow(binding.searchBackSc.windowToken, 0)
                return@setOnEditorActionListener true
            }
            false
        }
        binding.adViewContainerSc.adViewContainer.post {
            mAdView = AdsLoading.loadBanner(this, binding.adViewContainerSc.adViewContainer)
        }

        mBlockedContactsViewModel!!.mBlockedContactsList.observe(this, androidx.lifecycle.Observer { blockedList ->
            try {
                mChatContactList.clear()
                mAllChatContactList.clear()

                mAllChatContactList = mChatsViewModel!!.getAllChatsLists()
                mChatContactList.addAll(mAllChatContactList)

                for (chat in mAllChatContactList) {
                    for (blockedChat in blockedList) {
                        if (chat.groupID.equals(blockedChat.groupID, ignoreCase = true)) {
                            mChatContactList.remove(chat)
                        }
                    }
                }
                selectContactListAdapter!!.updateList(mChatContactList)
                if (mChatContactList.size == 0) {
                    binding.emptySelectedRl.visibility = View.VISIBLE
                    binding.selectContactRecycleView.visibility = View.GONE
                } else {
                    binding.emptySelectedRl.visibility = View.GONE
                    binding.selectContactRecycleView.visibility = View.VISIBLE
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    override fun onResume() {
        super.onResume()
        mBlockedContactsViewModel!!.getAllBlockedList()
        if (mAdView != null) {
            AdsLoading.resumeAds(this, mAdView, binding.adViewContainerSc.adViewContainer)
        }
    }
    override fun onPause() {
        if (mAdView != null) {
            mAdView!!.pause()
        }
        super.onPause()
    }

    override fun onDestroy() {
        if (mAdView != null) {
            mAdView!!.destroy()
        }
        super.onDestroy()
    }
    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }
    /**
     * Search filter result
     */
    override fun filterResult(size: Int) {
        if (size == 0) {
            binding.noResultFoundTxtSc.visibility = View.VISIBLE
            binding.selectContactRecycleView.visibility = View.GONE
        } else {
            binding.noResultFoundTxtSc.visibility = View.GONE
            binding.selectContactRecycleView.visibility = View.VISIBLE
        }
    }
}