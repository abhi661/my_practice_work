package com.android.centrl.viewmodel

import android.app.Application
import com.android.centrl.persistence.entity.ChatsCountsEntity
import com.android.centrl.persistence.entity.MultiChatChannelsEntity
import com.android.centrl.persistence.repositry.MultiChannelCountRepository

/**
 * View model for get multi channels and chat counts
 */
class MultiChannelCountViewModel(application: Application) : BaseViewModel(application) {

    private var multiChannelCountRepository: MultiChannelCountRepository =
        MultiChannelCountRepository(application)
    /**
     * @return the chats count from the table
     */
    fun getAllCountID(): ArrayList<ChatsCountsEntity> {
        return multiChannelCountRepository.getAllCountID()
    }
    /**
     * @return the chats count from the table
     */
    fun getCount(groupId: String): Int {
        return multiChannelCountRepository.getCount(groupId)
    }
    /**
     * @return the chats count from the table
     */
    fun getAllCount(): ArrayList<ChatsCountsEntity> {
        return multiChannelCountRepository.getAllCount()
    }
    /**
     * Delete chats count.
     */
    fun deleteCount(groupId: String) {
        multiChannelCountRepository.deleteCount(groupId)
    }
    /**
     * @return the multi channel details from the table
     */
    fun getAllGroupMultiChat(): ArrayList<MultiChatChannelsEntity> {
        return multiChannelCountRepository.getAllGroupMultiChat()
    }
    /**
     * Delete group package code details.
     */
    fun deleteGroupAllPackageCode(groupId: String) {
        multiChannelCountRepository.deleteGroupAllPackageCode(groupId)
    }

}