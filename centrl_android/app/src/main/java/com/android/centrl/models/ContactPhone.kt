package com.android.centrl.models

/**
 * Contact get phone number model class.
 */
class ContactPhone(var number: String, var type: Int)