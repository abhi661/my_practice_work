package com.android.centrl.notificationservice

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import android.text.format.DateUtils
import android.view.View
import android.widget.RemoteViews
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import com.android.centrl.R
import com.android.centrl.appiconbadger.ShortcutBadger
import com.android.centrl.persistence.repositry.MultiChannelCountRepository
import com.android.centrl.ui.activity.ChatsRoomActivity
import com.android.centrl.ui.activity.MailChatActivity
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import java.util.*

/**
 * Centrl notification manager
 */
class CentrlNotificationManager(var context: Context) {

    var preferences: SharedPreferences = context.getSharedPreferences(AppConstant.PREF_NAME, Context.MODE_PRIVATE)
    var mMutiChannelRepository: MultiChannelCountRepository = MultiChannelCountRepository(context)
    /**
     * Centrl foreground notification
     */
    val centrlForegroundNotification: Notification
        @SuppressLint("WrongConstant")
        get() {
            // handle build version above android oreo
            val mNotificationManager = NotificationManagerCompat.from(context)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O && mNotificationManager.getNotificationChannel(FOREGROUND_CHANNEL_ID) == null) {
                val name: CharSequence = context.getString(R.string.text_name_notification)
                val importance = NotificationManagerCompat.IMPORTANCE_DEFAULT
                val channel = NotificationChannel(FOREGROUND_CHANNEL_ID, name, importance)
                channel.enableVibration(false)
                channel.setShowBadge(false)
                mNotificationManager.createNotificationChannel(channel)
            }
            val notificationIntent = Intent(Intent.ACTION_POWER_USAGE_SUMMARY)
            val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
            val remoteViews = RemoteViews(context.packageName, R.layout.forground_notification)

            // notification builder
            val notificationBuilder: NotificationCompat.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationCompat.Builder(context, FOREGROUND_CHANNEL_ID)
            } else {
                NotificationCompat.Builder(context)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                notificationBuilder
                    .setContent(remoteViews)
                    .setCustomBigContentView(remoteViews)
                    .setSmallIcon(R.drawable.ic_group_icon)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setOngoing(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
            } else {
                notificationBuilder
                    .setCustomBigContentView(remoteViews)
                    .setSmallIcon(R.drawable.ic_group_icon)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setOngoing(true)
                    .setContentTitle("Centrl is running in Background.")
                    .setContentText("Tap for details on battery usage")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentIntent(pendingIntent)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            }
            return notificationBuilder.build()
        }
    /**
     * Centrl notifications
     */
    @SuppressLint("WrongConstant")
    fun showCentrlNotification(notificationData: NotificationData) {
        try {
            val mChatBadgeGroupList = ArrayList<String>()
            for (chatCount in mMutiChannelRepository.getAllCountID()) {
                val groupId = chatCount.groupID
                if (!mChatBadgeGroupList.contains(groupId)) {
                    mChatBadgeGroupList.add(groupId)
                }
            }
            var notificationCount = mChatBadgeGroupList.size
            ShortcutBadger.applyCount(context, notificationCount)
            val mNotificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val name: CharSequence = context.getString(R.string.text_name_chat_notification)
                val importance = NotificationManagerCompat.IMPORTANCE_HIGH
                val channel = NotificationChannel(CHANNEL_ID, name, importance)
                channel.setShowBadge(true)
                val att = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SPEECH)
                    .build()
                if (preferences.getBoolean(AppConstant.LIGHT_UP, false)) {
                    channel.enableLights(true)
                    channel.lightColor = Color.WHITE
                } else {
                    channel.enableLights(false)
                }
                if (preferences.getBoolean(AppConstant.VIBRATE, false)) {
                    channel.enableVibration(true)
                    channel.vibrationPattern = longArrayOf(100, 200, 300, 400, 500, 400, 300, 200, 400)
                } else {
                    channel.enableVibration(false)
                }
                if (preferences.getBoolean(AppConstant.NOTI_SOUND, false)) {
                    val sound = Uri.parse("android.resource://" + context.packageName + "/" + R.raw.swiftly)
                    channel.setSound(sound, att)
                }
                if (preferences.getBoolean(AppConstant.SHOW_MSG, false)) {
                    channel.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                }
                mNotificationManager.createNotificationChannel(channel)
            }
            val notificationIntent: Intent = if (notificationData.packCode == AppConstant.GMAIL_CODE || notificationData.packCode == AppConstant.OUTLOOK_CODE) {
                Intent(context, MailChatActivity::class.java)
            } else {
                Intent(context, ChatsRoomActivity::class.java)
            }

            notificationIntent.putExtra(AppConstant.GROUP_KEY, notificationData.groupKey)
            notificationIntent.putExtra(AppConstant.CHAT_KEY, notificationData.uniqueKey)
            notificationIntent.putExtra(AppConstant.PACKAGE_CODE_KEY, notificationData.packCode)
            notificationIntent.putExtra(AppConstant.IS_FROM_NOTIFICATION, true)
            notificationIntent.putExtra(AppConstant.SHARED_TEXT_KEY, "")
            val pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT)
            val remoteViewsBigContent = RemoteViews(context.packageName, R.layout.notification_expanded)
            remoteViewsBigContent.setTextViewText(R.id.content_title, notificationData.groupTitle)
            if (notificationData.packCode == AppConstant.GMAIL_CODE) {
                if (notificationData.content!!.contains("\n")) {
                    try {
                        val temp = notificationData.content
                        val tempStr = temp!!.split("\n".toRegex()).toTypedArray()
                        val mailSubject = tempStr[0]
                        val mailSB = StringBuffer()
                        mailSB.append(mailSubject)
                        mailSB.append("\t")
                        for (i in 1 until tempStr.size) {
                            // mailSB.append("\n");
                            mailSB.append(tempStr[i])
                        }
                        val content = mailSB.toString()
                        remoteViewsBigContent.setTextViewText(R.id.content_text, content)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else {
                    remoteViewsBigContent.setTextViewText(R.id.content_text, notificationData.content)
                }
            } else {
                if(notificationData.isGroupChat == 1){
                    if(notificationData.content.contains("@@")){
                        val tempMSG = notificationData.content.split("@@")
                        val title = tempMSG[0]
                        val msg = tempMSG[1].replaceFirst(" ", "")
                        remoteViewsBigContent.setTextViewText(R.id.content_text, "$title: $msg")
                    } else {
                        remoteViewsBigContent.setTextViewText(R.id.content_text, notificationData.content)
                    }
                } else {
                    remoteViewsBigContent.setTextViewText(R.id.content_text, notificationData.content)
                }
            }

            if (notificationCount > 1) {
                notificationCount -= 1
                remoteViewsBigContent.setViewVisibility(R.id.notification_info, View.VISIBLE)
                remoteViewsBigContent.setTextViewText(R.id.notification_pack_count_txt, "+$notificationCount")
            } else {
                remoteViewsBigContent.setViewVisibility(R.id.notification_info, View.GONE)
            }
            remoteViewsBigContent.setImageViewResource(R.id.notification_type_img, CentrlUtil.getChannelsLogo()[notificationData.packCode]!!)
            if (notificationData.groupIconPath.isEmpty()) {
                remoteViewsBigContent.setImageViewResource(R.id.notification_profile_pic, R.drawable.default_user_image)
            } else {
                val iconBitmap = BitmapFactory.decodeFile(notificationData.groupIconPath)
                remoteViewsBigContent.setImageViewBitmap(R.id.notification_profile_pic, iconBitmap)
            }
            remoteViewsBigContent.setTextViewText(R.id.notification_time_txt, DateUtils.formatDateTime(context, System.currentTimeMillis(), DateUtils.FORMAT_SHOW_TIME))

            // notification builder
            val notificationBuilder: NotificationCompat.Builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationCompat.Builder(context, CHANNEL_ID)
            } else {
                NotificationCompat.Builder(context)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                notificationBuilder
                    .setCustomBigContentView(remoteViewsBigContent)
                    .setCustomContentView(remoteViewsBigContent)
                    .setContentTitle(notificationData.groupTitle)
                    .setContentText(notificationData.content)
                    .setSmallIcon(R.drawable.ic_group_icon)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setAutoCancel(true)
                    .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                    .setNumber(notificationCount)
                    .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setContentIntent(pendingIntent)
            } else {
                if (notificationData.groupIconPath != null) {
                    val iconBitmap = BitmapFactory.decodeFile(notificationData.groupIconPath)
                    notificationBuilder
                        .setContentTitle(notificationData.groupTitle)
                        .setContentText(notificationData.content)
                        .setCustomBigContentView(remoteViewsBigContent)
                        .setLargeIcon(iconBitmap)
                        .setSmallIcon(R.drawable.ic_group_icon)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setNumber(notificationCount).color = ContextCompat.getColor(context, R.color.colorPrimary)
                } else {
                    notificationBuilder
                        .setContentTitle(notificationData.groupTitle)
                        .setContentText(notificationData.content)
                        .setCustomBigContentView(remoteViewsBigContent)
                        .setSmallIcon(R.drawable.ic_group_icon)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(true)
                        .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
                        .setNumber(notificationCount)
                        .setColor(ContextCompat.getColor(context, R.color.colorPrimary))
                        .setContentIntent(pendingIntent)
                }
            }
            if (preferences.getBoolean(AppConstant.NOTI_SOUND, false)) {
                notificationBuilder.setSound(Uri.parse("android.resource://" + context.packageName + "/" + R.raw.swiftly))
            }
            if (preferences.getBoolean(AppConstant.LIGHT_UP, false)) {
                notificationBuilder.setLights(Color.WHITE, 1000, 500)
            }
            if (preferences.getBoolean(AppConstant.VIBRATE, false)) {
                notificationBuilder.setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (preferences.getBoolean(AppConstant.SHOW_MSG, false)) {
                    notificationBuilder.setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                }
            }
            val build = notificationBuilder.build()
            if (preferences.getBoolean(AppConstant.LIGHT_UP, false)) {
                build.ledARGB = -0x10000
                build.flags = Notification.FLAG_SHOW_LIGHTS
                build.ledOnMS = 100
                build.ledOffMS = 100
            }
            ShortcutBadger.applyNotification(context, build, notificationCount)
            mNotificationManager.notify(0, build)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    companion object {
        private const val FOREGROUND_CHANNEL_ID = "5"
        private const val CHANNEL_ID = "6"
        private var notificationManager: CentrlNotificationManager? = null
        fun getInstance(context: Context): CentrlNotificationManager? {
            synchronized(CentrlNotificationManager::class.java) {
                if (notificationManager == null) {
                    notificationManager =
                        CentrlNotificationManager(context)
                }
            }
            return notificationManager
        }
    }
}