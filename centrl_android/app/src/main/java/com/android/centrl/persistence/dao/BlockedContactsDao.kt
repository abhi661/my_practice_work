package com.android.centrl.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.centrl.persistence.entity.BlockedContactsEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Data Access Object for the blocked contacts table.
 */
@Dao
interface BlockedContactsDao {
    /**
     * Insert a blocked contacts details in the database. If the blocked contacts details already exists, replace it.
     *
     * @param blockedContactsEntity the blocked contacts details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBlockedContacts(blockedContactsEntity: BlockedContactsEntity): Completable

    /**
     * Get the blocked contacts details from the table. Since for simplicity we only have one blocked contacts in the database,
     *
     * @return the blocked contacts details from the table
     */
    @Query("SELECT * FROM blockedContacts")
    fun getAllBlockedList(): Flowable<List<BlockedContactsEntity>>

    /**
     * Get the blocked contacts details from the table. Since for simplicity we only have one blocked contacts details in the database,
     *
     * @return the blocked contacts details from the table
     */
    @Query("SELECT * FROM blockedContacts WHERE groupID = :groupId")
    fun getBlockedDetails(groupId: String): Flowable<BlockedContactsEntity>

    /**
     * Delete all blocked contacts details.
     */
    @Query("DELETE FROM blockedContacts WHERE groupID = :groupId")
    fun deleteBlockedContact(groupId: String)

    /**
     * Get the blocked contacts details from the table. Since for simplicity we only have one blocked contacts details in the database,
     *
     * @return the blocked contacts details from the table
     */
    @Query("SELECT * FROM blockedContacts")
    fun getBlockedContactsDetails(): List<BlockedContactsEntity>

    /**
     * Get the blocked contacts details from the table. Since for simplicity we only have one blocked contacts details in the database,
     *
     * @return the blocked contacts details from the table
     */
    @Query("SELECT * FROM blockedContacts WHERE groupID = :groupId")
    fun getBlockedContactDetails(groupId: String): BlockedContactsEntity
}