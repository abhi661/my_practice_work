package com.android.centrl.interfaces
/**
 * Pattern lock setup listener
 */
interface PatternDrawInterface {
    fun firstDrawPattern(pattern: String)
}