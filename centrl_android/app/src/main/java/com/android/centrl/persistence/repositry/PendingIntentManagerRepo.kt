package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.BlockedContactsDao
import com.android.centrl.persistence.dao.InstalledAppsDao
import com.android.centrl.persistence.dao.KeyboardAppTrayDao
import com.android.centrl.persistence.dao.RemovedKeyboardAppsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.BlockedContactsEntity
import com.android.centrl.persistence.entity.InstalledAppsEntity
import com.android.centrl.persistence.entity.KeyboardAppsTrayEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for managing get blocked contacts, install packages.
 */
class PendingIntentManagerRepo(context: Context) {

    private var blockedContactsDao: BlockedContactsDao
    private var mInstalledDao: InstalledAppsDao
    private var keyboardAppTrayDao: KeyboardAppTrayDao
    private var removedKeyboardAppTrayDao: RemovedKeyboardAppsDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        blockedContactsDao = centrlDatabase.bockedContactsDao()
        mInstalledDao = centrlDatabase.installedAppsDao()
        keyboardAppTrayDao = centrlDatabase.keyBoardAppTrayDao()
        removedKeyboardAppTrayDao = centrlDatabase.removedKeyBoardAppsDao()
    }

    /**
     * Insert a installed apps details in the database. If the installed apps details already exists, replace it.
     *
     * @param installedAppsEntity the installed apps details to be inserted.
     */
    fun insertInstalledApps(installedAppsEntity: InstalledAppsEntity) {
        try {
            Completable.fromAction {
                mInstalledDao.insertInstalledApps(installedAppsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Insert a keyboard apps details in the database. If the keyboard apps details already exists, replace it.
     *
     * @param keyboardAppsEntity the keyboard apps details to be inserted.
     */
    fun insertKeyboardAppTray(keyboardAppsEntity: KeyboardAppsTrayEntity) {
        try {
            Completable.fromAction {
                keyboardAppTrayDao.insertKeyboardAppsTray(keyboardAppsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * @return the blocked list from the table
     */
    fun getAllBlockedList(): ArrayList<BlockedContactsEntity> {
        return getBlockedListAsyncTask()
    }

    /**
     * @return the keyboard apps details from the table
     */
    fun getAllKeyboardAppTrayPackCode(): ArrayList<Int> {
        return getAppTrayPackCodeAsyncTask()
    }

    /**
     * @return the keyboard apps details from the table
     */
    fun getAllKeyboardAppTrayPackName(): ArrayList<String> {
        return getAppTrayPackNameAsyncTask()
    }

    /**
     * @return the keyboard apps details from the table
     */
    fun getRemovedAllKeyboardAppPackName(): ArrayList<Int> {
        return getRemovedAppPackNameAsyncTask()
    }

    /**
     * @return the installed app package name from the table
     */
    fun getAllInstalledPackageName(): ArrayList<String> {
        return getAllInstalledPackagesAsyncTask()
    }

    /**
     * AsyncTask for get app tray package code from database
     */
    private fun getAppTrayPackCodeAsyncTask(): ArrayList<Int> {
        val result = doAsyncResult(null,{
            keyboardAppTrayDao.getAllKeyboardAppsTrayPackCode() as ArrayList<Int>
        })
        return result.get()
    }

    /**
     * AsyncTask for get app tray package name from database
     */
    private fun getAppTrayPackNameAsyncTask(): ArrayList<String> {
        val result = doAsyncResult(null,{
            keyboardAppTrayDao.getAllKeyboardAppsTrayPackName() as ArrayList<String>
        })
        return result.get()
    }

    /**
     * AsyncTask for get app tray package name from database
     */
    private fun getRemovedAppPackNameAsyncTask(): ArrayList<Int> {
        val result = doAsyncResult(null,{
            removedKeyboardAppTrayDao.getRemovedChatAppPackages() as ArrayList<Int>
        })
        return result.get()
    }

    /**
     * AsyncTask for get blocked list from database
     */
    private fun getBlockedListAsyncTask(): ArrayList<BlockedContactsEntity> {
        val result = doAsyncResult(null,{
            blockedContactsDao.getBlockedContactsDetails() as ArrayList<BlockedContactsEntity>
        })
        return result.get()
    }

    /**
     * AsyncTask for get blocked list from database
     */
    private fun getAllInstalledPackagesAsyncTask(): ArrayList<String> {
        val result = doAsyncResult(null,{
            mInstalledDao.getAllInstalledPackageName() as ArrayList<String>
        })
        return result.get()
    }
}