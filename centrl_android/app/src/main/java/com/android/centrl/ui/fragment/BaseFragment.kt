package com.android.centrl.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.centrl.viewmodel.ChatsViewModel
import com.android.centrl.viewmodel.GroupChannelsViewModel
import com.android.centrl.viewmodel.MultiChannelCountViewModel

open class BaseFragment : Fragment() {

    var mMutiChannelViewModel: MultiChannelCountViewModel? = null
    var mGroupChannelsViewModel: GroupChannelsViewModel? = null
    var mChatsViewModel: ChatsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mMutiChannelViewModel = ViewModelProvider(this).get(MultiChannelCountViewModel::class.java)
        mGroupChannelsViewModel = ViewModelProvider(this).get(GroupChannelsViewModel::class.java)
        mChatsViewModel = ViewModelProvider(this).get(ChatsViewModel::class.java)
    }
}