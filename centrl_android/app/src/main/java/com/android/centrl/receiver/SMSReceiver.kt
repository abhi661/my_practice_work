package com.android.centrl.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import android.util.Patterns
import com.android.centrl.notificationservice.NotificationData
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.persistence.repositry.ChatsRepository
import com.android.centrl.persistence.repositry.GroupChannelsRepository
import com.android.centrl.services.NotificationHandleService
import com.android.centrl.utils.AppConstant
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.utils.DateUtil
import java.util.*
import java.util.regex.Pattern

/**
 * SMS receive for receive SMS from device
 */
class SMSReceiver : BroadcastReceiver() {

    var pattern: Pattern = Pattern.compile("(|^)\\d{6}")
    private var mGroupChannelsRepository: GroupChannelsRepository? = null
    private var chatsRepository: ChatsRepository? = null
    override fun onReceive(context: Context, intent: Intent) {
        try {
            mGroupChannelsRepository = GroupChannelsRepository(context)
            chatsRepository = ChatsRepository(context)
            val bundle = intent.extras
            if (intent.action.equals("android.provider.Telephony.SMS_RECEIVED", ignoreCase = true)) {
                if (bundle != null) {
                    val sms = bundle[SMS_BUNDLE] as Array<Any>?
                    var smsMsg = ""
                    var smsSenderNumber = ""
                    var time: Long? = null
                    var smsMessage: SmsMessage
                    for (i in sms!!.indices) {
                        smsMessage = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            val format = bundle.getString("format")
                            SmsMessage.createFromPdu(sms[i] as ByteArray, format)
                        } else {
                            SmsMessage.createFromPdu(sms[i] as ByteArray)
                        }
                        if (smsMessage.messageBody != null) {
                            smsMsg = smsMessage.messageBody
                        }
                        smsSenderNumber = smsMessage.originatingAddress!!
                        time = smsMessage.timestampMillis
                    }
                    if (isValidMobile(smsSenderNumber) && smsSenderNumber.startsWith("+")) {
                        PrefManager.getInstance(context)!!
                            .saveBoolean(AppConstant.IS_VALID_NUMBER, true)
                        if (getNotificationData(context, smsSenderNumber, smsMsg, time!!) != null) {
                            val sVar = getNotificationData(context, smsSenderNumber, smsMsg, time)
                            val intentSMS = Intent(context, NotificationHandleService::class.java)
                            val bundleSMS = Bundle()
                            bundleSMS.putSerializable(NotificationHandleService.INTENT_KEY, sVar)
                            intentSMS.putExtras(bundleSMS)
                            NotificationHandleService.enqueueWork(context, intentSMS)
                        } else {
                            val sVar = NotificationData()
                            sVar.packageName = CentrlUtil.getDefaultSmsAppPackageName(context)
                            sVar.packCode = AppConstant.SMS_CODE
                            sVar.phoneNumber = smsSenderNumber
                            sVar.sortKey = null
                            sVar.mailKey = ""
                            sVar.notiType = AppConstant.CHAT_SCREEN
                            sVar.notiAt = DateUtil.getDbNotiAt(time)
                            sVar.title = smsSenderNumber
                            sVar.content = smsMsg
                            sVar.uniqueKey =
                                NotificationData.md5(smsSenderNumber + "@" + AppConstant.SMS_CODE)
                            val chat = mGroupChannelsRepository!!.getGroupKey(
                                sVar.uniqueKey,
                                AppConstant.SMS_CODE
                            )
                            val chatKey = chat.groupID
                            if (chatKey.isEmpty()) {
                                sVar.iconPath = ""
                                sVar.groupKey = sVar.uniqueKey
                            } else {
                                sVar.groupKey = chat.groupID
                                sVar.iconPath = chat.iconPath
                            }
                            val intentSMS = Intent(context, NotificationHandleService::class.java)
                            val bundleSMS = Bundle()
                            bundleSMS.putSerializable(NotificationHandleService.INTENT_KEY, sVar)
                            intentSMS.putExtras(bundleSMS)
                            NotificationHandleService.enqueueWork(context, intentSMS)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * Validate phone number
     */
    private fun isValidMobile(phone: String): Boolean {
        return Patterns.PHONE.matcher(phone).matches()
    }

    /**
     * Get name and details from phone contacts or existing chat contacts
     */
    private fun getNotificationData(
        context: Context?,
        mPhoneNumber: String?,
        msg: String?,
        time: Long
    ): NotificationData? {
        try {
            val contact = CentrlUtil.getContactDetailsByNumber(mPhoneNumber, context)
            val sVar = NotificationData()
            sVar.packageName = CentrlUtil.getDefaultSmsAppPackageName(context!!)
            sVar.packCode = AppConstant.SMS_CODE
            sVar.phoneNumber = mPhoneNumber
            sVar.sortKey = null
            sVar.mailKey = ""
            sVar.notiType = AppConstant.CHAT_SCREEN
            sVar.notiAt = DateUtil.getDbNotiAt(time)
            sVar.content = msg
            if (contact != null) {
                sVar.title = contact.name
                sVar.uniqueKey =
                    NotificationData.md5(contact.name!!.toLowerCase(Locale.getDefault()) + "@0" + AppConstant.SMS_CODE)
            } else {
                sVar.title = mPhoneNumber
                sVar.uniqueKey = NotificationData.md5(mPhoneNumber + "@0" + AppConstant.SMS_CODE)
            }
            val chat = mGroupChannelsRepository!!.getGroupKey(sVar.uniqueKey, AppConstant.SMS_CODE)
            val chatKey = chat.groupID
            if (chatKey.isEmpty()) {
                for (lastChats in chatsRepository!!.getAllChatsList(AppConstant.CHAT_SCREEN)) {
                    if (lastChats.title == sVar.title) {
                        sVar.groupKey = lastChats.groupID
                        sVar.groupIconPath = lastChats.groupIconPath
                    }
                }
                if (contact != null) {
                    if (contact.profilePath != null) {
                        sVar.iconPath =
                            CentrlUtil.getContactImage(context, contact.profilePath, contact.id)
                    } else {
                        sVar.iconPath = ""
                    }
                } else {
                    sVar.iconPath = ""
                }
                if (sVar.groupKey == null) {
                    sVar.groupKey = sVar.uniqueKey
                }
                sVar.groupTitle = sVar.title!!

                if (sVar.groupIconPath == null) {
                    sVar.groupIconPath = sVar.iconPath
                }

            } else {
                sVar.groupKey = chat.groupID
                sVar.groupTitle = chat.groupTitle
                val groupSize: Int = mGroupChannelsRepository!!.getGroupList(chat.groupID).size
                if (chat.edit == 1) {
                    sVar.iconPath = chat.iconPath
                    sVar.groupIconPath = chat.groupIconPath
                } else {
                    if (contact != null) {
                        if (contact.profilePath != null) {
                            sVar.iconPath =
                                CentrlUtil.getContactImage(context, contact.profilePath, contact.id)
                        } else {
                            sVar.iconPath = ""
                        }
                    } else {
                        sVar.iconPath = ""
                    }

                    if (groupSize == 1) {
                        sVar.groupIconPath = sVar.iconPath
                    } else {
                        sVar.groupIconPath = chat.groupIconPath
                    }
                }
            }
            return sVar
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null
    }

    companion object {
        const val SMS_BUNDLE = "pdus"
    }
}