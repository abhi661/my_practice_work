package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import io.reactivex.disposables.CompositeDisposable
/**
 * Base view model.
 */
open class BaseViewModel(application: Application) : AndroidViewModel(application) {
    protected val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCleared() {
        disposables.clear()
    }
}