package com.android.centrl.persistence.dao

import androidx.room.*
import com.android.centrl.persistence.entity.GroupChannelsEntity

/**
 * Data Access Object for the groups table.
 */
@Dao
interface GroupChannelsDao {

    /**
     * @param groupChannelsEntity the groups details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertGroup(groupChannelsEntity: GroupChannelsEntity)
    /**
     * update the groups details.
     */
    @Query("UPDATE groupChannel SET groupID = :groupId, groupTitle = :groupTitle, title = :title, iconPath = :notiIcon, groupIconPath = :groupIcon WHERE chatKey = :chatKey")
    fun updateGroup(chatKey: String, groupId: String, notiIcon: String, groupIcon: String, groupTitle: String, title: String)

    /**
     * update the groups details.
     */
    @Query("UPDATE groupChannel SET iconPath = :notiIcon,groupTitle = :groupTitle, title = :title, groupIconPath = :groupIcon WHERE groupID = :groupId")
    fun updateIcon(groupId: String, notiIcon: String, groupIcon: String, groupTitle: String, title: String)
    /**
     * merge the groups.
     */
    @Query("UPDATE groupChannel SET groupID = :groupId, groupTitle = :groupTitle WHERE chatKey = :chatKey")
    fun mergeGroup(groupId: String, groupTitle: String, chatKey: String)

    /**
     * update the mute in groups.
     */
    @Query("UPDATE groupChannel SET muteChat = :muteChat WHERE groupID = :groupId")
    fun updateMuteChat(muteChat: Int, groupId: String)
    /**
     * update group profile.
     */
    @Query("UPDATE groupChannel SET groupTitle = :groupTitle, groupIconPath = :groupIcon WHERE groupID = :groupId")
    fun updateEditProfileGroup(groupId: String, groupTitle: String, groupIcon: String)
    /**
     * update single profile.
     */
    @Query("UPDATE groupChannel SET groupTitle = :groupTitle, groupIconPath = :groupIcon, iconPath = :iconPath, edit = :isEdit WHERE groupID = :groupId")
    fun updateEditSingleProfile(groupId: String, groupTitle: String, groupIcon: String, iconPath: String, isEdit: Int)
    /**
     * update single profile.
     */
    @Query("UPDATE groupChannel SET groupID = :groupId, title = :title, chatKey = :chatKey, groupTitle = :title WHERE title = :phoneNumber")
    fun updateTitleGroup(title: String, groupId: String, chatKey: String, phoneNumber: String)

    /**
     * @return the chatkey from the table
     */
    @Query("SELECT chatKey FROM groupChannel WHERE chatKey = :chatKey")
    fun getChatKey(chatKey: String): String

    /**
     * @return the edited profile picture from table
     */
    @Query("SELECT edit FROM groupChannel WHERE groupID = :groupId")
    fun getIsEdited(groupId: String): Int
    /**
     * @return the reply chatkey from the table
     */
    @Query("SELECT chatKey FROM groupChannel WHERE groupID = :groupId AND packageCode = :packageCode")
    fun getReplyChatKey(groupId: String, packageCode: Int): String

    /**
     * @return the group key from the table
     */
    @Query("SELECT * FROM groupChannel WHERE chatKey = :chatKey AND packageCode = :packageCode")
    fun getGroupKey(chatKey: String, packageCode: Int): GroupChannelsEntity

    /**
     * @return the group title from the table
     */
    @Query("SELECT * FROM groupChannel WHERE groupID = :groupId AND packageCode = :packageCode")
    fun getGroupTitle(groupId: String, packageCode: Int): GroupChannelsEntity
    /**
     * @return the group details from the table
     */
    @Query("SELECT * FROM groupChannel WHERE groupID = :groupId")
    fun getGroupDetails(groupId: String): GroupChannelsEntity
    /**
     * @return the all group details from the table
     */
    @Query("SELECT * FROM groupChannel WHERE groupID = :groupId")
    fun getGroupList(groupId: String): List<GroupChannelsEntity>
    /**
     * @return the all group details from the table
     */
    @Query("SELECT DISTINCT groupID FROM groupChannel WHERE notiType = :notiType")
    fun getAllGroupList(notiType: String): List<String>

    /**
     * @return the return the all chats list from the table
     */
    @Query("SELECT DISTINCT groupID FROM groupChannel")
    fun getAllContactsLists(): List<String>

    /**
     * @return the group channels package code from the table
     */
    @Query("SELECT packageCode FROM groupChannel WHERE groupID = :groupId")
    fun getGroupChannelList(groupId: String): List<Int>
    /**
     * Delete group details from table.
     */
    @Query("DELETE FROM groupChannel WHERE groupID = :groupId")
    fun deleteGroup(groupId: String)
}