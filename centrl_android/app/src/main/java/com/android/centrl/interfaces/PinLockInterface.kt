package com.android.centrl.interfaces
/**
 * PIN lock setup listener
 */
interface PinLockInterface {
    fun firstPinLock(pin: String)
}