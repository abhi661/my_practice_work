package com.android.centrl.models

import com.android.centrl.persistence.entity.ChatsEntity

/**
 * Chat and mail object class for separate receiver and sender.
 */
class ChatModelObject : ListObject() {
    lateinit var chatModel: ChatsEntity

    override fun getType(userId: Int): Int {
        return if (chatModel.messageType.equals(ChatsEntity.MSG_TYPE_SENT, ignoreCase = true)) {
            TYPE_GENERAL_RIGHT
        } else TYPE_GENERAL_LEFT
    }
}