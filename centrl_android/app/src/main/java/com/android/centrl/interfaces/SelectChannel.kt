package com.android.centrl.interfaces
/**
 * Channel select listener
 */
interface SelectChannel {
    fun channelSelected(packCode: Int)
}