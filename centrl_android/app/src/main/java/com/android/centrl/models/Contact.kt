package com.android.centrl.models

import java.util.*

/**
 * Contact model class.
 */
class Contact {
    var id: String? = null
    var name: String? = null
    var profilePath: String? = null
    var phoneNumber: String? = null
    var emails: ArrayList<ContactEmail>? = null
    var numbers: ArrayList<ContactPhone>? = null

    constructor() {}

    constructor(id: String, name: String) {
        this.id = id
        this.name = name
        emails = ArrayList()
        numbers = ArrayList()
    }

    override fun toString(): String {
        var result = name
        if (numbers!!.size > 0) {
            val number = numbers!![0]
            result += " (" + number.number + " - " + number.type + ")"
        }
        if (emails!!.size > 0) {
            val email = emails!![0]
            result += " [" + email.address + " - " + email.type + "]"
        }
        return result!!
    }
}