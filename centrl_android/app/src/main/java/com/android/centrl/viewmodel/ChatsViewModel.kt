package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.persistence.repositry.ChatsRepository

/**
 * View model for managing chat details.
 */
class ChatsViewModel(application: Application) : BaseViewModel(application) {

    private var chatsRepository: ChatsRepository = ChatsRepository(application)
    private val getChatContactsList = MutableLiveData<ArrayList<ChatsEntity>>()
    /**
     * @param chatsEntity the chat details to be inserted.
     */
    fun insertChat(chatsEntity: ChatsEntity) {
        chatsRepository.insertChat(chatsEntity)
    }
    /**
     * update the chat details.
     */
    fun updateChat(chatKey: String, groupId: String) {
        chatsRepository.updateChat(chatKey, groupId)
    }
    /**
     * update the chat details.
     */
    fun updateTitleChat(groupId: String, title: String, chatKey: String, phoneNumber: String) {
        chatsRepository.updateTitleChat(groupId, title, chatKey, phoneNumber)
    }
    /**
     * @return the group chat list from the table
     */
    fun getGroupChatsList(groupId: String): ArrayList<ChatsEntity> {
        return chatsRepository.getGroupChatsList(groupId)
    }
    /**
     * @return the group chat list from the table
     */
    fun getGroupChannelsList(groupId: String): ArrayList<Int> {
        return chatsRepository.getGroupChannelsList(groupId)
    }
    /**
     * @return the last chats from the table
     */
    fun getLastGroupChats(groupId: String): ChatsEntity {
        return chatsRepository.getLastGroupChats(groupId)
    }
    /**
     * @return the single chats list from the table
     */
    fun getSingleChatsList(groupId: String, packageCode: Int): ArrayList<ChatsEntity> {
        return chatsRepository.getSingleChatsList(groupId, packageCode)
    }
    /**
     * @return the last single chats from the table
     */
    fun getLastSingleChats(groupId: String, packageCode: Int): ChatsEntity {
        return chatsRepository.getLastSingleChats(groupId, packageCode)
    }
    /**
     * @return the return the all chats list from the table
     */
    fun getAllChatsList(notiType: String): ArrayList<ChatsEntity> {
        return chatsRepository.getAllChatsList(notiType)
    }
    /**
     * @return the return the all chats list from the table
     */
    fun getChatContactsList(notiType: String){
       getChatContactsList.value = chatsRepository.getAllChatsList(notiType)
    }

    val mChatContactsList: LiveData<ArrayList<ChatsEntity>>
        get() = getChatContactsList
    /**
     * @return the return the all chats list from the table
     */
    fun getAllChatsLists(): ArrayList<ChatsEntity> {
        return chatsRepository.getAllChatsLists()
    }
    /**
     * @return the last chats from the table
     */
    fun getLastChat(groupId: String, notiType: String): ChatsEntity {
        return chatsRepository.getLastChat(groupId, notiType)
    }
    /**
     * Delete chats from table.
     */
    fun deleteChat(groupId: String) {
        chatsRepository.deleteChat(groupId)
    }
    /**
     * Delete message details from table.
     */
    fun deleteChatMessages(chatId: Int) {
        chatsRepository.deleteChatMessages(chatId)
    }
}