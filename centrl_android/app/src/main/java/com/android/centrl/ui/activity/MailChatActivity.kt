package com.android.centrl.ui.activity

import android.app.NotificationManager
import android.content.*
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.PopupMenu
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.pm.ShortcutInfoCompat
import androidx.core.content.pm.ShortcutManagerCompat
import androidx.core.graphics.drawable.IconCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.adapter.MailListAdapter
import com.android.centrl.appiconbadger.ShortcutBadger
import com.android.centrl.databinding.ActivityMailChatBinding
import com.android.centrl.interfaces.RecyclerViewClickListener
import com.android.centrl.notificationservice.PendingIntentManager
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.persistence.entity.BlockedContactsEntity
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.ui.dialog.DialogMailFilterMessage
import com.android.centrl.ui.views.RecyclerViewTouchListener
import com.android.centrl.utils.*
import com.bumptech.glide.Glide
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

/**
 * Mail chat room activity
 */
class MailChatActivity : BaseActivity(), PopupMenu.OnMenuItemClickListener,
    MailListAdapter.MailListAdapterInterface, DialogMailFilterMessage.DialogFilterMessageListener{

    companion object {
        val chatList = ArrayList<ChatsEntity>()
        private const val mailsScreen = "mails"
    }

    private lateinit var binding: ActivityMailChatBinding
    private var nReceiver: NotificationReceiver? = null
    private lateinit var adapter: MailListAdapter
    private lateinit var pendingIntentManager: PendingIntentManager
    private var mChatGroupIdList: ArrayList<ChatsEntity>? = null
    private var animation: Animation? = null
    private lateinit var chatDetails: GroupChannelsEntity
    private var isMultiSelect = false
    private var groupSize: Int = 0
    private var isMute: Int = 0
    private var filterPackageCode: Int = 0
    private var isFilterMessage: Boolean = false
    private var isBlocked = false
    private var isShowedMultiSelect = false
    private var isFromBundle = false


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mail_chat)
        binding.handler = ClickHandlers()
        val manager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(0)
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, true)
        pendingIntentManager = PendingIntentManager.getInstance(this)
        val bundle = intent.extras
        if (bundle != null) {
            groupKey = bundle.getString(AppConstant.GROUP_KEY)
            chatKey = bundle.getString(AppConstant.CHAT_KEY)
            packageCode = bundle.getInt(AppConstant.PACKAGE_CODE_KEY)
            isFromBundle = bundle.getBoolean(AppConstant.IS_FROM_NOTIFICATION)
        }
        initView()
        binding.chatRoomRecyclerViewMc.addOnItemTouchListener(RecyclerViewTouchListener(this, binding.chatRoomRecyclerViewMc, object : RecyclerViewClickListener {
                override fun onClick(view: View, position: Int) {
                    try {
                        if (adapter.lastSelectedPosition.size == 0) {
                            isMultiSelect = false
                        }
                        if (isMultiSelect) {
                            adapter.multiSelectList(position)
                            onMultiSelect(adapter.mChatGroupId)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                override fun onLongClick(view: View, position: Int) {
                    try {
                        isMultiSelect = true
                        adapter.multiSelectList(position)
                        onMultiSelect(adapter.mChatGroupId)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            })
        )
        mBlockedContactsViewModel!!.mInsertStatus.observe(this, Observer {
            PendingIntentManager.getInstance(this).getBlockedContact()
        })
        mGroupChannelsViewModel!!.mGroupDetails.observe(this, Observer { result ->
            try{
                PrefManager.getInstance(this)!!.saveStringValue(AppConstant.CHAT_GROUP_ID, groupKey)
                chatDetails = result
                groupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
                isMute = chatDetails.muteChat
                mMutiChannelViewModel!!.deleteCount(groupKey!!)
                mMutiChannelViewModel!!.deleteGroupAllPackageCode(chatDetails.groupID)

                var tempCount = 0
                for (chatCount in mMutiChannelViewModel!!.getAllCount()) {
                    val counts: Int = mMutiChannelViewModel!!.getCount(chatCount.groupID)
                    tempCount += counts
                }
                ShortcutBadger.applyCount(this, tempCount)

                if(chatDetails.isGroup == 1){
                    binding.groupChatChoiceMc.visibility = View.GONE
                    binding.unmergeGroupLlMc.visibility = View.GONE
                } else {
                    if (groupSize > 1) {
                        binding.groupChatChoiceMc.visibility = View.VISIBLE
                        binding.unmergeGroupLlMc.visibility = View.VISIBLE
                    } else {
                        binding.groupChatChoiceMc.visibility = View.GONE
                        binding.unmergeGroupLlMc.visibility = View.GONE
                    }
                }

                binding.groupChatTitleTxtMc.text = chatDetails.groupTitle
                if(chatDetails.groupIconPath.isEmpty()){
                    binding.groupChatProfileImageMc.setImageResource(R.drawable.default_user_image)
                } else {
                    val profileImage: Bitmap = BitmapFactory.decodeFile(chatDetails.groupIconPath)
                    binding.groupChatProfileImageMc.setImageBitmap(profileImage)
                }

                chatList.clear()
                if (isFilterMessage) {
                    for (chat in mChatsViewModel!!.getSingleChatsList(groupKey!!, packageCode)) {
                        chatList.add(chat)
                    }
                } else {
                    for (chat in mChatsViewModel!!.getGroupChatsList(groupKey!!)) {
                        chatList.add(chat)
                    }
                }
                chatList.reverse()
                adapter.setDataChange(CentrlUtil.groupDataIntoHashMap(chatList))
                refreshUI()
                val wallpaperPath = preferences!!.getString(AppConstant.WALLPAPER_PATH, null)
                if (wallpaperPath != null) {
                    Glide.with(this)
                        .load(wallpaperPath)
                        .into(binding.imgMailBg)
                } else {
                    binding.chatRootRlMc.setBackgroundColor(resources.getColor(R.color.bg_color, null))
                    Glide.with(this)
                        .load(R.drawable.defaut_background)
                        .into(binding.imgMailBg)
                }
            }catch (e: Exception){
                e.printStackTrace()
            }
        })
    }
    /**
     * Initialization views
     */
    private fun initView(){
        try{
            animation = AnimationUtils.loadAnimation(this, R.anim.search_right_to_left)

            val layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
            val dividerItemDecoration = DividerItemDecoration(binding.chatRoomRecyclerViewMc.context, DividerItemDecoration.VERTICAL)
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this, R.drawable.divider)!!)
            binding.chatRoomRecyclerViewMc.addItemDecoration(dividerItemDecoration)
            binding.chatRoomRecyclerViewMc.layoutManager = layoutManager

            adapter = MailListAdapter(this, null, this)
            adapter.setUser(0)
            binding.chatRoomRecyclerViewMc.adapter = adapter

            nReceiver = NotificationReceiver()
            val filter = IntentFilter()
            filter.addAction(AppConstant.ACTION_BROADCAST_RECEIVER_CHAT)
            registerReceiver(nReceiver, filter)

        }catch (e: Exception){
            e.printStackTrace()
        }
    }
    /**
     * Click handler class
     */
    inner class ClickHandlers {
        /**
         * Views click handler
         */

        fun onClick(v: View?) {
            when(v!!.id){
                R.id.group_chat_back_layout_mc -> {
                    closeActivity()
                }
                R.id.group_chat_menu_mc -> {
                    try{
                        val popup = PopupMenu(this@MailChatActivity, binding.groupChatMenuMc)
                        popup.menuInflater.inflate(R.menu.menu, popup.menu)
                        popup.setOnMenuItemClickListener(this@MailChatActivity)
                        popup.show()
                        val chat = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)

                        isMute = chat.muteChat
                        val menu = popup.menu
                        if (isMute == 0) {
                            menu.getItem(1).title = resources.getString(R.string.mute_txt)
                        } else {
                            menu.getItem(1).title = resources.getString(R.string.unmute_txt)
                        }
                        val getChatBlocked = mBlockedContactsViewModel!!.getBlockedContacts(groupKey!!)
                        if(getChatBlocked.groupID.isNotEmpty()){
                            if (getChatBlocked.groupID.equals(chat.groupID, ignoreCase = true)) {
                                menu.getItem(2).title = resources.getString(R.string.unblock_from_txt)
                                isBlocked = true
                            } else {
                                menu.getItem(2).title = resources.getString(R.string.block_from_txt)
                                isBlocked = false
                            }
                        } else {
                            menu.getItem(2).title = resources.getString(R.string.block_from_txt)
                            isBlocked = false
                        }

                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }
                R.id.group_chat_choice_mc -> {
                    val dialogFilter = DialogMailFilterMessage()
                    val bundle = Bundle()
                    bundle.putBoolean("isfilter", isFilterMessage)
                    dialogFilter.arguments = bundle
                    dialogFilter.show(supportFragmentManager, DialogMailFilterMessage.TAG)
                }
                R.id.unmerge_group_ll_mc -> {
                    try {
                        val groupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
                        if(groupSize == mChatGroupIdList!!.size){
                            var newGroupKey = ""
                            if (mChatGroupIdList!!.size >= 1) {
                                for (chat in mChatGroupIdList!!) {
                                    val rand = Random()
                                    newGroupKey = rand.nextInt(10000).toString()
                                    val chatsDetails = mGroupChannelsViewModel!!.getGroupKey(chat.chatKey, chat.packageCode)
                                    mGroupChannelsViewModel!!.updateGroup(chatsDetails.chatKey, newGroupKey, chatsDetails.iconPath, chatsDetails.iconPath, chatsDetails.title)
                                    mChatsViewModel!!.updateChat(chatsDetails.chatKey, newGroupKey)
                                }
                            }
                            val lastChat = mChatsViewModel!!.getLastChat(newGroupKey, AppConstant.CHAT_SCREEN)
                            packageCode = lastChat.packageCode
                            groupKey = lastChat.groupID
                            chatKey = lastChat.chatKey
                            refresh()
                        } else {
                            if (mChatGroupIdList!!.size >= 1) {
                                for (chat in mChatGroupIdList!!) {
                                    val rand = Random()
                                    val groupKey = rand.nextInt(10000).toString()
                                    val chatsDetails = mGroupChannelsViewModel!!.getGroupKey(chat.chatKey, chat.packageCode)
                                    mGroupChannelsViewModel!!.updateGroup(chatsDetails.chatKey, groupKey, chatsDetails.iconPath, chatsDetails.iconPath, chatsDetails.title)
                                    mChatsViewModel!!.updateChat(chatsDetails.chatKey, groupKey)
                                }
                            }
                            val getGroupSize = mGroupChannelsViewModel!!.getGroupList(groupKey!!).size
                            if(getGroupSize == 1){
                                val chatGroup = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)
                                mGroupChannelsViewModel!!.updateGroup(chatGroup.chatKey, groupKey!!, chatGroup.iconPath, chatGroup.iconPath, chatGroup.title)
                                mChatsViewModel!!.updateChat(chatGroup.chatKey, groupKey!!)
                            }
                            val lastChat = mChatsViewModel!!.getLastChat(groupKey!!, AppConstant.CHAT_SCREEN)
                            packageCode = lastChat.packageCode
                            refresh()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                R.id.select_delete_mc -> {
                   removeMails()
                }
                R.id.select_copy_mc -> {
                    try {
                        if (mChatGroupIdList!!.size >= 1) {
                            val sb = StringBuilder()
                            val str = "\n"
                            for (chat in mChatGroupIdList!!) {
                                sb.append(str)
                                sb.append(chat.messages)
                            }
                            copyText(sb.toString())
                            refresh()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                R.id.select_close_mc -> {
                    refresh()
                }
                R.id.mail_room_header_rl -> {
                    val mChatTitle = java.util.ArrayList<String>()
                    val intent = Intent(this@MailChatActivity, ProfileActivity::class.java)
                    intent.putExtra(AppConstant.GROUP_KEY, groupKey)
                    intent.putExtra(AppConstant.PARTICIPANTS, mChatTitle)
                    startActivity(intent)
                }
            }
        }
    }

    /**
     * Remove mails alert dialog.
     */
    private fun removeMails() {
        val builder = AlertDialog.Builder(this)
        if(mChatGroupIdList!!.size == 1){
            builder.setTitle(resources.getString(R.string.delete_chat_title))
        } else {
            builder.setTitle(resources.getString(R.string.delete_chat)+" ${mChatGroupIdList!!.size} "+resources.getString(R.string.delete_msgs))
        }
        builder.setCancelable(true)
        builder.setNegativeButton(resources.getString(R.string.cancel)) { dialog, _ -> dialog.dismiss() }
        builder.setPositiveButton(resources.getString(R.string.delete_chat)) { dialog, _ ->
            try {
                if (mChatGroupIdList!!.size >= 1) {
                    for (chat in mChatGroupIdList!!) {
                        mChatsViewModel!!.deleteChatMessages(chat.chatID)
                    }
                    refresh()
                    dialog.dismiss()
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val dialog = builder.create()
        dialog.show()
        dialog.getButton(AlertDialog.BUTTON_POSITIVE).isAllCaps = false
        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).isAllCaps = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.continue_color, null))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(resources.getColor(R.color.continue_color, null))
        } else {
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#4FC3B8"))
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#4FC3B8"))
        }
    }

    /**
     * Multi select for delete, copy and unmerge
     */
    private fun onMultiSelect(chats: ArrayList<ChatsEntity>) {
        try {
            binding.selectCountTxtMc.text = chats.size.toString()
            mChatGroupIdList = chats
            if (chats.size >= 1) {
                if (!isShowedMultiSelect) {
                    isShowedMultiSelect = true
                    binding.multiselectTitleBarMc.startAnimation(animation)
                    binding.multiselectTitleBarMc.visibility = View.VISIBLE
                    binding.chatHeaderMainLlMc.visibility = View.GONE
                }
            } else {
                isShowedMultiSelect = false
                binding.chatHeaderMainLlMc.startAnimation(animation)
                binding.chatHeaderMainLlMc.visibility = View.VISIBLE
                binding.multiselectTitleBarMc.visibility = View.GONE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * Refresh and update the views
     */
    private fun refresh() {
        try {
            isShowedMultiSelect = false
            binding.chatHeaderMainLlMc.startAnimation(animation)
            binding.chatHeaderMainLlMc.visibility = View.VISIBLE
            binding.multiselectTitleBarMc.visibility = View.GONE
            adapter.selectList(-1)
            adapter.lastSelectedPosition.clear()
            isMultiSelect = false
            mGroupChannelsViewModel!!.groupDetails(groupKey!!)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * Refresh UI
     */
    private fun refreshUI(){
        if(chatList.size == 0){
            binding.chatRoomRecyclerViewMc.visibility = View.GONE
            binding.emptyImageMailRoom.visibility = View.VISIBLE
        } else {
            binding.chatRoomRecyclerViewMc.visibility = View.VISIBLE
            binding.emptyImageMailRoom.visibility = View.GONE
        }
    }
    /**
     * Copy selected text from chats
     */
    private fun copyText(text: String) {
        try {
            if (text.isNotEmpty()) {
                val clipboardManager = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                val clipData: ClipData = ClipData.newPlainText("chats", text)
                clipboardManager.setPrimaryClip(clipData)
                if(mChatGroupIdList!!.size == 1){
                    CentrlSnackBar.showSnackBar(binding.mailRoomSnackar.snackbarCl,  resources.getString(R.string.msg_copied))
                } else {
                    CentrlSnackBar.showSnackBar(binding.mailRoomSnackar.snackbarCl,  resources.getString(R.string.msgs_copied))
                }

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onStart() {
        super.onStart()
        val manager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(0)
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, true)
        mGroupChannelsViewModel!!.groupDetails(groupKey!!)
    }

    /**
     * Receiving messages from notification and update the UI
     */
    internal inner class NotificationReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            try {
                val receivedPackCode = intent.getIntExtra(AppConstant.PACKAGE_CODE_KEY, 0)
                val receivedType = intent.getStringExtra(AppConstant.NOTIFICATION_TYPE)
                if (receivedType == mailsScreen) {
                    if (isFilterMessage) {
                        if (filterPackageCode == receivedPackCode) {
                            val chat = mChatsViewModel!!.getLastSingleChats(groupKey!!, packageCode)
                            chatKey = chat.chatKey
                            packageCode = chat.packageCode
                            chatList.add(chat)
                            chatList.reverse()
                            adapter.setDataChange(CentrlUtil.groupDataIntoHashMap(chatList))
                            mMutiChannelViewModel!!.deleteCount(groupKey!!)
                            mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                        }
                    } else {
                        val chat = mChatsViewModel!!.getLastGroupChats(groupKey!!)
                        chatKey = chat.chatKey
                        packageCode = chat.packageCode
                        chatList.add(chat)
                        chatList.reverse()
                        adapter.setDataChange(CentrlUtil.groupDataIntoHashMap(chatList))
                        mMutiChannelViewModel!!.deleteCount(groupKey!!)
                        mMutiChannelViewModel!!.deleteGroupAllPackageCode(chat.groupID)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (nReceiver !== null) {
            unregisterReceiver(nReceiver)
        }
    }

    override fun onStop() {
        super.onStop()
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, false)
    }
    /**
     * Menu options click event
     */
    override fun onMenuItemClick(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.action_profile -> {
                val mChatTitle = java.util.ArrayList<String>()
                val intent = Intent(this, ProfileActivity::class.java)
                intent.putExtra(AppConstant.GROUP_KEY, groupKey)
                intent.putExtra(AppConstant.PARTICIPANTS, mChatTitle)
                startActivity(intent)
                return true
            }
            R.id.action_mute -> {
                if (isMute == 0) {
                    mGroupChannelsViewModel!!.updateMuteChat(1, groupKey!!)
                } else {
                    mGroupChannelsViewModel!!.updateMuteChat(0, groupKey!!)
                }
                return true
            }
            R.id.action_block -> {
                if (isBlocked) {
                    mBlockedContactsViewModel!!.deleteBlockedContact(chatDetails.groupID)
                } else {
                    if (mBlockedContactsViewModel!!.getAllBlockedContactsList().size == 0) {
                        mBlockedContactsViewModel!!.insertBlockedContacts(
                            BlockedContactsEntity(
                                chatDetails.groupID,
                                chatDetails.chatKey,
                                chatDetails.title
                            )
                        )
                    } else {
                        val getChatBlocked = mBlockedContactsViewModel!!.getBlockedContacts(groupKey!!)
                        if (getChatBlocked.groupID.equals(chatDetails.groupID, ignoreCase = true)) {
                            CentrlSnackBar.showSnackBar(binding.mailRoomSnackar.snackbarCl, resources.getString(R.string.already_blocked))
                        } else {
                            mBlockedContactsViewModel!!.insertBlockedContacts(
                                BlockedContactsEntity(
                                    chatDetails.groupID,
                                    chatDetails.chatKey,
                                    chatDetails.title
                                )
                            )
                        }
                    }
                }
                return true
            }

            R.id.action_add_shortcut -> {
                try {
                    val chat = mGroupChannelsViewModel!!.getGroupDetails(groupKey!!)
                    createHomeShortcut(chat)
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
                return true
            }
            else -> return false
        }
    }
    /**
     * This is use to create shotcut to home screen for particular chat.
     */
    private fun createHomeShortcut(chat: GroupChannelsEntity) {
        try {
            if (ShortcutManagerCompat.isRequestPinShortcutSupported(applicationContext)) {
                val intent = Intent(applicationContext, MailChatActivity::class.java)
                intent.action = Intent.ACTION_MAIN
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra(AppConstant.GROUP_KEY, groupKey)
                intent.putExtra(AppConstant.PACKAGE_CODE_KEY, packageCode)
                intent.putExtra(AppConstant.CHAT_KEY, chatKey)
                intent.putExtra(AppConstant.IS_FROM_NOTIFICATION, true)
                if (chat.iconPath.isEmpty()) {
                    val shortcutInfo = ShortcutInfoCompat.Builder(applicationContext, chat.groupID)
                            .setIntent(intent) // !!! intent's action must be set on oreo
                            .setShortLabel(chat.groupTitle)
                            .setIcon(IconCompat.createWithResource(applicationContext, R.drawable.default_user_image))
                            .build()
                    ShortcutManagerCompat.requestPinShortcut(applicationContext, shortcutInfo, null)
                } else {
                    val picBitmap = ImageUtility.decodeUri(this, Uri.fromFile(File(chat.groupIconPath)), 192)
                    val shortcutInfo = ShortcutInfoCompat.Builder(applicationContext, chat.groupID)
                            .setIntent(intent) // !!! intent's action must be set on oreo
                            .setShortLabel(chat.groupTitle)
                            .setIcon(IconCompat.createWithBitmap(picBitmap))
                            .build()
                    ShortcutManagerCompat.requestPinShortcut(applicationContext, shortcutInfo, null)
                }
            } else {
                Toast.makeText(this, resources.getString(R.string.not_support), Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * This will take in to mail app for reply.
     */
    override fun replyMail(chatKey: String, packCode: Int) {
        val packageName = installedAppsViewModel!!.getInstalledPackageName(packCode)
        if(packageName.isNotEmpty()){
            pendingIntentManager.goOtherApps(chatKey, installedAppsViewModel!!.getInstalledPackageName(packCode))
        } else {
            CentrlSnackBar.showSnackBar(binding.mailRoomSnackar.snackbarCl, resources.getString(R.string.package_not_installed))
        }
    }
    /**
     * This is use to filter channel messages
     */
    override fun onMessageFilter(packCode: Int) {
        if (packCode == AppConstant.GROUP_CODE) {
            isFilterMessage = false
            binding.groupChatChoiceMc.setImageResource(R.drawable.ic_group_icon)
        } else {
            isFilterMessage = true
            filterPackageCode = packCode
            packageCode = packCode
            if(packCode != 0){
                binding.groupChatChoiceMc.setImageResource(CentrlUtil.getChannelsLogo()[packageCode]!!)
            }
        }
        try {
            chatList.clear()
            if (isFilterMessage) {
                for (chat in mChatsViewModel!!.getSingleChatsList(groupKey!!, packageCode)) {
                    chatList.add(chat)
                }
            } else {
                for (chat in mChatsViewModel!!.getGroupChatsList(groupKey!!)) {
                    chatList.add(chat)
                }
            }
            chatList.reverse()
            adapter.setDataChange(CentrlUtil.groupDataIntoHashMap(chatList))
            refreshUI()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (isShowedMultiSelect) {
                refresh()
            } else {
                closeActivity()
            }
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

    private fun closeActivity(){
        try{
            if(isFromBundle){
                isFromBundle = false
                val nextScreen = Intent(this, InteractionsActivity::class.java)
                nextScreen.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                nextScreen.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                nextScreen.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(nextScreen)
            }
            finish()
            PrefManager.getInstance(this)!!.saveStringValue(AppConstant.CHAT_GROUP_ID, "")
        }catch (e: Exception){
            e.printStackTrace()
        }
    }
}
