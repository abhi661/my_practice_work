package com.android.centrl.persistence.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.android.centrl.persistence.dao.*
import com.android.centrl.persistence.entity.*
import com.android.centrl.utils.AppConstant

/**
 * The Room database that contains the ChatBackup table
 */
@Database(
    entities = [ChatBackupEntity::class],
    version = 1,
    exportSchema = false
)
abstract class CentrlBackupDatabase: RoomDatabase() {
    /**
     * Chats backup Dao
     */
    abstract fun chatBackupDao(): ChatBackupDao

    companion object {
        /**
         * Create centrl database.
         */
        fun getInstance(context: Context): CentrlBackupDatabase = Room.databaseBuilder(
            context.applicationContext,
            CentrlBackupDatabase::class.java,
            AppConstant.DATA_BASE_BACKUP
        ).setJournalMode(JournalMode.TRUNCATE).build()
    }
}