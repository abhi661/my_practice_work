package com.android.centrl.ui.dialog

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.android.centrl.R
import com.android.centrl.databinding.DialogRateUsBinding

/**
 * Rate us dialog
 */
class DialogRateUs : DialogFragment() {


    private lateinit var binding: DialogRateUsBinding

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
       val dialog = Dialog(requireActivity(), R.style.DialogBlackTransparent)
        binding = DataBindingUtil.inflate(dialog.layoutInflater, R.layout.dialog_rate_us, null, false)
        dialog.setContentView(binding.root)

        binding.closeRateUs.setOnClickListener { dialog.dismiss() }
        binding.rateusBtn.setOnClickListener {
            rateApp()
            dialog.dismiss()
        }
        binding.lateBtn.setOnClickListener { dialog.dismiss() }
        return dialog
    }
    /**
     * This is use to open rate screen in google play
     */
    private fun rateApp() {
        try {
            val rateIntent = rateIntentForUrl("market://details")
            startActivity(rateIntent)
             startActivityForResult(rateIntent ,RATE_APP_REQUEST_CODE)
        } catch (e: ActivityNotFoundException) {
            val rateIntent = rateIntentForUrl("https://play.google.com/store/apps/details")
            startActivity(rateIntent)
        }
    }
    /**
     * This is use to open rate screen in google play
     */
    private fun rateIntentForUrl(url: String): Intent {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(String.format("%s?id=%s", url, requireActivity().packageName)))
        var flags = Intent.FLAG_ACTIVITY_NO_HISTORY or Intent.FLAG_ACTIVITY_MULTIPLE_TASK
        flags = if (Build.VERSION.SDK_INT >= 21) {
            flags or Intent.FLAG_ACTIVITY_NEW_DOCUMENT
        } else {
            flags or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        intent.addFlags(flags)
        return intent
    }

    companion object {
        var TAG = "DialogRateUs"
        var RATE_APP_REQUEST_CODE = 200
    }
}