package com.android.centrl.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.persistence.entity.ChatsEntity
import com.android.centrl.persistence.repositry.BlockedContactRepo
import com.android.centrl.persistence.repositry.GroupChannelsRepository
import com.android.centrl.persistence.repositry.MultiChannelCountRepository
import com.android.centrl.utils.CentrlUtil
import com.android.centrl.utils.DateUtil
import java.util.*

/**
 * Chat tab contacts list adapter
 */
class ChatContactListAdapter(
    context: Context,
    arrayList: ArrayList<ChatsEntity>,
    filterSearchInterface: FilterSearchInterface
) : RecyclerView.Adapter<ChatContactListAdapter.ChatAppMsgViewHolder>(), Filterable {

    private val mFilterSearchInterface: FilterSearchInterface
    private val blockedContactRepo: BlockedContactRepo
    private var mMultiChannelRepository: MultiChannelCountRepository? = null
    private var mGroupChannelsRepository: GroupChannelsRepository? = null
    var chatArrayList: ArrayList<ChatsEntity>
    private var chatArrayListCopy: ArrayList<ChatsEntity>
    var lastSelectedPosition = ArrayList<String>()
    var mChatGroupId = ArrayList<ChatsEntity>()
    var mBlockedContactList = ArrayList<String>()
    var mSameChannelList = ArrayList<String>()
    var mGroupChatList = ArrayList<String>()
    private var context: Context
    private var previousPosition = -1
    private var isMultiSelect = false
    private var isCheckboxMultiSelect = false
    private var isMergeSelect = false
    private var isTxtEmpty = false

    init {
        chatArrayList = arrayList
        this.context = context
        chatArrayListCopy = arrayList
        blockedContactRepo = BlockedContactRepo(context)
        mFilterSearchInterface = filterSearchInterface
        mMultiChannelRepository = MultiChannelCountRepository(context)
        mGroupChannelsRepository = GroupChannelsRepository(context)
    }
    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatAppMsgViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.chat_app_item_view, parent, false)
        return ChatAppMsgViewHolder(view)
    }

    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ChatAppMsgViewHolder, position: Int) {
        try {
            val chat = chatArrayList[position]
            // If the message is a received message.
            holder.chatTitleTxt!!.text = chat.groupTitle
            if(chat.groupChat == 1){
                if(chat.messageType == ChatsEntity.MSG_TYPE_RECEIVED){
                    if(chat.messages.contains("@@")){
                        val tempMSG = chat.messages.split("@@")
                        val title = tempMSG[0]
                        val msg = tempMSG[1].replaceFirst(" ", "")
                        holder.chatMSGTxt!!.text = "$title: $msg"
                    } else {
                        holder.chatMSGTxt!!.text = chat.messages
                    }
                } else {
                    holder.chatMSGTxt!!.text = chat.messages
                }
            } else {
                holder.chatMSGTxt!!.text = chat.messages
            }

            if (chat.timeStamp.isNotEmpty()) {
                holder.chatMSGTimeTxt!!.text = DateUtil.convertAMFormat(chat.timeStamp).toLowerCase(
                    Locale.getDefault()
                )
            } else {
                holder.chatMSGTimeTxt!!.text = ""
            }
            if(chat.groupIconPath!!.isEmpty()){
                holder.profileImageView!!.setImageResource(R.drawable.default_user_image)
            } else {
                val profileImage: Bitmap = BitmapFactory.decodeFile(chat.groupIconPath)
                holder.profileImageView!!.setImageBitmap(profileImage)
            }

            if (chat.muteChat == 1) {
                holder.chatTypeImage!!.setBackgroundResource(R.drawable.ic_mute_chat)
            } else {
                if(chat.packageCode != 0){
                    holder.chatTypeImage!!.setBackgroundResource(CentrlUtil.getChannelsLogo()[chat.packageCode]!!)
                }
            }
            if (!isMultiSelect) {
                holder.mergeCheckBox!!.isChecked = false
                if (position == previousPosition) {
                    holder.chatContactRootLayout!!.setBackgroundColor(ContextCompat.getColor(context, R.color.gray_color_select))
                } else {
                    holder.chatContactRootLayout!!.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                }
            } else {
                if (chat.isSelected) {
                    holder.chatContactRootLayout!!.setBackgroundColor(ContextCompat.getColor(context, R.color.gray_color_select))
                    if (isCheckboxMultiSelect) {
                        holder.mergeCheckBox!!.isChecked = true
                    }
                } else {
                    holder.chatContactRootLayout!!.setBackgroundColor(ContextCompat.getColor(context, R.color.white))
                    holder.mergeCheckBox!!.isChecked = false
                }
            }
            val count = mMultiChannelRepository!!.getCount(chat.groupID)
            if (count == 0) {
                holder.chatCountTxt!!.visibility = View.GONE
                holder.chatTitleTxt!!.setTextColor(ContextCompat.getColor(context, R.color.chat_title))
                holder.chatMSGTxt!!.setTextColor(ContextCompat.getColor(context, R.color.chat_msg_r))
                holder.chatMSGTimeTxt!!.setTextColor(ContextCompat.getColor(context, R.color.chat_msg_r))
                holder.chatTitleTxt!!.setTypeface(null, Typeface.BOLD)
                holder.chatMSGTxt!!.setTypeface(null, Typeface.NORMAL)
                holder.chatMSGTimeTxt!!.setTypeface(null, Typeface.NORMAL)
            } else {
                holder.chatCountTxt!!.text = count.toString()
                holder.chatCountTxt!!.visibility = View.VISIBLE
                holder.chatTitleTxt!!.setTextColor(ContextCompat.getColor(context, R.color.chat_title))
                holder.chatMSGTxt!!.setTextColor(ContextCompat.getColor(context, R.color.chat_title))
                holder.chatMSGTimeTxt!!.setTextColor(ContextCompat.getColor(context, R.color.chat_title))
                holder.chatTitleTxt!!.setTypeface(null, Typeface.BOLD)
                holder.chatMSGTxt!!.setTypeface(null, Typeface.BOLD)
                holder.chatMSGTimeTxt!!.setTypeface(null, Typeface.BOLD)
            }
            var packCount = mMultiChannelRepository!!.getGroupAllPackageCode(chat.groupID).size
            if (packCount > 1) {
                packCount -= 1
                holder.chatPackageCountTxt!!.text = "+$packCount"
                holder.chatPackageCountTxt!!.visibility = View.VISIBLE
            } else {
                holder.chatPackageCountTxt!!.visibility = View.GONE
            }
            if (isMergeSelect) {
                holder.mergeCheckBox!!.visibility = View.VISIBLE
            } else {
                holder.mergeCheckBox!!.visibility = View.GONE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
    /**
     * This is use to select contacts using checkbox to merge
     */
    fun chooseMergeOption(mergeSelect: Boolean) {
        isMergeSelect = mergeSelect
        isCheckboxMultiSelect = mergeSelect
        notifyDataSetChanged()
    }
    /**
     * This is use to remove highlight for selected contacts
     */
    fun selectList(position: Int) {
        isMultiSelect = false
        isCheckboxMultiSelect = false
        previousPosition = position
        notifyDataSetChanged()
    }
    /**
     * This is use to multi select contacts to merge
     */
    fun multiSelectList(position: Int) {
        try{
            isMultiSelect = true
            if (lastSelectedPosition.contains(position.toString())) {
                lastSelectedPosition.remove(position.toString())
                val chat = chatArrayList[position]
                mChatGroupId.remove(chat)
                previousPosition = -1
                chat.isSelected = false
                if (mBlockedContactList.size != 0 && mBlockedContactList.contains(chat.groupID)) {
                    mBlockedContactList.remove(chat.groupID)
                }
                val groupDetailsList = mGroupChannelsRepository!!.getGroupList(chat.groupID)
                for (group in groupDetailsList) {
                    mSameChannelList.remove(group.packageCode.toString())
                }
                if(mGroupChatList.size != 0){
                    mGroupChatList.remove(chat.groupID)
                }
                notifyItemChanged(position)
            } else {
                lastSelectedPosition.add(position.toString())
                val chat = chatArrayList[position]
                mChatGroupId.add(chat)
                chat.isSelected = true
                previousPosition = position
                val chatBlockedContact = blockedContactRepo.getBlockedDetails(chat.groupID)
                if (chatBlockedContact.groupID.isNotEmpty()) {
                    mBlockedContactList.add(chatBlockedContact.groupID)
                }
                val groupDetailsList = mGroupChannelsRepository!!.getGroupList(chat.groupID)
                for (group in groupDetailsList) {
                    mSameChannelList.add(group.packageCode.toString())
                }
                if(chat.groupChat == 1){
                    mGroupChatList.add(chat.groupID)
                }
                notifyItemChanged(position)
            }
        }catch (e: Exception){
            e.printStackTrace()
        }
    }


    override fun getItemCount(): Int {
        return chatArrayList.size
    }
    /**
     * This is use to search particular name
     */
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    isTxtEmpty = true
                    chatArrayList = chatArrayListCopy
                } else {
                    isTxtEmpty = false
                    val filteredList = ArrayList<ChatsEntity>()
                    for (row in chatArrayListCopy) {
                        if (row.groupTitle!!.toLowerCase(Locale.getDefault()).contains(
                                charString.toLowerCase(
                                    Locale.getDefault()
                                )
                            )) {
                            filteredList.add(row)
                        }
                    }
                    chatArrayList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = chatArrayList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                chatArrayList = filterResults.values as ArrayList<ChatsEntity>
                if (isTxtEmpty) {
                    mFilterSearchInterface.filterResult(-1)
                } else {
                    mFilterSearchInterface.filterResult(chatArrayList.size)
                }
                notifyDataSetChanged()
            }
        }
    }
    /**
     * Adapter view holder
     */
    class ChatAppMsgViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {
        var chatTitleTxt: TextView? = null
        var chatMSGTxt: TextView? = null
        var chatMSGTimeTxt: TextView? = null
        var chatCountTxt: TextView? = null
        var chatPackageCountTxt: TextView? = null
        var profileImageView: ImageView? = null
        var chatTypeImage: ImageView? = null
        var mergeCheckBox: CheckBox? = null
        var chatContactRootLayout: RelativeLayout? = null

        init {
            if (itemView != null) {
                chatTitleTxt = itemView.findViewById<View>(R.id.chat_title_text_view) as TextView
                chatMSGTxt = itemView.findViewById<View>(R.id.chat_msg_text_view) as TextView
                chatMSGTimeTxt = itemView.findViewById<View>(R.id.chat_time_text_view) as TextView
                chatCountTxt = itemView.findViewById<View>(R.id.chat_count_txt) as TextView
                chatPackageCountTxt = itemView.findViewById<View>(R.id.pack_count_txt) as TextView
                profileImageView = itemView.findViewById<View>(R.id.chat_profile_image) as ImageView
                chatTypeImage = itemView.findViewById<View>(R.id.chat_type_view) as ImageView
                chatContactRootLayout = itemView.findViewById<View>(R.id.chat_contact_rl) as RelativeLayout
                mergeCheckBox = itemView.findViewById<View>(R.id.select_merge_checkbox) as CheckBox
            }
        }
    }
    /**
     * This will update the list
     */
    fun updateList(chatList: ArrayList<ChatsEntity>) {
        chatArrayList = chatList
        chatArrayListCopy = chatList
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
    /**
     * Search filter interface
     */
    interface FilterSearchInterface {
        fun filterResult(size: Int)
    }
}