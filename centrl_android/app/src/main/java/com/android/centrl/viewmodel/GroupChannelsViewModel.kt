package com.android.centrl.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.centrl.persistence.entity.GroupChannelsEntity
import com.android.centrl.persistence.repositry.GroupChannelsRepository
import com.android.centrl.persistence.repositry.KeyBoardAppsRepository

/**
 * View model for managing chats group details
 */
class GroupChannelsViewModel(application: Application) : BaseViewModel(application) {

    private val mGetGroupDetails = MutableLiveData<GroupChannelsEntity>()
    private val mKeyBoardTrayApps = MutableLiveData<ArrayList<Int>>()

    private var groupChannelsRepository: GroupChannelsRepository =
        GroupChannelsRepository(application)
    private var keyboardAppsRepository: KeyBoardAppsRepository = KeyBoardAppsRepository(application)
    /**
     * @param groupChannelsEntity the groups details to be inserted.
     */
    fun insertGroup(groupChannelsEntity: GroupChannelsEntity) {
        groupChannelsRepository.insertGroup(groupChannelsEntity)
    }
    /**
     * update the groups details.
     */
    fun updateGroup(chatKey: String, groupId: String, notiIcon: String, groupIcon: String, groupTitle: String) {
        groupChannelsRepository.updateGroup(chatKey, groupId, notiIcon, groupIcon, groupTitle, groupTitle)
    }
    /**
     * merge the groups.
     */
    fun mergeGroup(groupId: String, groupTitle: String, chatKey: String) {
        groupChannelsRepository.mergeGroup(groupId, groupTitle, chatKey)
    }
    /**
     * update the mute in groups.
     */
    fun updateMuteChat(muteChat: Int, groupId: String) {
        groupChannelsRepository.updateMuteChat(muteChat, groupId)
    }
    /**
     * update group profile.
     */
    fun updateEditProfileGroup(groupId: String, groupTitle: String, groupIcon: String) {
        groupChannelsRepository.updateEditProfileGroup(groupId, groupTitle, groupIcon)
    }
    /**
     * update single profile.
     */
    fun updateEditSingleProfile(groupId: String, groupTitle: String, groupIcon: String, iconPath: String, isEdit: Int) {
        groupChannelsRepository.updateEditSingleProfile(groupId, groupTitle, groupIcon, iconPath, isEdit)
    }
    /**
     * update single profile.
     */
    fun updateTitleGroup(title: String, groupId: String, chatKey: String, phoneNumber: String) {
        groupChannelsRepository.updateTitleGroup(title, groupId, chatKey, phoneNumber)
    }
    /**
     * @return the chatkey from the table
     */
    fun getChatKey(chatKey: String): String {
        return groupChannelsRepository.getChatKey(chatKey)
    }
    /**
     * @return the reply chatkey from the table
     */
    fun getReplyChatKey(groupId: String, packageCode: Int): String {
        return groupChannelsRepository.getReplyChatKey(groupId, packageCode)
    }
    /**
     * @return the group key from the table
     */
    fun getGroupKey(chatKey: String, packageCode: Int): GroupChannelsEntity {
        return groupChannelsRepository.getGroupKey(chatKey, packageCode)
    }
    /**
     * @return the group title from the table
     */
    fun getGroupDetails(groupId: String): GroupChannelsEntity {
        return groupChannelsRepository.getGroupDetails(groupId)
    }
    /**
     * @return the group title from the table
     */
    fun groupDetails(groupId: String) {
        mGetGroupDetails.value = groupChannelsRepository.getGroupDetails(groupId)
    }
    val mGroupDetails: LiveData<GroupChannelsEntity>
        get() = mGetGroupDetails

    fun getKeyboardTrayApps(){
        mKeyBoardTrayApps.value = keyboardAppsRepository.getAllKeyboardAppTrayPackCode()
    }

    val mKeyboardApps: LiveData<ArrayList<Int>>
        get() = mKeyBoardTrayApps

    /**
     * @return the group details from the table
     */
    fun getGroupList(groupId: String): ArrayList<GroupChannelsEntity> {
        return groupChannelsRepository.getGroupList(groupId)
    }
    /**
     * Delete group details from table.
     */
    fun deleteGroup(groupId: String) {
        groupChannelsRepository.deleteGroup(groupId)
    }
}