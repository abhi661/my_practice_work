package com.android.centrl.persistence.injections

import android.app.Application
import android.content.Context
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.datasource.KeyBoardAppsTrayDataSource
import com.android.centrl.persistence.repositry.KeyboardAppsTrayRepository
import com.android.centrl.viewmodel.KeyBoardAppsViewModelFactory
/**
 * Enables injection of data sources.
 */
object KeyBoardTrayInjection {
    /**
     * Provide centrl data source
     */
    private fun provideCentrlDataSource(context: Context?): KeyBoardAppsTrayDataSource {
        val database = CentrlDatabase.getInstance(context!!)
        return KeyboardAppsTrayRepository(database.keyBoardAppTrayDao(), database.removedKeyBoardAppsDao())
    }
    /**
     * Provide view model factory
     */
    fun provideViewModelFactory(application: Application, context: Context?): KeyBoardAppsViewModelFactory {
        val dataSource = provideCentrlDataSource(context)
        return KeyBoardAppsViewModelFactory(application, dataSource)
    }
}