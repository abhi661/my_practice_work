package com.android.centrl.persistence.repositry

import android.content.Context
import com.android.centrl.persistence.dao.InstalledAppsDao
import com.android.centrl.persistence.database.CentrlDatabase
import com.android.centrl.persistence.entity.InstalledAppsEntity
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsyncResult

/**
 * Access point for managing installed apps data.
 */
class InstalledAppsRepository(context: Context) {

    private var mInstalledDao: InstalledAppsDao

    init {
        val centrlDatabase = CentrlDatabase.getInstance(context)
        mInstalledDao = centrlDatabase.installedAppsDao()
    }

    /**
     * Insert a installed apps details in the database. If the installed apps details already exists, replace it.
     */
    fun insertInstalledApps(installedAppsEntity: InstalledAppsEntity) {
        try {
            Completable.fromAction {
                mInstalledDao.insertInstalledApps(installedAppsEntity)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     *
     * @return the package name from the table
     */
    fun getInstalledPackageName(packageCode: Int): String {
        return getPackageAsyncTask(packageCode)
    }

    /**
     * Delete package from table.
     */
    fun deleteInstalledApps(packageCode: Int) {
        try {
            Completable.fromAction {
                mInstalledDao.deleteInstalledApps(packageCode)
            }.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {}
                    override fun onComplete() {}
                    override fun onError(e: Throwable) {}
                })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * AsyncTask for get packages from database
     */
    private fun getPackageAsyncTask(packCode: Int): String {
        val packageName = doAsyncResult(null,{
            mInstalledDao.getInstalledPackageName(packCode)
        })
        return packageName.get()
    }
}