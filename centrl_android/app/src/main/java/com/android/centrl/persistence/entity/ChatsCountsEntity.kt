package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a notification counts table
 */
@Entity(tableName = "chatCounts")
data class ChatsCountsEntity(
    @PrimaryKey
    var groupID: String,
    var chatKey: String,
    var count: Int
)