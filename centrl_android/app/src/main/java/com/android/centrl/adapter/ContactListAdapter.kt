package com.android.centrl.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.android.centrl.R
import com.android.centrl.models.Contact
import com.bumptech.glide.Glide
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter
import jp.wasabeef.glide.transformations.CropCircleTransformation
import java.util.*
/**
 * Contact tab contacts list adapter
 */
class ContactListAdapter(context: Context, arrayList: ArrayList<Contact>?, contactFilterSearchInterface: ContactFilterSearchInterface) : RecyclerView.Adapter<ContactListAdapter.ContactsViewHolder>(), Filterable, StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder?> {

    var contactArrayList: ArrayList<Contact>? = null
    private var contactArrayListCopy: ArrayList<Contact>? = null
    var context: Context
    var contactFilterSearchInterface: ContactFilterSearchInterface
    private var isTxtEmpty = false

    init {
        contactArrayList = arrayList
        this.context = context
        contactArrayListCopy = arrayList
        this.contactFilterSearchInterface = contactFilterSearchInterface
    }
    // Passes the message object to a ViewHolder so that the contents can be bound to UI.
    override fun onBindViewHolder(holder: ContactsViewHolder, position: Int) {
        val msgDto = contactArrayList!![position]
        // If the message is a received message.
        holder.chatTitleTxt!!.text = msgDto.name
        Glide.with(context)
            .load(msgDto.profilePath)
            .error(R.drawable.default_user_image)
            .bitmapTransform(CropCircleTransformation(context))
            .into(holder.profileImageView)
    }
    // Inflates the appropriate layout according to the ViewType.
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.contact_item_view, parent, false)
        return ContactsViewHolder(view)
    }

    override fun getHeaderId(position: Int): Long {
        return if (position == 0) {
            -1
        } else {
            contactArrayList!![position].name!![0].toLong()
        }
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_header, parent, false)
        return object : RecyclerView.ViewHolder(view) {}
    }

    override fun onBindHeaderViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val textView = holder!!.itemView as TextView
        textView.text = contactArrayList!![position].name!![0].toString()
        holder.itemView.tag = position
    }

    override fun getItemCount(): Int {
        return contactArrayList!!.size
    }

    override fun getItemId(position: Int): Long {
        return contactArrayList!![position].hashCode().toLong()
    }
    /**
     * This is use to search particular name
     */
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val charString = charSequence.toString()
                if (charString.isEmpty()) {
                    isTxtEmpty = true
                    contactArrayList = contactArrayListCopy
                } else {
                    val filteredList = ArrayList<Contact>()
                    for (row in contactArrayListCopy!!) {
                        if (row.name!!.toLowerCase(Locale.getDefault()).contains(charString.toLowerCase(Locale.getDefault()))) {
                            filteredList.add(row)
                        }
                    }
                    contactArrayList = filteredList
                }
                val filterResults =
                    FilterResults()
                filterResults.values = contactArrayList
                return filterResults
            }

            override fun publishResults(charSequence: CharSequence, filterResults: FilterResults) {
                try{
                    contactArrayList = filterResults.values!! as ArrayList<Contact>
                    if (isTxtEmpty) {
                        contactFilterSearchInterface.filterResult(-1)
                    } else {
                        contactFilterSearchInterface.filterResult(contactArrayList!!.size)
                    }
                    notifyDataSetChanged()
                }catch (e: Exception){
                    e.printStackTrace()
                }
            }
        }
    }
    /**
     * Adapter view holder
     */
    class ContactsViewHolder(itemView: View?) :
        RecyclerView.ViewHolder(itemView!!) {
        var chatTitleTxt: TextView? = null
        var profileImageView: ImageView? = null
        private var rootRL: RelativeLayout? = null

        init {
            if (itemView != null) {
                chatTitleTxt = itemView.findViewById(R.id.contact_title_text_view)
                profileImageView = itemView.findViewById(R.id.contact_profile_image)
                rootRL = itemView.findViewById(R.id.contact_list_item_root)
            }
        }
    }
    /**
     * This will update the list
     */
    fun updateList(chatArrayList: ArrayList<Contact>?) {
        contactArrayList = chatArrayList
        //now, tell the adapter about the update
        notifyDataSetChanged()
    }
    /**
     * Search filter interface
     */
    interface ContactFilterSearchInterface {
        fun filterResult(size: Int)
    }
}