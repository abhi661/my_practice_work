package com.android.centrl.persistence.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Entity model class for a keyboard tray apps table
 * */
@Entity(tableName = "keyboardAppsTray")
data class KeyboardAppsTrayEntity(
    @PrimaryKey
    var packageCode: Int,
    var packagesName: String
)