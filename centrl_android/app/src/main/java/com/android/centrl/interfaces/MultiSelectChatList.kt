package com.android.centrl.interfaces

import com.android.centrl.persistence.entity.ChatsEntity
import java.util.*
/**
 * Chat and mail contacts multi select listener
 */
interface MultiSelectChatList {
    fun onMultiSelect(
        multiSelect: Boolean,
        checkboxMultiSelect: Boolean,
        chatArrayList: ArrayList<ChatsEntity>,
        blockedContactSelected: Boolean,
        sameChannelSelected: Boolean,
        groupChatsSelected: Boolean
    )

    fun onReceived(
        position: Int,
        chatArrayList: ArrayList<String>
    )
}