package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import com.android.centrl.R
import com.android.centrl.notificationservice.PrefManager
import com.android.centrl.utils.AppConstant
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit

/**
 * Account setup success activity.
 */
class AccountSetupActivity : BaseActivity() {
    private val worker: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_setup)
        PrefManager.getInstance(this)!!.saveBoolean(AppConstant.IS_CHAT_SCREEN, false)
        PrefManager.getInstance(this)!!.saveBoolean(PrefManager.IS_INIT, true)
        val runnable = Runnable {
            editor!!.putBoolean(AppConstant.PERMISSION_SCREEN, true)
            editor!!.putBoolean(AppConstant.VIBRATE, true)
            editor!!.putBoolean(AppConstant.LIGHT_UP, true)
            editor!!.putBoolean(AppConstant.NOTI_SOUND, true)
            editor!!.putBoolean(AppConstant.SHOW_MSG, true)
            editor!!.commit()
            val intent = Intent(this@AccountSetupActivity, InteractionsActivity::class.java)
            startActivity(intent)
            finish()
        }
        worker.schedule(runnable, 2, TimeUnit.SECONDS)
    }
}