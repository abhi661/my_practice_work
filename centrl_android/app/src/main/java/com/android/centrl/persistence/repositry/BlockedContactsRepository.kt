package com.android.centrl.persistence.repositry

import com.android.centrl.persistence.dao.BlockedContactsDao
import com.android.centrl.persistence.datasource.BlockedContactsDataSource
import com.android.centrl.persistence.entity.BlockedContactsEntity
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * Using the Room database as a data source.
 */
class BlockedContactsRepository(private val mBlockedContacts: BlockedContactsDao) :
    BlockedContactsDataSource {
    /**
     * Insert a blocked contacts details in the database. If the blocked contacts details already exists, replace it.
     */
    override fun insertBlockedContacts(blockedContactsEntity: BlockedContactsEntity): Completable {
        return mBlockedContacts.insertBlockedContacts(blockedContactsEntity)
    }
    /**
     * @return the blocked contacts details from the table
     */
    override fun getAllBlockedList(): Flowable<List<BlockedContactsEntity>> {
        return mBlockedContacts.getAllBlockedList()
    }
    /**
     * @return the blocked contacts details from the table
     */
    override fun getBlockedDetails(groupID: String): Flowable<BlockedContactsEntity> {
        return mBlockedContacts.getBlockedDetails(groupID)
    }
    /**
     * Delete all blocked contacts details.
     */
    override fun deleteBlockedContact(groupID: String) {
        mBlockedContacts.deleteBlockedContact(groupID)
    }

}