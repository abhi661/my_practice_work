package com.android.centrl.utils

/**
 * Centrl constant
 */
object AppConstant {
    const val DATA_BASE = "centrlChats.db"
    const val DATA_BASE_BACKUP = "backup.db"
    const val ACTION_BROADCAST_RECEIVER = "com.android.centrl"
    const val ACTION_BROADCAST_RECEIVER_CHAT = "com.android.centrl.chat"
    const val PREF_NAME = "central_chat"
    const val IS_CHAT_SCREEN = "is_chat"
    const val PERMISSION_SCREEN = "permission_setup"
    const val INTRO_SCREEN_FINISHED = "intro_setup_finished"
    const val CHAT_SCREEN = "chat"
    const val MAIL_SCREEN = "mails"
    const val FONT_SIZE = "font_size"
    const val WALLPAPER_PATH = "wallpaper_path"
    const val IS_VALID_NUMBER = "isValidNumber"
    const val VIBRATE = "vibrate"
    const val LIGHT_UP = "light_up"
    const val NOTI_SOUND = "noti_sound"
    const val SHOW_MSG = "show_msg"
    const val PREMIUM = "premium"
    const val CLASS_NAME = "class_name"
    const val CLASS_NAME_SPLASH = "ui.activity.SplashActivity"
    const val PIN_LOCK = "pin_lock"
    const val SETUP_FINGER = "setupPIN"
    const val PATTERN_LOCK = "pattern_lock"
    const val LOCK_TYPE = "lock_type"
    const val OLD_BACKUP_ID = "old_id"
    const val FIRST_BACKUP = "first_backup"
    const val BACKUP_TIME = "backup_time"
    const val NOTISAVE = "com.android.centrl"

    const val GROUP_KEY = "group_key"
    const val CHAT_KEY = "unique_key"
    const val SHARED_TEXT_KEY = "shared_key"
    const val PACKAGE_CODE_KEY = "package_code"
    const val NOTIFICATION_TYPE = "noti_type"
    const val IS_FROM_NOTIFICATION = "is_from_notification"
    const val CHAT_GROUP_ID = "chat_group_ID"
    const val GOOGLE_SIGN_SKIP = "signin_skip"
    const val PARTICIPANTS = "participants"
    const val EMAIL = "email"
    const val IS_SEND = "isSend"
    const val SET_IMAGE = "isSetImage"
    const val LAUNCH_COUNT = "counter"
    const val AD_SHOWED = "ad_date"
    const val CONTINUE_BUTTON = "#4FC3B8"

    const val NONE_LOCK_CODE = 0
    const val PIN_LOCK_CODE = 1
    const val PATTERN_LOCK_CODE = 2
    const val FINGER_LOCK_CODE = 3

    const val MESSENGER_CODE = 1
    const val WHATSAPP_CODE = 2
    const val TELEGRAM_CODE = 3
    const val SMS_CODE = 4
    const val SLACK_CODE = 5
    const val SKYPE_CODE = 6
    const val KIK_CODE = 7
    const val WECHAT_CODE = 8
    const val VIBER_CODE = 9
    const val KAKAO_CODE = 10
    const val LINE_CODE = 11
    const val INSTAGRAM_CODE = 12
    const val HANGOUTS_CODE = 13
    const val DISCORD_CODE = 14
    const val ICQ_CODE = 15
    const val REDDIT_CODE = 16
    const val QQ_CODE = 17
    const val GMAIL_CODE = 30
    const val OUTLOOK_CODE = 31
    const val GROUP_CODE = 40
    const val TOTAL_CHATS_APP = 17

    const val SLEEP: Long = 30

}