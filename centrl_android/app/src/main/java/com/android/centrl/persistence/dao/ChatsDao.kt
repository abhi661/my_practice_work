package com.android.centrl.persistence.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.android.centrl.persistence.entity.ChatsEntity

/**
 * Data Access Object for the chat table.
 */
@Dao
interface ChatsDao {

    /**
     * @param chatsEntity the chat details to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertChat(chatsEntity: ChatsEntity)
    /**
     * update the chat details.
     */
    @Query("UPDATE chats SET groupID = :groupId WHERE chatKey = :chatKey")
    fun updateChat(chatKey: String, groupId: String)
    /**
     * update the chat details.
     */
    @Query("UPDATE chats SET groupID = :groupId, title = :title, chatKey = :chatKey WHERE title = :phoneNumber")
    fun updateTitleChat(groupId: String, title: String, chatKey: String, phoneNumber: String)

    /**
     * @return the group chat list from the table
     */
    @Query("SELECT * FROM chats WHERE groupID = :groupId")
    fun getGroupChatsList(groupId: String): List<ChatsEntity>

    /**
     * @return the last chats from the table
     */
    @Query("SELECT * FROM chats WHERE groupID = :groupId ORDER BY chatID DESC LIMIT 1")
    fun getLastGroupChats(groupId: String): ChatsEntity

    /**
     * @return the single chats list from the table
     */
    @Query("SELECT * FROM chats WHERE groupID = :groupId AND packageCode = :packageCode")
    fun getSingleChatsList(groupId: String, packageCode: Int): List<ChatsEntity>

    /**
     * @return the last single chats from the table
     */
    @Query("SELECT * FROM chats WHERE groupID = :groupId AND packageCode = :packageCode ORDER BY chatID DESC LIMIT 1")
    fun getLastSingleChats(groupId: String, packageCode: Int): ChatsEntity

    /**
     * @return the last chats from the table
     */
    @Query("SELECT * FROM chats WHERE groupID = :groupId and notiType = :notiType ORDER BY chatID DESC LIMIT 1")
    fun getLastChat(groupId: String, notiType: String): ChatsEntity
    /**
     * @return the last chats from the table
     */
    @Query("SELECT * FROM chats WHERE groupID = :groupId ORDER BY chatID DESC LIMIT 1")
    fun getLastChats(groupId: String): ChatsEntity
    /**
     * Delete chats from table.
     */
    @Query("DELETE FROM chats WHERE groupID = :groupId")
    fun deleteChat(groupId: String)

    /**
     * Delete message details from table.
     */
    @Query("DELETE FROM chats WHERE chatID = :chatId")
    fun deleteChatMessages(chatId: Int)
}