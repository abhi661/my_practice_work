package com.android.centrl.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.centrl.R
import com.android.centrl.adapter.FAQExpandableListAdapter
import com.android.centrl.models.FAQChild
import com.android.centrl.utils.CentrlUtil
import kotlinx.android.synthetic.main.activity_f_a_q.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class FAQActivity : AppCompatActivity(), FAQExpandableListAdapter.ChildSelectInterface {

    private var adapterFAQ: FAQExpandableListAdapter? = null
    private val faqHeaders = ArrayList<String>()
    private val childFAQArrayList: HashMap<String, List<FAQChild>> = HashMap()
    private var lastPosition = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_f_a_q)

        faq_list_back.setOnClickListener {
            finish()
        }
        try{
            val jsonFileString: String = CentrlUtil.getJsonFromAssets(
                applicationContext,
                "faq.json"
            )
            getFAQListResponse(jsonFileString)

            simple_expandable_listview.setOnGroupExpandListener { groupPosition ->
                if (lastPosition != -1
                    && groupPosition != lastPosition
                ) {
                    simple_expandable_listview.collapseGroup(lastPosition)
                }
                lastPosition = groupPosition
            }
        }catch (e: Exception){
            e.printStackTrace()
        }

    }
    /**
     * Get FAQ datas from json
     */
    // parsing json data (Advetisement details response)
    private fun getFAQListResponse(response: String) {
        try {
            val mJSONObj = JSONObject(response)
            val mResultJSON: JSONObject = mJSONObj.getJSONObject("result")
            faqHeaders.clear()
            childFAQArrayList.clear()
            if (mResultJSON.length() > 0) {
                    val mResultJSONArray: JSONArray = mResultJSON.getJSONArray("faq")
                    for (i in 0 until mResultJSONArray.length()) {
                        val titleJSON = mResultJSONArray.getJSONObject(i)
                        val title: String = titleJSON.getString("title")
                        faqHeaders.add(title)
                    }
                    for (l in 0 until faqHeaders.size) {
                        val titleJSON = mResultJSONArray.getJSONObject(l)
                        val mResultJSONTemplateArray: JSONArray = titleJSON.getJSONArray(
                            faqHeaders.get(
                                l
                            )
                        )
                        val faqDetailsArrayList = ArrayList<FAQChild>()
                        for (k in 0 until mResultJSONTemplateArray.length()) {
                            val mDetailsJSONObject: JSONObject = mResultJSONTemplateArray.getJSONObject(
                                k
                            )
                            val ques =mDetailsJSONObject.getString("question")
                            val ans =mDetailsJSONObject.getString("answer")
                            faqDetailsArrayList.add(FAQChild(ques, ans))
                        }
                        childFAQArrayList.put(faqHeaders.get(l), faqDetailsArrayList)
                    }
                    if (faqHeaders.size > 0) {
                         adapterFAQ = FAQExpandableListAdapter(
                             this,
                             faqHeaders,
                             childFAQArrayList,
                             this
                         )
                         simple_expandable_listview.setAdapter(adapterFAQ)
                    }
            }

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun childItem(ques: String, ans: String) {
        val intent = Intent(this@FAQActivity, FAQDetailsActivity::class.java)
        intent.putExtra("header", ques)
        intent.putExtra("content", ans)
        startActivity(intent)
    }
}