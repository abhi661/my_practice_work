package me.aflak.libraries.callback;

import me.aflak.libraries.utils.FingerprintToken;

public interface FingerprintSecureCallback {
    void onAuthenticationSucceeded();
    void onAuthenticationFailed();
    void onNewFingerprintEnrolled(FingerprintToken token);
    void onAuthenticationError(int errorCode, String error);
    void onAuthenticationHelp(int errorCode, String error);
}
