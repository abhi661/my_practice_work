package me.aflak.libraries.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintToken {
    private CipherHelper cipherHelper;

    public FingerprintToken(CipherHelper cipherHelper) {
        this.cipherHelper = cipherHelper;
    }

    public void validate(){
        if(cipherHelper !=null) {
            cipherHelper.generateNewKey();
        }
    }
}
