package me.aflak.libraries.callback;

import me.aflak.libraries.view.Fingerprint;

public interface FailAuthCounterCallback {
    void onTryLimitReached(Fingerprint fingerprint);
}
